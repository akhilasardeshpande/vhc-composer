[Bugs](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=bugs)
[Code smells](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=code_smells)
[Coverage](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=coverage)
[Duplicated lines](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=duplicated_lines_density)
[Lines of code](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=ncloc)
[Maintainability](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=sqale_rating)
[Quality gate](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=alert_status)
[Reliability](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=reliability_rating)
[Security](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=security_rating)
[Technical debt](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=sqale_index)
[Vulnerabilities](https://whitewalker.ci.swiggyops.de/api/project_badges/measure?project=swiggy%3Asupply%3Avendor%3Avhc-composer%3Aall&metric=vulnerabilities)
# README

### How do I get set up?

- Summary of set up
  - Using Docker Compose: `docker-compose up`
  - Running Locally: `make dev`
    > might have to install reflex to run locally `go get github.com/cespare/reflex`.
    
  - If you are still getting the error try
    > export PATH=$PATH:$(go env GOPATH)/bin
- Configuration
  - All Configuration for local setup will be taken from commons.conf, dev.conf and .env file
    > Configs in .env will override dev.conf which in turn overrides commons.conf.
- Dependencies
  - All Dependencies are present in vendor folder
- How to run tests
  - To Run the test locally use `make test-local` It'll run test & prints the coverage
- Deployment instructions
  - Deployed via Shuttle
  - command `coast trigger pipeline -b master -r vhc-composer -a <commit-id> -f app.yaml` -e <Env>
  - Logs:
    - Prod using logman or coast
      > coast get logs -n vhc-composer -e production -r singapore -t swiggy.cloud --since 15m
    - Staging using coast command
      > coast get logs -n vhc-composer -e staging -r singapore -t swig.gy --since 15m
  - Status:
    - Prod using coast command
      > coast get status -s vhc-composer -e production -r singapore -t swiggy.cloud
    - Staging using coast command
      > coast get status -s vhc-composer -e staging -r singapore -t swig.gy
  - coast-cli setup:
    - Ref: https://bitbucket.org/swigy/coast-cli/src/master/

### Contribution guidelines

- Writing tests
  - Refer: `/tests` directory to see some sample SLT's
- Onboarding New Service
  - Put all code inside `graph/services/<service_name>` folder
  - Need to add custom instrumentation code for new-relic for each end point as `gqlgen` library desn't have a middleware which automatically does it as of writing this readme file
  - Refer other end points for new-relic & prometheus instrumentation is done

### Who do I talk to?

- Any of the Contributers

### Some Useful Links & Commands

- Install graphql Library:
  - Command: `go install github.com/99designs/gqlgen@v0.12.0`
- See gqlgen version
  - Command `gqlgen version`
- Generate the schema:
  - Command: `gqlgen generate`
- Sync the Dependencies to vendor folder

  - Command: `go mod vendor`

- ssh tunneling:

  - sample command: `ssh -N sf -L 8090:10.65.111.28:8090`
    > Ref: https://swiggy.atlassian.net/browse/SHTTL-1300

- Composer URL:

  - Staging: https://vhc-composer.staging.singapore.swig.gy/
  - Prod URL: https://vhc-composer.swiggy.com/

- Sentry:
  [Production URL](https://sentry.io/organizations/bundl-technologies-pvt-ltd/)

- New Relic:
  [Production URL](https://one.newrelic.com/launcher/nr1-core.explorer?pane=eyJuZXJkbGV0SWQiOiJhcG0tbmVyZGxldHMudHJhbnNhY3Rpb25zIiwiZW50aXR5SWQiOiJOek0zTkRnMmZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME5URTFOVEExTkRrIn0=&sidebars[0]=eyJuZXJkbGV0SWQiOiJucjEtY29yZS5hY3Rpb25zIiwiZW50aXR5SWQiOiJOek0zTkRnMmZFRlFUWHhCVUZCTVNVTkJWRWxQVG53ME5URTFOVEExTkRrIiwic2VsZWN0ZWROZXJkbGV0Ijp7Im5lcmRsZXRJZCI6ImFwbS1uZXJkbGV0cy50cmFuc2FjdGlvbnMifX0=&platform[accountId]=737486&platform[timeRange][duration]=1800000)

- Graphana:
  [Production URL](https://grafana.production.singapore.swiggy.cloud/d/YC_spAUGk/vhc-composer?orgId=37)

- Logman:
  [Production URL](https://logman.swiggyops.de/d/mkqRUh-Mk/service-logs?orgId=1&var-Pod=rock&var-service=vhc-composer&var-search=&var-AND=&var-AND2=)
