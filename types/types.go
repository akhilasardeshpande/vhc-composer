package types

import (
	"context"
	"io"
)

type HttpRequest struct {
	Context     context.Context
	Url         string
	Method      string
	Body        io.Reader
	Headers     map[string]string
	QueryParams map[string]string
}

type HttpRequestMeta struct {
	ApiName string
	source  string
}
