package big_cache

type WebsocketConfigResponse struct {
	PK    string   `json:"PK"`
	SK    string   `json:"SK"`
	Value []string `json:"value"`
}
