package big_cache

import (
	"time"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"github.com/allegro/bigcache/v3"
	log "github.com/sirupsen/logrus"
)

const WebsocketPrefix = "WS:"

var cache *bigcache.BigCache

func InitializeWebsocketCache() {
	log.Infof("VendorConfig invoked")
	apiName := "VendorConfig"
	url := conf.VendorConfigURL + conf.VendorConfigWebSocketPath
	log.Infof("VendorConfig Request {URL: %v }", url)
	queryParams := make(map[string]string)
	queryParams["i"] = "RIDS"
	var content WebsocketConfigResponse

	err := utils.MakeRequest("GET", url, nil, nil, queryParams, &content, apiName, nil)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
	}
	log.Infof("Vendor-Config Response %v", content.Value)
	cache, err = bigcache.NewBigCache(bigcache.DefaultConfig(8400 * time.Hour))
	if err != nil {
		log.Panicf("Error While Initalising BigCache %v", err.Error())
	}
	for _, restID := range content.Value {
		cache.Set(WebsocketPrefix+restID, []byte("1"))
	}

}

func WebSocketEnabled(restaurantId string) bool {
	if _, err := cache.Get(WebsocketPrefix + restaurantId); err == nil {
		return true
	}
	return false
}
