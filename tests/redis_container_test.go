package tests

import (
	"fmt"
	"testing"

	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/kafka"
	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/redis"
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/stretchr/testify/assert"
)

func InitRedisContainer(t *testing.T) *redis.Redis {
	redisContainer, err := redis.NewContainer()

	assert.Equal(t, nil, err, "Failed to initialise Redis test container")
	conf.Redis.RedisUrl = fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port())
	conf.Redis.RedisPassword = ""
	return redisContainer
}

func initKafkaTestContainer(t *testing.T) *kafka.Kafka {

	kafkaContainer, err := kafka.NewContainer()
	assert.Equal(t, nil, err, "Failed to initialise Kafka test container")
	host := fmt.Sprintf("%s:%s", kafkaContainer.Host(), kafkaContainer.Port())

	conf.TxnHAPrimaryCluster.Host = host
	conf.TxnHASecondaryCluster.Host = host

	if err := kafkaContainer.CreateTopics([]string{conf.NewOrderConsumer.Topic, conf.OrderCancelConsumer.Topic, conf.EditedOrderConsumer.Topic, conf.OrderStatusUpdateV2Consumer.Topic, conf.PlacingFSMConsumer.Topic}); err != nil {
		t.Errorf("Error creating topics error = %v", err)
	}

	return kafkaContainer
}
