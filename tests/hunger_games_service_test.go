package tests

import (
	"fmt"
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/go-redis/redis/v8"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestSwiggyStarRewards(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	scoreMockData, err := ioutil.ReadFile("../mocks/data/score_response.json")
	require.Equal(t, nil, err, "Could not fetch Score mock data")
	httpmock.RegisterResponder("GET", conf.HungerGamesBaseURL+conf.HungerGamesGetScorePath,
		httpmock.NewBytesResponder(200, scoreMockData))

	tierMockData, err := ioutil.ReadFile("../mocks/data/tier_response.json")
	require.Equal(t, nil, err, "Could not fetch Tier mock data")
	httpmock.RegisterResponder("GET", conf.HungerGamesBaseURL+conf.HungerGamesGetTieringDetailsPath,
		httpmock.NewBytesResponder(200, tierMockData))
	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	resolvers := graph.GraphqlHandler(redisClient)
	clt := client.New(graph.GetHeadersMiddleware(resolvers))
	query := `query get_swiggy_star_rewards {
				get_swiggy_star_rewards(input: {
					restaurantIds: [12895]
				}) {
					result {
						restaurantId
						needsAtttentionMetrics
						currentScore
						previousScore
						arrowDirection
						currentTierName
						currentTierScore
						previousTierScore
					}
				}
			}`

	var resp struct {
		Data *model.SwiggyStarRewardsResponse `json:"get_swiggy_star_rewards"`
	}

	err = clt.Post(query, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}

	require.Equal(t, "On Time Preparation Score, Order Ratings Score",
		resp.Data.Result[0].NeedsAtttentionMetrics, "Needs attention metrics did not match")

	require.Equal(t, int64(40), resp.Data.Result[0].CurrentScore, "Current score metrics did not match")
}
