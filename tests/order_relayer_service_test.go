package tests

import (
	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/kafka"
	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/wiremock"
	"bitbucket.org/swigy/vhc-composer/cache/big_cache"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/kafka/consumer"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"fmt"
	"github.com/99designs/gqlgen/client"
	"github.com/go-redis/redis/v8"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
	"time"
)

func ProduceEvents(k *kafka.Kafka, t *testing.T) {

	testMessages := []struct {
		val   string
		topic string
	}{
		{
			val:   `{"eventId":"19dbbe2b-7277-41ba-afa3-3da9b748b630","eventTimestamp":"2021-05-22T16:12:46.867","eventType":"PLACING_STATE_TRANSITION","version":2,"data":{"orderId":104753989461,"restaurantId":12896,"fromState":"VENDOR_FULFILLED","toState":"CANCELLED","metadata":"{}","updatedBy":"VVO_PLACER"}}`,
			topic: conf.PlacingFSMConsumer.Topic,
		},
		{
			val:   `{"eventId":"9e4be81c-b272-41c7-bd30-311beab278a0","eventTimestamp":"2021-04-14 12:39:23","eventType":"NEW_ORDER","data":{"orderId":104753989462,"restaurantId":9990,"partnerType":3,"orderType":"regular","paymentTxnStatus":"success","orderedTime":"2021-04-14T12:39:23","orderMetaData":"null","restaurantDetails":{"lat":"19.2048361","lng":"72.97761860000003","name":"Hotel Swagath","partnerId":"12896","restaurantPartnerId":"12896","area":{"id":214,"name":"Thane","city":{"id":5,"name":"Mumbai"}},"phones":["02225479547,02225471291"]},"cafeData":{},"status":{"isEdited":false,"orderStatus":"processing","editedStatus":"unedited"},"billing":{"bill":180,"incoming":186,"orderSpending":0,"total":186,"tax":0,"discount":36,"restaurantTradeDiscount":36,"paymentMethod":"Cash","coupon":{"code":""},"packingCharge":0,"gst":0,"finalGPPrice":0,"restaurantDiscountHit":36,"restaurantOffersDiscount":0,"freebieDiscountHit":0},"delivery":{},"placing":{"prepTimePred":13.187229},"customer":{"customerId":"8781652","customerArea":"Thane West","customerLat":"19.203246","customerLng":"72.9744","customerDistance":0.6,"customerComment":"","mobile":"9769343960","name":"Sakshi Bhanushali "},"cart":{"charges":{"vat":0,"serviceTax":0,"serviceCharge":0,"packingCharge":0,"deliveryCharge":32,"convenienceFee":10,"restaurantTaxationType":"GST","gst":0,"gstDetails":{"cartCGST":0,"cartIGST":0,"cartSGST":0,"itemCGST":0,"itemIGST":0,"packagingCGST":0,"packagingSGST":0},"taxExpressions":{"cgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","igst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","sgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","packagingCgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingSgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingIgst":"[TOTAL_PACKING_CHARGES]*0.0"}},"items":[{"itemId":"6864013","mealQuantity":1,"externalItemId":"","quantity":1,"name":"Paneer Tikka Masala","packingCharges":0,"restaurantDiscountHit":36,"subTotal":180,"total":180,"addons":[],"variants":[],"category":"Indian","charges":{"vat":"0","serviceCharge":"0","serviceTax":"0","gst":"0"},"taxExpressions":{"vat":"0","serviceCharge":"0","serviceTax":"0","gstInclusive":false},"gstDetails":{"cgst":0,"igst":0,"sgst":0},"isVeg":"1","subCategory":"Paneer Main Course","itemRestaurantOffersDiscount":0,"freeItemQuantity":0}],"meals":[]},"orderInfo":{"isAssured":false},"deliveryFeeCouponBreakup":{"discountShare":{"restaurantDiscount":0}},"paymentTransaction":[{"transactionId":"101459363368","paymentMeta":{}}]}}`,
			topic: conf.NewOrderConsumer.Topic,
		},
		{
			val:   `{"eventId":"19dbbe2b-7277-41ba-afa3-3da9b748b630","eventTimestamp":"2021-05-22T16:12:46.867","eventType":"PLACING_STATE_TRANSITION","version":2,"data":{"orderId":104753989462,"restaurantId":9990,"fromState":"VENDOR_FULFILLED","toState":"CANCELLED","metadata":"{}","updatedBy":"VVO_PLACER"}}`,
			topic: conf.PlacingFSMConsumer.Topic,
		},
		{
			val:   `{"eventId":"9e4be81c-b272-41c7-bd30-311beab278a0","eventTimestamp":"2021-04-14 12:39:23","eventType":"NEW_ORDER","data":{"orderId":101459363368,"restaurantId":12895,"partnerType":3,"orderType":"regular","paymentTxnStatus":"success","orderedTime":"2021-04-14T12:39:23","orderMetaData":"null","restaurantDetails":{"lat":"19.2048361","lng":"72.97761860000003","name":"Hotel Swagath","partnerId":"12895","restaurantPartnerId":"12895","area":{"id":214,"name":"Thane","city":{"id":5,"name":"Mumbai"}},"phones":["02225479547,02225471291"]},"cafeData":{},"status":{"isEdited":false,"orderStatus":"processing","editedStatus":"unedited"},"billing":{"bill":180,"incoming":186,"orderSpending":0,"total":186,"tax":0,"discount":36,"restaurantTradeDiscount":36,"paymentMethod":"Cash","coupon":{"code":""},"packingCharge":0,"gst":0,"finalGPPrice":0,"restaurantDiscountHit":36,"restaurantOffersDiscount":0,"freebieDiscountHit":0},"delivery":{},"placing":{"prepTimePred":13.187229},"customer":{"customerId":"8781652","customerArea":"Thane West","customerLat":"19.203246","customerLng":"72.9744","customerDistance":0.6,"customerComment":"","mobile":"9769343960","name":"Sakshi Bhanushali "},"cart":{"charges":{"vat":0,"serviceTax":0,"serviceCharge":0,"packingCharge":0,"deliveryCharge":32,"convenienceFee":10,"restaurantTaxationType":"GST","gst":0,"gstDetails":{"cartCGST":0,"cartIGST":0,"cartSGST":0,"itemCGST":0,"itemIGST":0,"packagingCGST":0,"packagingSGST":0},"taxExpressions":{"cgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","igst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","sgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","packagingCgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingSgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingIgst":"[TOTAL_PACKING_CHARGES]*0.0"}},"items":[{"itemId":"6864013","mealQuantity":1,"externalItemId":"","quantity":1,"name":"Paneer Tikka Masala","packingCharges":0,"restaurantDiscountHit":36,"subTotal":180,"total":180,"addons":[],"variants":[],"category":"Indian","charges":{"vat":"0","serviceCharge":"0","serviceTax":"0","gst":"0"},"taxExpressions":{"vat":"0","serviceCharge":"0","serviceTax":"0","gstInclusive":false},"gstDetails":{"cgst":0,"igst":0,"sgst":0},"isVeg":"1","subCategory":"Paneer Main Course","itemRestaurantOffersDiscount":0,"freeItemQuantity":0}],"meals":[]},"orderInfo":{"isAssured":false},"deliveryFeeCouponBreakup":{"discountShare":{"restaurantDiscount":0}},"paymentTransaction":[{"transactionId":"101459363368","paymentMeta":{}}]}}`,
			topic: conf.NewOrderConsumer.Topic,
		},
		{
			val:   `{"eventId":"19dbbe2b-7277-41ba-afa3-3da9b748b630","eventTimestamp":"2021-05-22T16:12:46.867","eventType":"PLACING_STATE_TRANSITION","version":2,"data":{"orderId":101459363368,"restaurantId":12895,"fromState":"VENDOR_FULFILLED","toState":"CANCELLED","metadata":"{}","updatedBy":"VVO_PLACER"}}`,
			topic: conf.PlacingFSMConsumer.Topic,
		},
		{
			val:   `{"is_pop_order":false,"geo_details":{"restaurant_location":{"lat":"12.930831","lng":"77.6332112","latAsDouble":12.930831,"lngAsDouble":77.6332112},"customer_location":{"lat":"12.923584","lng":"77.606864","latAsDouble":12.923584,"lngAsDouble":77.606864},"city_id":"1","area_id":"1","area_name":"Koramangala","zone_id":"1"},"finance_details":{"system_pay":0,"system_bill":422,"system_collect":0},"banner_factor":0,"geo_fence_breached":false,"partner_name":"DELIVERY_PLATFORM","order_id":"101459363368","status":"reached","source":"APP","time":"2021-05-22 07:07:55","deliveryboy_id":"1220190","deDetails":{"id":"1220190","name":"Manoj Kumar Guppa","mobileNumber":"8431508067","altMobile":"8431508067","nick":"Manoj Kumar Guppa","imageUrl":"https://de-docs.s3.amazonaws.com/de-images/eoGHYmRtfJiFvLm2aounoJSk.jpeg","lat":"12.923671","lng":"77.6069739"},"restaurant_id":12895,"restaurant_name":"Sardarji Malai Chaap wale(Subhash Nagar)"}`,
			topic: conf.OrderStatusUpdateV2Consumer.Topic,
		},
		{
			val:   `{"eventId":"a3033868-879b-4df6-ae06-bb05b16e5d52","eventTimestamp":"2021-06-05 20:14:18","eventType":"ORDER_EDITED","data":{"orderId":101459363369,"restaurantId":72566,"partnerType":19,"orderType":"regular","paymentTxnStatus":"success","orderedTime":"2021-06-05T20:06:05","orderMetaData":"null","restaurantDetails":{"lat":"17.4297917","lng":"78.427618","name":"Eat n Joy Bakers","partnerId":"kczp4vyh","restaurantPartnerId":"kczp4vyh","area":{"id":163,"name":"Ameerpet","city":{"id":3,"name":"Hyderabad"}},"phones":["7207086219,8639265619"]},"cafeData":{},"status":{"isEdited":true,"orderStatus":"processing","editedStatus":"edited","editedTimestamp":"2021-06-05T20:14:18"},"billing":{"bill":251.0,"incoming":0.0,"orderSpending":0.0,"total":420.0,"tax":37.72999954223633,"discount":0.0,"restaurantTradeDiscount":0.0,"paymentMethod":"Juspay","coupon":{"code":""},"packingCharge":0.0,"gst":37.73,"finalGPPrice":0.0,"restaurantDiscountHit":0.0,"restaurantOffersDiscount":0.0,"freebieDiscountHit":0.0},"delivery":{},"placing":{"prepTimePred":0.0},"customer":{"customerId":"803858","customerArea":"Jawahar Nagar","customerLat":"17.43979926419798","customerLng":"78.42712439596653","customerDistance":1.7,"customerComment":"","mobile":"8142174405","name":"Jyothirmai"},"cart":{"charges":{"vat":0.0,"serviceTax":0.0,"serviceCharge":0.0,"packingCharge":0.0,"deliveryCharge":0.0,"convenienceFee":0.0,"restaurantTaxationType":"GST","gst":37.73,"gstDetails":{"cartCGST":5.974999904632568,"cartIGST":0.0,"cartSGST":5.974999904632568,"itemCGST":5.974999904632568,"itemIGST":0.0,"packagingCGST":0.0,"packagingSGST":0.0},"taxExpressions":{"cgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","igst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","sgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","packagingCgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingSgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingIgst":"[TOTAL_PACKING_CHARGES]*0.0"}},"items":[{"itemId":"19946953","externalItemId":"11951930","quantity":1,"name":"Veg Grilled Sandwich","packingCharges":0.0,"restaurantDiscountHit":0.0,"subTotal":59.0,"total":59.0,"addons":[],"variants":[],"category":"Quick Bites","charges":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","gst":"2.95"},"taxExpressions":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","sgst":"[SUBTOTAL]*0.025","igst":"[SUBTOTAL]*0.0","cgst":"[SUBTOTAL]*0.025","gstInclusive":false},"gstDetails":{"cgst":1.475000023841858,"igst":0.0,"sgst":1.475000023841858},"isVeg":"1","subCategory":"Sandwiches","itemRestaurantOffersDiscount":0.0,"freeItemQuantity":0},{"itemId":"19946947","externalItemId":"11952371","quantity":2,"name":"Chicken Burger","packingCharges":0.0,"restaurantDiscountHit":0.0,"subTotal":180.0,"total":180.0,"addons":[{"choiceId":"8182824","externalChoiceId":"A2969742","addonTaxExpressions":{"sgst":"","igst":"","cgst":"","gstInclusive":false},"addonCharges":{"gst":"0.0"},"addonGstDetails":{"sgst":0.0,"cgst":0.0,"igst":0.0},"groupId":"2908594","name":"Cheese","price":10.0}],"variants":[],"category":"Quick Bites","charges":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","gst":"9.0"},"taxExpressions":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","sgst":"[SUBTOTAL]*0.025","igst":"[SUBTOTAL]*0.0","cgst":"[SUBTOTAL]*0.025","gstInclusive":false},"gstDetails":{"cgst":4.5,"igst":0.0,"sgst":4.5},"isVeg":"0","subCategory":"Burgers","itemRestaurantOffersDiscount":0.0,"freeItemQuantity":0}],"meals":[]},"orderInfo":{"isAssured":false},"deliveryFeeCouponBreakup":{"discountShare":{"restaurantDiscount":0.0}},"paymentTransaction":[{"transactionId":"com.swiggy-105978965545-1","paymentMeta":{"paymentQRUrl":""}}]}}`,
			topic: conf.EditedOrderConsumer.Topic,
		},
		{
			val:   `{"eventId": "2e97cca8-ba15-44a8-8005-d148ca9f01b8","eventTimestamp": "2021-05-22 12:48:05","eventType": "ORDER_CANCELLED","data": {"orderId": 101459363369,"restaurantId": 12895,"partnerType": 2,"status": "cancelled","cancelledAt": 1621657103000,"foodPrepared": false,"cancellationReason": "","cancellationSubReason": "289"}}`,
			topic: conf.OrderCancelConsumer.Topic,
		},
		{
			val:   `{"eventId":"9e4be81c-b272-41c7-bd30-311beab278a0","eventTimestamp":"2021-04-14 12:39:23","eventType":"NEW_ORDER","data":{"orderId":"101459363368","restaurantId":12895,"partnerType":3,"orderType":"regular","paymentTxnStatus":"success","orderedTime":"2021-04-14T12:39:23","orderMetaData":"null","restaurantDetails":{"lat":"19.2048361","lng":"72.97761860000003","name":"Hotel Swagath","partnerId":"12895","restaurantPartnerId":"12895","area":{"id":214,"name":"Thane","city":{"id":5,"name":"Mumbai"}},"phones":["02225479547,02225471291"]},"cafeData":{},"status":{"isEdited":false,"orderStatus":"processing","editedStatus":"unedited"},"billing":{"bill":180,"incoming":186,"orderSpending":0,"total":186,"tax":0,"discount":36,"restaurantTradeDiscount":36,"paymentMethod":"Cash","coupon":{"code":""},"packingCharge":0,"gst":0,"finalGPPrice":0,"restaurantDiscountHit":36,"restaurantOffersDiscount":0,"freebieDiscountHit":0},"delivery":{},"placing":{"prepTimePred":13.187229},"customer":{"customerId":"8781652","customerArea":"Thane West","customerLat":"19.203246","customerLng":"72.9744","customerDistance":0.6,"customerComment":"","mobile":"9769343960","name":"Sakshi Bhanushali "},"cart":{"charges":{"vat":0,"serviceTax":0,"serviceCharge":0,"packingCharge":0,"deliveryCharge":32,"convenienceFee":10,"restaurantTaxationType":"GST","gst":0,"gstDetails":{"cartCGST":0,"cartIGST":0,"cartSGST":0,"itemCGST":0,"itemIGST":0,"packagingCGST":0,"packagingSGST":0},"taxExpressions":{"cgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","igst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","sgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","packagingCgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingSgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingIgst":"[TOTAL_PACKING_CHARGES]*0.0"}},"items":[{"itemId":"6864013","mealQuantity":1,"externalItemId":"","quantity":1,"name":"Paneer Tikka Masala","packingCharges":0,"restaurantDiscountHit":36,"subTotal":180,"total":180,"addons":[],"variants":[],"category":"Indian","charges":{"vat":"0","serviceCharge":"0","serviceTax":"0","gst":"0"},"taxExpressions":{"vat":"0","serviceCharge":"0","serviceTax":"0","gstInclusive":false},"gstDetails":{"cgst":0,"igst":0,"sgst":0},"isVeg":"1","subCategory":"Paneer Main Course","itemRestaurantOffersDiscount":0,"freeItemQuantity":0}],"meals":[]},"orderInfo":{"isAssured":false},"deliveryFeeCouponBreakup":{"discountShare":{"restaurantDiscount":0}},"paymentTransaction":[{"transactionId":"101459363368","paymentMeta":{}}]}}`,
			topic: conf.NewOrderConsumer.Topic,
		},
		{
			val:   `{"eventId":"19dbbe2b-7277-41ba-afa3-3da9b748b630","eventTimestamp":"2021-05-22T16:12:46.867","eventType":"PLACING_STATE_TRANSITION","version":2,"data":{"orderId":"101459363368","restaurantId":12895,"fromState":"VENDOR_FULFILLED","toState":"CANCELLED","metadata":"{}","updatedBy":"VVO_PLACER"}}`,
			topic: conf.PlacingFSMConsumer.Topic,
		},
		{
			val:   `{"is_pop_order":false,"geo_details":{"restaurant_location":{"lat":"12.930831","lng":"77.6332112","latAsDouble":12.930831,"lngAsDouble":77.6332112},"customer_location":{"lat":"12.923584","lng":"77.606864","latAsDouble":12.923584,"lngAsDouble":77.606864},"city_id":"1","area_id":"1","area_name":"Koramangala","zone_id":"1"},"finance_details":{"system_pay":0,"system_bill":422,"system_collect":0},"banner_factor":0,"geo_fence_breached":false,"partner_name":"DELIVERY_PLATFORM","order_id":"101459363368","status":"reached","source":"APP","time":"2021-05-22 07:07:55","deliveryboy_id":"1220190","deDetails":{"id":"1220190","name":"Manoj Kumar Guppa","mobileNumber":"8431508067","altMobile":"8431508067","nick":"Manoj Kumar Guppa","imageUrl":"https://de-docs.s3.amazonaws.com/de-images/eoGHYmRtfJiFvLm2aounoJSk.jpeg","lat":"12.923671","lng":"77.6069739"},"restaurant_id":12895,"restaurant_name":"Sardarji Malai Chaap wale(Subhash Nagar)"}`,
			topic: conf.OrderStatusUpdateV2Consumer.Topic,
		},
		{
			val:   `{"eventId":"a3033868-879b-4df6-ae06-bb05b16e5d52","eventTimestamp":"2021-06-05 20:14:18","eventType":"ORDER_EDITED","data":{"orderId":"101459363369","restaurantId":72566,"partnerType":19,"orderType":"regular","paymentTxnStatus":"success","orderedTime":"2021-06-05T20:06:05","orderMetaData":"null","restaurantDetails":{"lat":"17.4297917","lng":"78.427618","name":"Eat n Joy Bakers","partnerId":"kczp4vyh","restaurantPartnerId":"kczp4vyh","area":{"id":163,"name":"Ameerpet","city":{"id":3,"name":"Hyderabad"}},"phones":["7207086219,8639265619"]},"cafeData":{},"status":{"isEdited":true,"orderStatus":"processing","editedStatus":"edited","editedTimestamp":"2021-06-05T20:14:18"},"billing":{"bill":251.0,"incoming":0.0,"orderSpending":0.0,"total":420.0,"tax":37.72999954223633,"discount":0.0,"restaurantTradeDiscount":0.0,"paymentMethod":"Juspay","coupon":{"code":""},"packingCharge":0.0,"gst":37.73,"finalGPPrice":0.0,"restaurantDiscountHit":0.0,"restaurantOffersDiscount":0.0,"freebieDiscountHit":0.0},"delivery":{},"placing":{"prepTimePred":0.0},"customer":{"customerId":"803858","customerArea":"Jawahar Nagar","customerLat":"17.43979926419798","customerLng":"78.42712439596653","customerDistance":1.7,"customerComment":"","mobile":"8142174405","name":"Jyothirmai"},"cart":{"charges":{"vat":0.0,"serviceTax":0.0,"serviceCharge":0.0,"packingCharge":0.0,"deliveryCharge":0.0,"convenienceFee":0.0,"restaurantTaxationType":"GST","gst":37.73,"gstDetails":{"cartCGST":5.974999904632568,"cartIGST":0.0,"cartSGST":5.974999904632568,"itemCGST":5.974999904632568,"itemIGST":0.0,"packagingCGST":0.0,"packagingSGST":0.0},"taxExpressions":{"cgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","igst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","sgst":"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0","packagingCgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingSgst":"[TOTAL_PACKING_CHARGES]*0.0","packagingIgst":"[TOTAL_PACKING_CHARGES]*0.0"}},"items":[{"itemId":"19946953","externalItemId":"11951930","quantity":1,"name":"Veg Grilled Sandwich","packingCharges":0.0,"restaurantDiscountHit":0.0,"subTotal":59.0,"total":59.0,"addons":[],"variants":[],"category":"Quick Bites","charges":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","gst":"2.95"},"taxExpressions":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","sgst":"[SUBTOTAL]*0.025","igst":"[SUBTOTAL]*0.0","cgst":"[SUBTOTAL]*0.025","gstInclusive":false},"gstDetails":{"cgst":1.475000023841858,"igst":0.0,"sgst":1.475000023841858},"isVeg":"1","subCategory":"Sandwiches","itemRestaurantOffersDiscount":0.0,"freeItemQuantity":0},{"itemId":"19946947","externalItemId":"11952371","quantity":2,"name":"Chicken Burger","packingCharges":0.0,"restaurantDiscountHit":0.0,"subTotal":180.0,"total":180.0,"addons":[{"choiceId":"8182824","externalChoiceId":"A2969742","addonTaxExpressions":{"sgst":"","igst":"","cgst":"","gstInclusive":false},"addonCharges":{"gst":"0.0"},"addonGstDetails":{"sgst":0.0,"cgst":0.0,"igst":0.0},"groupId":"2908594","name":"Cheese","price":10.0}],"variants":[],"category":"Quick Bites","charges":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","gst":"9.0"},"taxExpressions":{"vat":"0","serviceCharge":"0.00","serviceTax":"0","sgst":"[SUBTOTAL]*0.025","igst":"[SUBTOTAL]*0.0","cgst":"[SUBTOTAL]*0.025","gstInclusive":false},"gstDetails":{"cgst":4.5,"igst":0.0,"sgst":4.5},"isVeg":"0","subCategory":"Burgers","itemRestaurantOffersDiscount":0.0,"freeItemQuantity":0}],"meals":[]},"orderInfo":{"isAssured":false},"deliveryFeeCouponBreakup":{"discountShare":{"restaurantDiscount":0.0}},"paymentTransaction":[{"transactionId":"com.swiggy-105978965545-1","paymentMeta":{"paymentQRUrl":""}}]}}`,
			topic: conf.EditedOrderConsumer.Topic,
		},
		{
			val:   `{"eventId": "2e97cca8-ba15-44a8-8005-d148ca9f01b8","eventTimestamp": "2021-05-22 12:48:05","eventType": "ORDER_CANCELLED","data": {"orderId":"101459363369","restaurantId": 12895,"partnerType": 2,"status": "cancelled","cancelledAt": 1621657103000,"foodPrepared": false,"cancellationReason": "","cancellationSubReason": "289"}}`,
			topic: conf.OrderCancelConsumer.Topic,
		},
	}

	for i := range testMessages {
		message := kafka.Message{Topic: testMessages[i].topic, Value: testMessages[i].val, Headers: map[string]string{}}
		if err := k.Produce(message); err != nil {
			t.Errorf("Error producing message = %v, error = %v", message, err)
		}
		t.Log("successfully produced messages")
	}

}

func TestOrderSubscription(t *testing.T) {

	container, err := wiremock.NewContainer()
	assert.Nil(t, err, "error initialising wiremock")
	mocks.MockVendorAuthSuccessResponse(t, container)

	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	InitializingBigCache(t)
	kafkaContainer := initKafkaTestContainer(t)
	consumer.StartAllConsumers(redisClient)

	resolvers := graph.GraphqlHandler(redisClient)
	c := client.New(graph.GetHeadersMiddleware(resolvers))
	q := `subscription{
	    orderUpdates(restIds:[12895]){
	    restaurantId
	    orderId
	    timeStamp
	    event
	    }}`

	var msg struct {
		resp struct {
			OrderUpdates struct {
				RestaurantID int64
				OrderID      int64
				TimeStamp    string
				Event        string
			}
		}
		err error
	}

	initPayload := map[string]interface{}{constants.AccessToken: "dummy"}
	subscription := c.WebsocketWithPayload(q, initPayload)
	defer subscription.Close()
	time.Sleep(5 * time.Second)
	ProduceEvents(kafkaContainer, t)
	i := 0
	for {
		msg.err = subscription.Next(&msg.resp)
		if msg.err == nil && int64(12895) == msg.resp.OrderUpdates.RestaurantID {
			assert.Equal(t, int64(12895), msg.resp.OrderUpdates.RestaurantID)
			assert.Equal(t, int64(101459363368), msg.resp.OrderUpdates.OrderID)
			assert.Equal(t, "NEW_ORDER", msg.resp.OrderUpdates.Event)
			break
		} else if i > 2 {
			t.Error(msg.err)
			break
		}
		i = i + 1

	}

	subWithoutAuth := c.Websocket(q)
	msg.err = subWithoutAuth.Next(&msg.resp)
	assert.NotNil(t, msg.err, "Error! Connection was established with empty auth token")

}
func InitializingBigCache(t *testing.T) {

	URL := conf.VendorConfigURL + conf.VendorConfigWebSocketPath
	mockData, err := ioutil.ReadFile("../mocks/data/vendor_config_websocket_success.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("GET", URL,
		httpmock.NewBytesResponder(200, mockData))
	httpmock.Activate()
	defer httpmock.Deactivate()
	big_cache.InitializeWebsocketCache()

	// cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(8400 * time.Hour))
	// if err != nil {
	// 	t.Errorf("Error While Initalising BigCache %v", err.Error())
	// }

	// cache.Set("WS:5290", []byte("1"))

}
