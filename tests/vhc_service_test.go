package tests

import (
	"testing"

	grpcTest "bitbucket.org/swigy/oh-my-test-helper/golang/pkg/grpc"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/graph/services/rms_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_service"
	"bitbucket.org/swigy/vhc-composer/mocks"
	pb "bitbucket.org/swigy/vhc-service/pkg/proto"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
)

func TestGetNode(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.DeactivateAndReset()

	container, err := grpcTest.NewContainer("7778", func(server *grpc.Server) {
		pb.RegisterVhcServiceServer(server, &mocks.UnimplementedVhcServer{})
	})
	if err != nil {
		t.Errorf("Error Initializing container. Error = %+v", err)
	}
	conf.VhcServiceURL = "localhost:7778"
	var (
		request = &grpcTest.Request{
			Package: "vhcAdminContract",
			Service: "vhcService",
			Method:  "GetNode",
		}
		expected = mocks.FetchDummyNode()
		want     = &grpcTest.Response{
			Data: &expected,
			Err:  nil,
		}
	)

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	resolvers := graph.Resolver{
		VHCService: vhc_service.NewVHCService(),
		RMSService: rms_service.NewRMSService(),
	}
	c := client.New(graph.GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	q := `
	query GetNode{
		GetNode(id:"0", dependencies:[{key:"OUTLET_ID", value:"12895",type:STRING}]){
		  id,issues{id,is_leaf,title,sub_title,dependencies},is_leaf,title,sub_title
	}}`

	var resp struct {
		GetNode *model.GetNodeResponse `json:"GetNode"`
	}
	err = c.Post(q, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}
	t.Logf("Response: %v", resp)

	t.Logf("Out: %v \n expected:%v", resp, expected)
	assert.Equal(t, expected.Id, resp.GetNode.ID)
	assert.Equal(t, expected.IsLeaf, resp.GetNode.IsLeaf)
	assert.Equal(t, expected.Title, resp.GetNode.Title)
	assert.Equal(t, expected.SubTitle, resp.GetNode.SubTitle)

	// Count Test
	count, err := container.GetRequestCount(request)
	if err != nil || count != 1 {
		t.Errorf("Want Request Count=1, got=%v, Error=%v", count, err)
	}
}
