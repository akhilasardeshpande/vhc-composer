package tests

import (
	"fmt"
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/go-redis/redis/v8"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestContentResolution(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.HeadlessCMSBaseURL + conf.HeadlessCMSContentResolutionPath
	mockData, err := ioutil.ReadFile("../mocks/data/hcms_success_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}

	httpmock.RegisterResponder("GET", URL,
		httpmock.NewBytesResponder(200, mockData))

	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	resolvers := graph.GraphqlHandler(redisClient)
	c := client.New(graph.GetHeadersMiddleware(resolvers))

	query := `query space {
  resolveByUserIdAndSpaceId(input: {userId: "1", spaceId: "1"}) {
    type
  }
}`

	var resp interface{}
	err = c.Post(query, &resp, client.AddHeader(sessionKey, ""))
	RespMap := resp.(map[string]interface{})
	data := RespMap["resolveByUserIdAndSpaceId"]
	dataString := data.(map[string]interface{})["type"].(string)
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, "space", dataString, "spaceId matching failed")
}

func TestFeedback(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	url := conf.HeadlessCMSBaseURL + conf.HeadlessCMSFeedbackApi
	mockData, err := ioutil.ReadFile("../mocks/data/hcms_feedback_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", url, httpmock.NewBytesResponder(200, mockData))
	redisContainer := InitRedisContainer(t)
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port()),
	})
	resolvers := graph.GraphqlHandler(redisClient)
	c := client.New(graph.GetHeadersMiddleware(resolvers))
	query := `mutation feedback{
				feedback(input: {userId: "123", contentId: "234"}) {
					contentId
				}
			}`
	var resp struct {
		Data *model.FeedbackResp `json:"feedback"`
	}
	err = c.Post(query, &resp, client.AddHeader(sessionKey, ""))
	if err != nil {
		t.Error(err)
	}
	require.Equal(t, "123-456", resp.Data.ContentID, "contentId did not match")
}
