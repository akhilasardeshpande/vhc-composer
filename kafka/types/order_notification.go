package types

import (
	"encoding/json"
)

type OrderNotification struct {
	RestaurantID int64  `json:"restaurantId"`
	OrderID      int64  `json:"orderId"`
	Event        string `json:"event"`
	TimeStamp    string `json:"timeStamp"`
}

func (n OrderNotification) MarshalBinary() ([]byte, error) {
	return json.Marshal(n)
}
