package types

import (
	"fmt"
	"strconv"

	"bitbucket.org/swigy/vhc-composer/utils"
)

type PlacingFSMEvent struct {
	EventID        string              `json:"eventId"`
	EventType      string              `json:"eventType"`
	EventTimestamp string              `json:"eventTimestamp"`
	Version        int                 `json:"version"`
	Data           PlacingFSMEventData `json:"data"`
}
type PlacingFSMEventData struct {
	OrderId      interface{} `json:orderId`
	RestaurantId int64       `json:"restaurantId"`
	FromState    string      `json:"fromState"`
	ToState      string      `json:"toState"`
	UpdatedBy    string      `json:"updatedBy`
}

func (p *PlacingFSMEvent) NotificationMapper() (OrderNotification, error) {

	var orderID int64
	var err error
	switch order := p.Data.OrderId.(type) {
	case int64:
		orderID = order
	case string:
		orderID, err = strconv.ParseInt(order, 10, 64)
		if err != nil {
			return OrderNotification{}, fmt.Errorf("error while parsing string to int64 : %v", err.Error())
		}
	case float64:
		orderID = int64(order)
	default:
		return OrderNotification{}, fmt.Errorf("type %T not supported for OrderID", order)

	}
	return OrderNotification{p.Data.RestaurantId, orderID, p.EventType, utils.ConvertToRMSTimeStampFormat(p.EventTimestamp)}, nil
}
