package types

import (
	"fmt"
	"strconv"
)

import (
	"bitbucket.org/swigy/vhc-composer/utils"
)

const deliveryEventType = "ORDER_STATUS_UPDATE_DELIVERY"

type DeliveryEvent struct {
	OrderId        string `json:"order_id"`
	RestaurantId   int64  `json:"restaurant_id"`
	EventTimestamp string `json:"time"`
}

func (d *DeliveryEvent) NotificationMapper() (OrderNotification, error) {
	OrderID, err := strconv.ParseInt(d.OrderId, 10, 64)
	if err != nil {
		return OrderNotification{}, fmt.Errorf("unable to parse orderid %s", d.OrderId)
	}

	return OrderNotification{d.RestaurantId, OrderID, deliveryEventType, utils.ConvertToRMSTimeStampFormat(d.EventTimestamp)}, nil
}
