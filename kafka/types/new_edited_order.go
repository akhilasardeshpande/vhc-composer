package types

import (
	"bitbucket.org/swigy/vhc-composer/utils"
)

type NewAndEditedOrderEvent struct {
	EventID        string         `json:"eventId"`
	EventType      string         `json:"eventType"`
	EventTimestamp string         `json:"eventTimestamp"`
	Data           NewEditedOrder `json:"data"`
}
type NewEditedOrder struct {
	OrderId      int64 `json:"orderId"`
	RestaurantId int64 `json:"restaurantId"`
}

func (n *NewAndEditedOrderEvent) NotificationMapper() OrderNotification {
	return OrderNotification{n.Data.RestaurantId, n.Data.OrderId, n.EventType, utils.ConvertToRMSTimeStampFormat(n.EventTimestamp)}

}
