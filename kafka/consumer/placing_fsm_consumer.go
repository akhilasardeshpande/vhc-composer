package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"bitbucket.org/swigy/vhc-composer/cache/big_cache"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/kafka/types"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/go-redis/redis/v8"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type PlacingFSMEventHandler struct {
	RedisClient redis.UniversalClient
}

func (handler PlacingFSMEventHandler) Handle(record *swgykafka.Record) (swgykafka.Status, error) {
	defer metric.Consumer.MeasureConsumerLatency(record.Topic, time.Now())
	txn := metric.TraceTransaction(placingFSMConsumer)
	defer metric.CloseTransaction(txn)
	var PlacingFSM types.PlacingFSMEvent

	log.WithFields(log.Fields{"Record": string(record.Value), "Topic": record.Topic}).Info("New message on  PlacingFSM StateTransition")
	err := json.Unmarshal(record.Value, &PlacingFSM)
	if err != nil {
		log.WithError(err).Error("Failed to unmarshal event in PlacingFSMEventHandler")
		return swgykafka.HardFailure, err
	}
	stream := fmt.Sprintf("%d", PlacingFSM.Data.RestaurantId)
	logFields := log.Fields{constants.RestID: PlacingFSM.Data.RestaurantId, constants.OrderID: PlacingFSM.Data.OrderId}

	if !big_cache.WebSocketEnabled(stream) {
		log.WithFields(logFields).Info("OrderSubscription Feature Gate not enabled on the Restaurant")
		return swgykafka.Success, nil
	}

	ordernotification, err := PlacingFSM.NotificationMapper()
	if err != nil {
		log.Errorf("Error While mapping PlacingFSM data to  Notificaton %v", err.Error())
		return swgykafka.HardFailure, err
	}
	value, _ := ordernotification.MarshalBinary()
	ctx := newrelic.NewContext(context.Background(), nil)
	str := handler.RedisClient.XAdd(ctx, &redis.XAddArgs{
		Stream: stream,
		MaxLen: maxlenstream,
		ID:     "*",
		Values: map[string]interface{}{
			"data": value,
		},
	})

	if str.Err() != nil {
		log.WithError(str.Err()).Errorf("Failed to add data into the redis stream")
		return swgykafka.HardFailure, str.Err()
	}
	log.WithFields(logFields).Infof("redis command %v", str)
	metric.Consumer.IncrementSuccessCounter(ordernotification.Event)
	return swgykafka.Success, nil
}
