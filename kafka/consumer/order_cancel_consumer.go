package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"bitbucket.org/swigy/vhc-composer/cache/big_cache"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/kafka/types"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/go-redis/redis/v8"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type OrderCancelEventHandler struct {
	RedisClient redis.UniversalClient
}

func (handler OrderCancelEventHandler) Handle(record *swgykafka.Record) (swgykafka.Status, error) {

	defer metric.Consumer.MeasureConsumerLatency(record.Topic, time.Now())
	txn := metric.TraceTransaction(cancelOrderConsumer)
	defer metric.CloseTransaction(txn)
	var cancelOrderEvent types.CancelOrderEvent
	log.WithFields(log.Fields{"Record": string(record.Value), "Topic": record.Topic}).Info("New message on OrderCancelEventHandler")
	err := json.Unmarshal(record.Value, &cancelOrderEvent)
	if err != nil {
		log.WithError(err).Errorf("Failed to unmarshal event in OrderCancelEventHandler ")
		return swgykafka.HardFailure, err
	}
	logFields := log.Fields{constants.RestID: cancelOrderEvent.Data.RestaurantId, constants.OrderID: cancelOrderEvent.Data.OrderId}
	stream := fmt.Sprintf("%d", cancelOrderEvent.Data.RestaurantId)
	if !big_cache.WebSocketEnabled(stream) {
		log.WithFields(logFields).Info("OrderSubscription Feature Gate not enabled on the Restaurant")
		return swgykafka.Success, nil
	}
	ordernotification := cancelOrderEvent.NotificationMapper()
	value, _ := ordernotification.MarshalBinary()

	ctx := newrelic.NewContext(context.Background(), nil)
	str := handler.RedisClient.XAdd(ctx, &redis.XAddArgs{
		Stream: stream,
		MaxLen: maxlenstream,
		ID:     "*",
		Values: map[string]interface{}{
			"data": value,
		},
	})
	if str.Err() != nil {
		log.WithError(str.Err()).Errorf("Failed to add data into the redis stream")
		return swgykafka.HardFailure, str.Err()
	}
	log.WithFields(logFields).Infof("redis command %v", str)
	metric.Consumer.IncrementSuccessCounter(ordernotification.Event)

	return swgykafka.Success, nil
}
