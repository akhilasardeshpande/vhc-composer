package consumer

import (
	"errors"

	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

const (
	maxlenstream                = 5
	newAndEditedOrderConsumer   = "NEW_AND_EDITED_ORDER_CONSUMER"
	cancelOrderConsumer         = "CANCEL_ORDER_CONSUMER"
	orderStatusUpdateV2Consumer = "ORDER_STATUS_UPDATE_V2_CONSUMER"
	placingFSMConsumer          = "PLACING_FSM_CONSUMER"
)

func NewConsumer(consumerConfig *conf.KafkaConsumerConfig, handler swgykafka.ConsumerHandler) (*swgykafka.Consumer, error) {
	if consumerConfig.PrimaryCluster == nil {
		return nil, errors.New("primary cluster cannot be nil")
	}

	// Build Cluster
	primary, err := swgykafka.NewClusterBuilder(consumerConfig.PrimaryCluster.Host).AuthMechanism(consumerConfig.PrimaryCluster.Auth).
		UserName(consumerConfig.PrimaryCluster.Username).Password(consumerConfig.PrimaryCluster.Password).Build()
	if err != nil {
		return nil, err
	}
	var secondary *swgykafka.Cluster
	if consumerConfig.SecondaryCluster != nil {
		secondary, err = swgykafka.NewClusterBuilder(consumerConfig.SecondaryCluster.Host).AuthMechanism(consumerConfig.SecondaryCluster.Auth).
			UserName(consumerConfig.SecondaryCluster.Username).Password(consumerConfig.SecondaryCluster.Password).Build()
		if err != nil {
			return nil, err
		}
	} else {
		secondary = nil
	}

	// Build Topic
	t, err := swgykafka.NewTopicBuilder(consumerConfig.Topic).Build()
	if err != nil {
		return nil, err
	}

	consConfig, err := swgykafka.NewConsumerConfigBuilder(primary, secondary, consumerConfig.PrimaryCluster.ClientId, t, consumerConfig.ConsumerGroup).
		EnableAutoCommit(true).PollTimeoutInMs(consumerConfig.PollTimeout).
		AutoOffsetReset(consumerConfig.AutoOffsetReset).Build()
	if err != nil {
		return nil, err
	}

	// Create Consumer Instance
	c, err := swgykafka.NewConsumer(*consConfig, handler, nil)

	return c, err
}

func StartAllConsumers(client redis.UniversalClient) {
	StartNewOrderConsumer(client)
	StartPlacingFSMConsumer(client)
	StartOrderCancelConsumer(client)
	StartOrderStatusUpdateV2Consumer(client)
	StartEditedOrderConsumer(client)

}

func StartEditedOrderConsumer(client redis.UniversalClient) {
	c, err := NewConsumer(&conf.EditedOrderConsumer, NewOrderEventHandler{RedisClient: client})
	logConsumerConfig("EDITED_ORDER_CONSUMER", conf.NewOrderConsumer)
	if err != nil {
		log.Errorf("error occurred in creating EditedOrder polling consumer : %v", err)
		return
	}
	err = c.Start()
	if err != nil {
		log.Errorf("error occurred in starting EditedOrder polling consumer : %v", err)
		return
	}

	log.Info("started EditedOrder consumer")
}

func StartNewOrderConsumer(client redis.UniversalClient) {
	c, err := NewConsumer(&conf.NewOrderConsumer, NewOrderEventHandler{RedisClient: client})
	logConsumerConfig("NEW_ORDER_CONSUMER", conf.NewOrderConsumer)

	if err != nil {
		log.Errorf("error occurred in creating NewOrder polling consumer : %v", err)
		return
	}
	err = c.Start()
	if err != nil {
		log.Errorf("Error occurred in starting NewOrder polling consumer : %v", err)
		return
	}
	log.Info("started NewOrder consumer")
}
func StartPlacingFSMConsumer(client redis.UniversalClient) {
	c, err := NewConsumer(&conf.PlacingFSMConsumer, PlacingFSMEventHandler{RedisClient: client})
	logConsumerConfig(placingFSMConsumer, conf.PlacingFSMConsumer)

	if err != nil {
		log.Errorf("error occurred in creating PlacingFSM polling consumer : %v", err)
		return
	}
	err = c.Start()
	if err != nil {
		log.Errorf("Error occurred in starting PlacingFSM polling consumer : %v", err)
		return
	}

	log.Info("started PlacingFSM consumer")
}
func StartOrderCancelConsumer(client redis.UniversalClient) {
	c, err := NewConsumer(&conf.OrderCancelConsumer, OrderCancelEventHandler{RedisClient: client})
	logConsumerConfig(cancelOrderConsumer, conf.OrderCancelConsumer)
	if err != nil {
		log.Errorf("error occurred in creating OrderCancel Event consumer :%v", err)
		return
	}
	err = c.Start()
	if err != nil {
		log.Errorf("Error occurred in starting OrderCancel polling consumer : %v", err)
		return
	}

	log.Info("started OrderCancel consumer")
}
func StartOrderStatusUpdateV2Consumer(client redis.UniversalClient) {
	c, err := NewConsumer(&conf.OrderStatusUpdateV2Consumer, OrderStatusUpdateV2EventHandler{RedisClient: client})
	logConsumerConfig(orderStatusUpdateV2Consumer, conf.OrderStatusUpdateV2Consumer)
	if err != nil {
		log.Errorf("error occurred in creating OrderStatusUpdateV2 event consumer :%v", err)
		return
	}
	err = c.Start()
	if err != nil {
		log.Errorf("Error occurred in starting OrderStatusUpdateV2 polling consumer : %v", err)
		return
	}

	log.Info("started OrderStatusUpdateV2 consumer")
}
func logConsumerConfig(consumerName string, consumerConfig conf.KafkaConsumerConfig) {
	logfields := log.Fields{"Topic": consumerConfig.Topic, "PollTimeout": consumerConfig.PollTimeout, "ConsumerGroup": consumerConfig.ConsumerGroup, "PrimaryCluster": consumerConfig.PrimaryCluster, "SecondaryCluster": consumerConfig.SecondaryCluster}
	log.WithFields(logfields).Infof("Creating %s", consumerName)

}
