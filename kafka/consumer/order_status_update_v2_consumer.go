package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"bitbucket.org/swigy/vhc-composer/cache/big_cache"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/kafka/types"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/go-redis/redis/v8"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type OrderStatusUpdateV2EventHandler struct {
	RedisClient redis.UniversalClient
}

func (handler OrderStatusUpdateV2EventHandler) Handle(record *swgykafka.Record) (swgykafka.Status, error) {
	defer metric.Consumer.MeasureConsumerLatency(record.Topic, time.Now())
	txn := metric.TraceTransaction(orderStatusUpdateV2Consumer)
	defer metric.CloseTransaction(txn)
	var DeliveryUpdate types.DeliveryEvent
	err := json.Unmarshal(record.Value, &DeliveryUpdate)
	log.WithFields(log.Fields{"Record": string(record.Value), "Topic": record.Topic}).Info("New message on  OrderStatusUpdateV2EventHandler")
	if err != nil {
		log.WithError(err).Error("Failed to unmarshal event in OrderStatusUpdateV2EventHandler")
		return swgykafka.HardFailure, err
	}
	if DeliveryUpdate.RestaurantId == 0 {
		log.WithFields(log.Fields{constants.OrderID: DeliveryUpdate.OrderId}).Info("Rejecting this event as event doesnot have valid RID")
		return swgykafka.Success, err
	}
	logFields := log.Fields{constants.RestID: DeliveryUpdate.RestaurantId, constants.OrderID: DeliveryUpdate.OrderId}
	stream := fmt.Sprintf("%d", DeliveryUpdate.RestaurantId)

	if !big_cache.WebSocketEnabled(stream) {
		log.WithFields(logFields).Info("OrderSubscription Feature Gate not enabled on the Restaurant")
		return swgykafka.Success, nil
	}

	ordernotification, err := DeliveryUpdate.NotificationMapper()
	if err != nil {
		log.Errorf("Error While mapping Delivery Data to Order Notificaton %v", err.Error())
		return swgykafka.HardFailure, err
	}
	value, _ := ordernotification.MarshalBinary()
	ctx := newrelic.NewContext(context.Background(), nil)
	str := handler.RedisClient.XAdd(ctx, &redis.XAddArgs{
		Stream: stream,
		MaxLen: maxlenstream,
		ID:     "*",
		Values: map[string]interface{}{
			"data": value}})
	if str.Err() != nil {
		log.WithError(str.Err()).Errorf("Failed to add data into the redis stream")
		return swgykafka.HardFailure, str.Err()
	}

	log.WithFields(logFields).Infof("redis command %v", str)
	metric.Consumer.IncrementSuccessCounter(ordernotification.Event)
	return swgykafka.Success, nil
}
