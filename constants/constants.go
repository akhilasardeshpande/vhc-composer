package constants

import "time"

const (
	AccessToken                  = "access_token"
	AuthorisationFailed          = "Authorisation Failed"
	AuthFailedMissingFields      = "Authentication Failed due to missing feilds"
	InvalidOrUnauthrisedOutletID = "Invalid Outlet ID/Unauthorised"
	InvalidGetTicketsQuery       = "Query should not contain cf_restaurant_id"
	InvalidOutletID              = "Invalid OutletID"
	InvalidResponseFromUpstream  = "Invalid Response From Upstream Server"
	UserAgent                    = "user-agent"
	PermissionNoneForRMSAuth     = 0
	BusinessMetricsPermission    = 4
	ContentType                  = "Content-Type"
	ContentVal                   = "application/json"
	SuccessStatusCode            = 1
	ReqFailed                    = "failure"
	PeriodConst                  = "period"
	RestIdsConst                 = "restIds"
	HttpGetRequest               = "GET"
	HttpPostRequest              = "POST"
	TicketID                     = "Ticket ID"
	Status                       = "Status"
	StatusCode                   = "StatusCode"
	RestID                       = "Restaurant ID"
	RestIDs                      = "Restaurant IDs"
	OrderID                      = "Order ID"
	Page                         = "page"
	Query                        = "Query"
	CfRestaurantID               = "cf_restaurant_id"
	RMSTimeFormat                = "2006-01-02T15:04:05-0700"
	TimeZone                     = "+0530"
	Production                   = "production"
	Development                  = "development"
	UserID                       = "user-id"
	DeviceID                     = "device-id"
	RequestID                    = "request-id"
	VendorAuthTimeout            = 1500 * time.Millisecond
	VendorAuthMaxRetry           = 3
	RMSMaxRetry                  = 3
	RMSTimeout                   = 2000 * time.Millisecond
	UserMetaHeader               = "user_meta"
	SRSValidationError           = "Validation Error"
	SRSMaxRetry                  = 3
	SRSTimeout                   = 3000 * time.Millisecond
	SubContextKey                = "subContext"
	KeepAlivePingInterval        = 9500 * time.Millisecond
)

type Source int

const (
	PartnerApp Source = iota
	OwnerApp
	VMS
)

func (index Source) String() string {
	return [...]string{"PartnerApp", "OwnerApp", "VMS"}[index]
}

type Platform int

const (
	Android Platform = iota
	IOS
	Web
)

func (index Platform) String() string {
	return [...]string{"android", "ios", "web"}[index]
}
