module bitbucket.org/swigy/vhc-composer

go 1.15

require (
	bitbucket.org/swigy/kafka-client-go v0.1.1-0.20210926070736-98356a1d0729
	bitbucket.org/swigy/oh-my-test-helper/golang v0.0.0-20210820102948-10783031e9be
	bitbucket.org/swigy/vhc-service v0.0.0-20210603081842-4c525f92c761
	bitbucket.org/swigy/vhc-ticketing v0.0.0-20210302053211-7665c6d163ea
	github.com/99designs/gqlgen v0.12.0
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/allegro/bigcache/v2 v2.2.5 // indirect
	github.com/allegro/bigcache/v3 v3.0.0
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/getsentry/sentry-go v0.11.0 // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.11.3
	github.com/gojek/heimdall/v7 v7.0.2
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/jarcoal/httpmock v1.0.7
	github.com/mileusna/useragent v1.0.2
	github.com/mitchellh/mapstructure v1.4.1
	github.com/newrelic/go-agent/v3 v3.10.0
	github.com/newrelic/go-agent/v3/integrations/nrgrpc v1.1.0
	github.com/onrik/logrus v0.8.0
	github.com/prometheus/client_golang v1.8.0
	github.com/prometheus/common v0.15.0
	github.com/rs/cors v1.6.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	golang.org/x/tools v0.1.3 // indirect
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.1.0 // indirect
)
