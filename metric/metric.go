package metric

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const NameSpace = "vendor"
const SubSystem = "vhc_composer"

var Registry *prometheus.Registry
var Api *ApiMetrics
var ExternalApi *ApiMetrics
var Consumer *ConsumerMetrics

func init() {
	Registry = prometheus.NewRegistry()
	Api = newAPIMetrics()
	ExternalApi = newExternalAPIMetrics()
	Consumer = newConsumerMetrics()

}

type ApiMetrics struct {
	counter              *prometheus.CounterVec
	histogram            *prometheus.HistogramVec
	activeRequestCounter *prometheus.GaugeVec
}
type ConsumerMetrics struct {
	eventCounter        *prometheus.CounterVec
	histogram           *prometheus.HistogramVec
	subscriptionCounter *prometheus.GaugeVec
	subscription        *prometheus.HistogramVec
}

func (c *ApiMetrics) IncrementSuccessCounter(apiName string) {
	c.counter.WithLabelValues(apiName, "SUCCESS", "").Inc()
}

func (c *ApiMetrics) IncrementFailureCounter(apiName string, statusCode string) {
	c.counter.WithLabelValues(apiName, "FAILURE", statusCode).Inc()
}

func (c *ApiMetrics) IncrementActiveRequests() {
	c.activeRequestCounter.With(nil).Inc()
}
func (c *ApiMetrics) DecrementActiveRequests() {
	c.activeRequestCounter.With(nil).Dec()
}

func (c *ApiMetrics) IncrementCounter(apiName string, err error) {
	if err == nil {
		c.IncrementSuccessCounter(apiName)
	} else {
		c.IncrementFailureCounter(apiName, err.Error())
	}
}

func (c *ApiMetrics) MeasureLatency(apiName string, startTime time.Time) {
	d := time.Since(startTime)
	c.histogram.WithLabelValues(apiName).Observe(d.Seconds())
}

func newExternalAPIMetrics() *ApiMetrics {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "external_api_request_counter",
		Help:      "Count of external api calls made by vhc-composer",
	}, []string{"api_name", "status", "error_code"})

	histogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "external_api_request_latency",
		Help:      "Latency of external api calls made by vhc-composer",
	}, []string{"api_name"})

	Registry.MustRegister(counter)
	Registry.MustRegister(histogram)

	return &ApiMetrics{
		counter:   counter,
		histogram: histogram,
	}
}

func newAPIMetrics() *ApiMetrics {
	counter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "total_request",
		Help:      "Total number of received request",
	}, []string{"method_name", "status", "error_code"})

	histogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "requests_latency",
		Help:      "Requests Latency histogram",
		Buckets:   []float64{0.005, 0.010, 0.020, 0.035, 0.05, 0.075, 0.1, 0.15, 0.22, 0.33, 0.45, 0.65, 0.8, 1, 1.5, 2.2, 3, 4, 5, 6, 8, 10, 12},
	}, []string{"method_name"})

	activeRequestCounter := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "active_request_count",
		Help:      "Count of requests being processed",
	}, nil)

	Registry.MustRegister(counter)
	Registry.MustRegister(histogram)
	Registry.MustRegister(activeRequestCounter)

	return &ApiMetrics{
		counter:              counter,
		histogram:            histogram,
		activeRequestCounter: activeRequestCounter,
	}
}
func (c *ConsumerMetrics) IncrementSuccessCounter(eventName string) {
	c.eventCounter.WithLabelValues(eventName, "SUCCESS").Inc()
}

func (c *ConsumerMetrics) IncrementFailureCounter(eventName string) {
	c.eventCounter.WithLabelValues(eventName, "FAILURE").Inc()
}
func (c *ConsumerMetrics) IncrementRejectionCounter(eventName string) {
	c.eventCounter.WithLabelValues(eventName, "REJECTED").Inc()
}
func (c *ConsumerMetrics) MeasureSubscriptionPeriod(subscriptionName, userAgent string, startTime time.Time) {
	d := time.Since(startTime)
	c.subscription.WithLabelValues(subscriptionName, userAgent).Observe(d.Seconds())
}
func (c *ConsumerMetrics) MeasureConsumerLatency(topicName string, startTime time.Time) {
	d := time.Since(startTime)
	c.histogram.WithLabelValues(topicName).Observe(d.Seconds())
}
func (c *ConsumerMetrics) IncrementSubscriptionCounter(subscriptionName, userAgent string) {
	c.subscriptionCounter.WithLabelValues(subscriptionName, userAgent).Inc()
}
func (c *ConsumerMetrics) DecrementSubscriptionCounter(subscriptionName, userAgent string) {
	c.subscriptionCounter.WithLabelValues(subscriptionName, userAgent).Dec()
}

func newConsumerMetrics() *ConsumerMetrics {

	eventCounter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "event_counter",
		Help:      "Total number of events relayed",
	}, []string{"event_name", "status"})

	histogram := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "consumer_latency",
		Help:      "consumer Latency Histogram",
	}, []string{"topic_name"})

	subscriptionCounter := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "subscription_count",
		Help:      "Count of subscribers",
	}, []string{"subscription_name", "user_agent"})

	subscription := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: NameSpace,
		Subsystem: SubSystem,
		Name:      "subscription_time",
		Help:      "Subscription Time Period ",
		Buckets:   []float64{15, 30, 60, 120, 240, 480, 900, 1800, 3600, 7200},
	}, []string{"subscription_name", "user_agent"})

	Registry.MustRegister(eventCounter)
	Registry.MustRegister(histogram)
	Registry.MustRegister(subscriptionCounter)
	Registry.MustRegister(subscription)

	return &ConsumerMetrics{
		eventCounter:        eventCounter,
		histogram:           histogram,
		subscriptionCounter: subscriptionCounter,
		subscription:        subscription,
	}
}
