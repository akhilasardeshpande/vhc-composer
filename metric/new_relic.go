package metric

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/newrelic/go-agent/v3/newrelic"
	"github.com/prometheus/common/log"
)

var NewRelicApp *newrelic.Application

func init() {
	var err error
	newRelicApp, err := newrelic.NewApplication(
		newrelic.ConfigAppName(conf.NewRelicAppName),
		newrelic.ConfigLicense(conf.NewRelicKey),
		newrelic.ConfigDistributedTracerEnabled(conf.NewRelicEnableDT),
		//		newrelic.ConfigDebugLogger(os.Stdout),
	)
	if err != nil {
		panic(err)
	}
	log.Info("new relic initialized successfully")

	NewRelicApp = newRelicApp

}
func TraceTransaction(name string) *newrelic.Transaction {
	if NewRelicApp != nil {
		return NewRelicApp.StartTransaction(name)
	}
	return nil
}

func CloseTransaction(transaction *newrelic.Transaction) {
	if transaction == nil {
		return
	}
	transaction.End()
}
