package mocks

import (
	"context"
	"fmt"

	"bitbucket.org/swigy/vhc-composer/graph/model"
	pb "bitbucket.org/swigy/vhc-ticketing/proto"
	"github.com/stretchr/testify/mock"
)

// MockedVhcTicketing : TODO Doc
type MockedVhcTicketing struct {
	mock.Mock
}

// GetTicket : TODO Doc
func (d *MockedVhcTicketing) GetTicket(id string) (*model.Ticket, error) {
	args := d.Called(id)
	return args.Get(0).(*model.Ticket), args.Error(1)
}

// GetTickets : TODO Doc
func (d *MockedVhcTicketing) GetTickets(query string, page int64) (*model.TicketsRes, error) {
	args := d.Called(query, page)
	return args.Get(0).(*model.TicketsRes), args.Error(1)
}

// CreateTicket : TODO Doc
func (d *MockedVhcTicketing) CreateTicket(ticket model.CreateTicketInput) (*model.Ticket, error) {
	args := d.Called(ticket)
	return args.Get(0).(*model.Ticket), args.Error(1)
}

// UpdateTicket : TODO Doc
func (d *MockedVhcTicketing) UpdateTicketStatus(id string, status int) (*model.Ticket, error) {
	args := d.Called(id, status)
	return args.Get(0).(*model.Ticket), args.Error(1)
}

// GetTicketConversations : TODO Doc
func (d *MockedVhcTicketing) GetTicketConversations(id string) ([]*model.Conversation, error) {
	args := d.Called(id)
	return args.Get(0).([]*model.Conversation), args.Error(1)
}

// CreateTicketReply : TODO Doc
func (d *MockedVhcTicketing) CreateTicketReply(id string, reply model.TicketReplyInput) (*model.TicketReply, error) {
	args := d.Called(id, reply)
	return args.Get(0).(*model.TicketReply), args.Error(1)
}

// UnimplementedVhcTicketingServer : TODO Doc
type UnimplementedVhcTicketingServer struct {
}

// UpdateTickets : TODO Doc
func (*UnimplementedVhcTicketingServer) UpdateTickets(c context.Context, i *pb.UpdateTicketsReq) (*pb.UpdateTicketsRes, error) {
	return nil, fmt.Errorf("method UpdateTickets not implemented")
}

// GetTicket : TODO Doc
func (*UnimplementedVhcTicketingServer) GetTicket(c context.Context, i *pb.TicketReq) (*pb.Ticket, error) {
	return nil, fmt.Errorf("method GetTicket not implemented")
}

// GetTickets : TODO Doc
func (*UnimplementedVhcTicketingServer) GetTickets(c context.Context, i *pb.TicketsReq) (*pb.TicketsRes, error) {
	return nil, fmt.Errorf("method GetTickets not implemented")
}

// CreateTicket : TODO Doc
func (*UnimplementedVhcTicketingServer) CreateTicket(c context.Context, ticket *pb.CreateTicketReq) (*pb.Ticket, error) {
	return nil, fmt.Errorf("method CreateTicket not implemented")
}

// UpdateTicket : TODO Doc
func (*UnimplementedVhcTicketingServer) UpdateTicket(c context.Context, i *pb.UpdateTicketReq) (*pb.Ticket, error) {
	return nil, fmt.Errorf("method UpdateTicket not implemented")
}

// GetTicketConversations : TODO Doc
func (*UnimplementedVhcTicketingServer) GetTicketConversations(c context.Context, i *pb.TicketConversationReq) (*pb.TicketConversationRes, error) {
	return nil, fmt.Errorf("method GetTicketConversations not implemented")
}

// CreateTicketReply : TODO Doc
func (*UnimplementedVhcTicketingServer) CreateTicketReply(c context.Context, i *pb.TicketReplyReq) (*pb.TicketReplyRes, error) {
	return nil, fmt.Errorf("method CreateTicketReply not implemented")
}

// UpdateTicketConversation : TODO Doc
func (*UnimplementedVhcTicketingServer) UpdateTicketConversation(c context.Context, i *pb.UpdateTicketConversationReq) (*pb.Conversation, error) {
	return nil, fmt.Errorf("method UpdateTicketConversation not implemented")
}

// DeleteTicketConversation : TODO Doc
func (*UnimplementedVhcTicketingServer) DeleteTicketConversation(c context.Context, i *pb.DeleteTicketConversationReq) (*pb.DeleteTicketConversationRes, error) {
	return nil, fmt.Errorf("method DeleteTicketConversation not implemented")
}

// FetchDummyTicket : TODO Doc
func FetchDummyTicket() pb.Ticket {

	customFields := pb.CustomField{
		CfRestaurantId: 9990,
		CfPermission:   0,
	}

	return pb.Ticket{
		Id:              46,
		Subject:         "",
		Description:     "<div>for testing, demo ticket [Packaging]</div>",
		DescriptionText: "for testing, demo ticket [Packaging]",
		Type:            "Packaging",
		Status:          2,
		Priority:        1,
		CreatedAt:       "2021-01-21T23:16:47Z",
		UpdatedAt:       "2021-01-21T23:16:47Z",
		CustomFields:    &customFields,
	}
}

// FetchDummyTickets : TODO Doc
func FetchDummyTickets() pb.TicketsRes {
	return pb.TicketsRes{
		Results: []*pb.Ticket{
			{
				Id:              46,
				Subject:         "",
				Description:     "<div>for testing, demo ticket [Packaging]</div>",
				DescriptionText: "for testing, demo ticket [Packaging]",
				Type:            "Packaging",
				Status:          2,
				Priority:        1,
				CreatedAt:       "2021-01-21T23:16:47Z",
				UpdatedAt:       "2021-01-21T23:16:47Z",
			},
		},
		Total: 1,
	}
}

// FetchTicketConversation : TODO Doc
func FetchTicketConversation() pb.TicketConversationRes {
	return pb.TicketConversationRes{
		Conversations: []*pb.Conversation{
			{
				Id:       48,
				BodyText: "",
				Body:     "",
			},
		},
	}
}

// FetchDummyTicketReplyRes : TODO Doc
func FetchDummyTicketReplyRes() pb.TicketReplyRes {
	return pb.TicketReplyRes{
		Id:       48,
		Body:     "",
		BodyText: "",
	}
}
