package mocks

import (
	"fmt"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/wiremock"
	"bitbucket.org/swigy/vhc-composer/conf"
)

func readMockData(t *testing.T, container *wiremock.WireMock, relativePath string) string {
	conf.VendorAuthURL = fmt.Sprintf("http://%s:%s", container.Host(), container.Port())
	mockPath := GetAbsolutePath(t, relativePath)
	mockData, err := ioutil.ReadFile(mockPath)
	assert.Nil(t, err, "Failed to read mock data")

	return string(mockData)
}
func MockVendorAuthSuccessResponse(t *testing.T, container *wiremock.WireMock) {
	mockData := readMockData(t, container, "mocks/data/vendor_auth_success_resp.json")

	if err := container.CreateStub(&wiremock.Stub{
		Request: wiremock.Request{
			Method:  "GET",
			UrlPath: conf.VendorAuthGetSessionPath,
		},
		Response: wiremock.Response{
			Status: 200,
			Body:   mockData,
		},
	}); err != nil {
		t.Errorf("Error Creating Stub, error = %v", err)
	}
}

func MockVendorAuthInvalidSessionResponse(t *testing.T, container *wiremock.WireMock) {
	mockData := readMockData(t, container, "mocks/data/vendor_auth_invalid_sess_resp.json")

	if err := container.CreateStub(&wiremock.Stub{
		Request: wiremock.Request{
			Method:  "GET",
			UrlPath: conf.VendorAuthGetSessionPath,
		},
		Response: wiremock.Response{
			Status: 200,
			Body:   mockData,
		},
	}); err != nil {
		t.Errorf("Error Creating Stub, error = %v", err)
	}
}

func MockVendorAuthInvalidInputResponse(t *testing.T, container *wiremock.WireMock) {
	mockData := readMockData(t, container, "mocks/data/vendor_auth_invalid_input_resp.json")

	if err := container.CreateStub(&wiremock.Stub{
		Request: wiremock.Request{
			Method:  "GET",
			UrlPath: conf.VendorAuthGetSessionPath,
		},
		Response: wiremock.Response{
			Status: 200,
			Body:   mockData,
		},
	}); err != nil {
		t.Errorf("Error Creating Stub, error = %v", err)
	}
}

func MockVendorAuthInternalErrorResponse(t *testing.T, container *wiremock.WireMock) {
	mockData := readMockData(t, container, "mocks/data/vendor_auth_internal_err_resp.json")

	if err := container.CreateStub(&wiremock.Stub{
		Request: wiremock.Request{
			Method:  "GET",
			UrlPath: conf.VendorAuthGetSessionPath,
		},
		Response: wiremock.Response{
			Status: 200,
			Body:   mockData,
		},
	}); err != nil {
		t.Errorf("Error Creating Stub, error = %v", err)
	}
}

func MockVendorAuthCallFailure(t *testing.T, container *wiremock.WireMock) {
	conf.VendorAuthURL = fmt.Sprintf("http://%s:%s", container.Host(), container.Port())
	if err := container.CreateStub(&wiremock.Stub{
		Request: wiremock.Request{
			Method:  "GET",
			UrlPath: conf.VendorAuthGetSessionPath,
		},
		Response: wiremock.Response{
			Status: 503,
		},
	}); err != nil {
		t.Errorf("Error Creating Stub, error = %v", err)
	}
}

func MockVendorAuthResponse(t *testing.T) {
	httpmock.Activate()

	mockData, err := ioutil.ReadFile("../mocks/data/vendor_auth_success_resp.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("GET", conf.VendorAuthURL + conf.VendorAuthGetSessionPath, httpmock.NewBytesResponder(200, mockData))
}
