package mocks

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"context"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"testing"
)

type MockedSrsService struct {
	mock.Mock
}

func (d *MockedSrsService) UpdateSlots(ctx context.Context, restaurantID int, request []*model.UpdateSlotRequest) ([]*model.SlotsByDay, error) {
	args := d.Called(ctx, restaurantID, request)
	return args.Get(0).([]*model.SlotsByDay), args.Error(1)
}

func MockUpdateSlotResponse(t *testing.T) {
	URL := conf.SrsURL + conf.SrsTimeSlotPath + "bulk/" + "9990"
	mockData, err := ioutil.ReadFile("../mocks/data/srs_update_slot_success_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("PATCH", URL,
		httpmock.NewBytesResponder(200, mockData))
}

func MockGetSlotResponse(t *testing.T) {
	URL := conf.SrsURL + conf.SrsTimeSlotPath + "restaurantTimings"
	mockData, err := ioutil.ReadFile("../mocks/data/srs_get_slot_success_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))
}
