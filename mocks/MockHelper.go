package mocks

import (
	"github.com/stretchr/testify/assert"
	"os"
	"path"
	"strings"
	"testing"
)

// GetAbsolutePath returns the absolute path of a file given relative path from vhc-composer repo root.
func GetAbsolutePath(t *testing.T, relativePath string) string {
	workDirPath, err := os.Getwd()
	assert.Nil(t, err, "unable to get working directory path")

	//Get project path from working directory
	splitPaths := strings.SplitAfter(workDirPath, "vhc-composer/")
	assert.NotEqualf(t, 0, len(splitPaths), "unable to get project path")

	return path.Join(splitPaths[0], relativePath)
}
