package kafka

import (
	"context"
	"fmt"
	confluent "github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	"os"
	"strconv"
)

const (
	kafkaPort     nat.Port = "9093/tcp"
	brokerPort    nat.Port = "9092/tcp"
	zookeeperPort          = "2181"
)

type Kafka struct {
	container        testcontainers.Container
	host             string
	port             string
	bootstrapServers string
	adminClient      *confluent.AdminClient
	producer         *confluent.Producer
}

func (k *Kafka) Host() string {
	return k.host
}

func (k *Kafka) Port() string {
	return k.port
}

func (k *Kafka) BootstrapServers() string {
	return k.bootstrapServers
}

func (k *Kafka) Stop() error {
	return k.container.Terminate(context.Background())
}

func NewContainer() (*Kafka, error) {
	env := map[string]string{
		"KAFKA_LISTENERS":                        fmt.Sprintf("PLAINTEXT://0.0.0.0:%v,BROKER://0.0.0.0:%s", kafkaPort.Port(), brokerPort.Port()),
		"KAFKA_LISTENER_SECURITY_PROTOCOL_MAP":   "BROKER:PLAINTEXT,PLAINTEXT:PLAINTEXT",
		"KAFKA_INTER_BROKER_LISTENER_NAME":       "BROKER",
		"KAFKA_BROKER_ID":                        "1",
		"KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR": "1",
		"KAFKA_OFFSETS_TOPIC_NUM_PARTITIONS":     "1",
		"KAFKA_LOG_FLUSH_INTERVAL_MESSAGES":      "10000000",
		"KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS": "0",
		"KAFKA_AUTO_CREATE_TOPICS_ENABLE":        strconv.FormatBool(false),
	}
	os.Setenv("TC_HOST", "localhost")
	req := testcontainers.ContainerRequest{
		Image:        "confluentinc/cp-kafka:5.2.1",
		ExposedPorts: []string{brokerPort.Port(), kafkaPort.Port(), zookeeperPort},
		Cmd:          []string{"sleep", "infinity"},
		Env:          env,
	}

	container, err := testcontainers.GenericContainer(context.Background(), testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})

	err = startZookeeper(container)
	if err != nil {
		return nil, fmt.Errorf("failed to start zookeeper")
	}

	port, _ := container.MappedPort(context.Background(), kafkaPort)
	host, _ := container.Host(context.Background())
	bootstrapServer := fmt.Sprintf("PLAINTEXT://%s:%s", host, port.Port())

	err = startKafka(container, bootstrapServer)
	if err != nil {
		return nil, fmt.Errorf("failed to start kafka")
	}

	ac, err := confluent.NewAdminClient(&confluent.ConfigMap{"bootstrap.servers": bootstrapServer})
	if err != nil {
		return nil, fmt.Errorf("error creating admin client")
	}

	producer, err := confluent.NewProducer(&confluent.ConfigMap{"bootstrap.servers": bootstrapServer})
	if err != nil {
		return nil, fmt.Errorf("error creating producer")
	}

	return &Kafka{
		container:        container,
		host:             host,
		port:             port.Port(),
		bootstrapServers: bootstrapServer,
		adminClient:      ac,
		producer:         producer,
	}, nil
}

func startZookeeper(container testcontainers.Container) error {
	cmd := []string{"sh", "-c", "printf 'clientPort=2181\ndataDir=/var/lib/zookeeper/data\ndataLogDir=/var/lib/zookeeper/log'> /zookeeper.properties; zookeeper-server-start /zookeeper.properties >/dev/null 2>&1 &"}
	_, err := container.Exec(context.Background(), cmd)
	if err != nil {
		return err
	}
	return nil
}

func startKafka(container testcontainers.Container, bootstrapServers string) error {
	zookeeperConnect := fmt.Sprintf("localhost:%s", zookeeperPort)
	cmd := []string{"sh", "-c", fmt.Sprintf("export KAFKA_ZOOKEEPER_CONNECT=%s; export KAFKA_ADVERTISED_LISTENERS=%s,BROKER://:%s; /etc/confluent/docker/run >/tmp/kafka-start.log 2>/tmp/kafka-start &", zookeeperConnect, bootstrapServers, brokerPort.Port())}
	_, err := container.Exec(context.Background(), cmd)
	if err != nil {
		return err
	}
	return nil
}
