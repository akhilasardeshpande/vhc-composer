package wiremock

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

func NewStubFromJson(str string) (*Stub, error) {
	stub := Stub{}
	err := json.Unmarshal([]byte(str), &stub)
	if err != nil {
		return nil, err
	}
	return &stub, nil
}

func (w *WireMock) CreateStub(stub *Stub) error {
	url := fmt.Sprintf("%s/__admin/mappings", w.adminEndpoint)
	jsonStr, err := json.Marshal(stub)
	if err != nil {
		return err
	}
	res, err := http.Post(url, "application/json", strings.NewReader(string(jsonStr)))
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusCreated {
		return errors.New(fmt.Sprintf("Stub creation failed, status: %s", res.Status))
	}
	return nil
}

func (w *WireMock) ResetAllStubs() error {
	url := fmt.Sprintf("%s/__admin/mappings/reset", w.adminEndpoint)
	res, err := http.Post(url, "application/json", nil)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return errors.New(fmt.Sprintf("Stub reset failed, status: %s", res.Status))
	}
	return nil
}

func (w *WireMock) DeleteAllStubs() error {
	url := fmt.Sprintf("%s/__admin/mappings", w.adminEndpoint)
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return errors.New(fmt.Sprintf("Stub deletion failed, status: %s", res.Status))
	}
	return nil
}

func (w *WireMock) GetRequestCount(criteria *Request) (int, error) {
	url := fmt.Sprintf("%s/__admin/requests/count", w.adminEndpoint)
	jsonStr, err := json.Marshal(criteria)
	if err != nil {
		return 0, err
	}
	res, err := http.Post(url, "application/json", strings.NewReader(string(jsonStr)))
	if err != nil {
		return 0, err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return 0, errors.New(fmt.Sprintf("GetRequestCount falied, status: %s", res.Status))
	}
	var output struct {
		Count int `json:"count"`
	}
	err = json.NewDecoder(res.Body).Decode(&output)
	if err != nil {
		return 0, err
	}
	return output.Count, nil
}
