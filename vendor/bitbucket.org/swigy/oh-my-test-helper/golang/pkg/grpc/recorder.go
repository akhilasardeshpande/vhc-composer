package grpc

import (
	"context"
	"fmt"
	"github.com/mohae/deepcopy"
	"google.golang.org/grpc"
	"sync"
	"time"
)

type recorder struct {
	mu    sync.Mutex
	stubs map[string]*Stub
}

func NewGRPCRecorder() *recorder {
	return &recorder{
		mu:    sync.Mutex{},
		stubs: map[string]*Stub{},
	}
}

func (r *recorder) add(stub *Stub) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	if _, ok := r.stubs[stub.Request.String()]; ok {
		return fmt.Errorf("stub already exists")
	}
	stub.count = 0
	r.stubs[stub.Request.String()] = stub

	return nil
}

func (r *recorder) find(str string) (s *Stub, ok bool) {
	r.mu.Lock()
	defer r.mu.Unlock()

	stub, ok := r.stubs[str]
	return stub, ok
}

func (r *recorder) incStubCall(stub *Stub) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	stub, ok := r.stubs[stub.Request.String()]
	if !ok {
		return fmt.Errorf("stub not found")
	}
	stub.count++
	return nil
}

func (r *recorder) reset(request *Request) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	stub, ok := r.stubs[request.String()]
	if !ok {
		return fmt.Errorf("stub not found")
	}
	stub.count = 0
	return nil
}

func (r *recorder) resetAll() error {
	r.mu.Lock()
	defer r.mu.Unlock()

	for _, stub := range r.stubs {
		stub.count = 0
	}
	return nil
}

func (r *recorder) delete(request *Request) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	delete(r.stubs, request.String())
	return nil
}

func (r *recorder) deleteAll() error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.stubs = map[string]*Stub{}
	return nil
}

func (r *recorder) count(request *Request) (int, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	stub, ok := r.stubs[request.String()]
	if ok {
		return stub.count, nil
	}
	return 0, nil
}

func (r *recorder) interceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	stub, ok := r.find(info.FullMethod)
	if !ok {
		return handler(ctx, req)
	}
	time.Sleep(time.Millisecond * time.Duration(stub.Response.FixedDelayMilliseconds))
	r.incStubCall(stub)

	if stub.Response.TemplateFn != nil {
		response := deepcopy.Copy(stub.Response.Data)
		stub.Response.TemplateFn(req, response)
		return response, stub.Response.Err
	}

	return stub.Response.Data, stub.Response.Err
}
