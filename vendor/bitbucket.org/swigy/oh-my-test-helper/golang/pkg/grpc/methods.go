package grpc

// This File contains the public API for the container.

func (g *GRPC) CreateStub(stub *Stub) error {
	return g.recorder.add(stub)
}

func (g *GRPC) ResetAllStubs() error {
	return g.recorder.resetAll()
}

func (g *GRPC) DeleteAllStubs() error {
	return g.recorder.deleteAll()
}

func (g *GRPC) GetRequestCount(request *Request) (count int, err error) {
	return g.recorder.count(request)
}
