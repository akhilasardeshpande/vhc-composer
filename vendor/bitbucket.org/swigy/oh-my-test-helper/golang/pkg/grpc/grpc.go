package grpc

import (
	"google.golang.org/grpc"
	"net"
)

type GRPC struct {
	host     string
	port     string
	recorder *recorder
	server   *grpc.Server
}

func (g *GRPC) Host() string {
	return g.host
}

func (g *GRPC) Port() string {
	return g.port
}

func (g *GRPC) Stop() error {
	g.server.Stop()
	return nil
}

// This Starts a New GRPC Server in a goroutine with the given port.
//
// All Protobuf services needs to be registered on the server before stubbing.
// The Stubs won't work otherwise, UmImplemented Error Code would be each time.
//
// Example -
//
//	container ,err := NewContainer("<port>", func(server *grpc.Server) {
//		core.RegisterGeoEntityServiceAPIServer(server, &core.UnimplementedGeoEntityServiceAPIServer{})
//	})
//
func NewContainer(port string, registerFunc func(server *grpc.Server)) (*GRPC, error) {
	recorder := NewGRPCRecorder()
	server := grpc.NewServer(grpc.UnaryInterceptor(recorder.interceptor))
	registerFunc(server)

	listen, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return nil, err
	}

	go server.Serve(listen)

	return &GRPC{
		host:     "localhost",
		port:     port,
		recorder: recorder,
		server:   server,
	}, nil
}
