package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"os"
)

type Redis struct {
	container testcontainers.Container
	host      string
	port      string
	client    *redis.Client
}

func (r *Redis) Host() string {
	return r.host
}

func (r *Redis) Port() string {
	return r.port
}

func (r *Redis) Client() *redis.Client {
	return r.client
}

func (r *Redis) Stop() error {
	return r.container.Terminate(context.Background())
}

func NewContainer() (*Redis, error) {
	ctx := context.Background()
	os.Setenv("TC_HOST", "localhost")
	req := testcontainers.ContainerRequest{
		Image:        "redis",
		ExposedPorts: []string{"6379/tcp"},
		WaitingFor:   wait.ForListeningPort("6379"),
	}

	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})

	if err != nil {
		return nil, err
	}

	host, _ := container.Host(context.Background())
	port, _ := container.MappedPort(context.Background(), "6379")

	client := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", host, port.Port()),
	})

	return &Redis{
		container: container,
		host:      host,
		port:      port.Port(),
		client:    client,
	}, nil
}
