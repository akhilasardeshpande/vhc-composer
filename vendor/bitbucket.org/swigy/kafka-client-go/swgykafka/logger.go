package swgykafka

import (
	"log"
	"os"
)

var standardLogger = log.New(os.Stdout, "KafkaClientGo : ", log.LstdFlags|log.Llongfile)
var standardErrLogger = log.New(os.Stderr, "KafkaClientGo : ", log.LstdFlags|log.Llongfile)
