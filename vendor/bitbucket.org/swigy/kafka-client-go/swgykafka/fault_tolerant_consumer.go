package swgykafka

import (
	"context"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"math"
	"strconv"
	"strings"
	"sync"
	"time"
)

// Interface for consumer worker
type consumerWorker interface {
	start() error
	shutdown()
	failOver() bool
	failBack() bool
}

//region Fault Tolerant Consumer
type faultTolerantConsumer struct {
	name                              string
	config                            *ConsumerConfig
	handler                           ConsumerHandler
	retryProducer                     *Producer
	retryTopic                        string
	deadLetterTopic                   string
	faultDetector                     *faultDetector
	primaryConsumerThread             *ConsumerThread
	secondaryConsumerThread           *ConsumerThread
	markerPrimaryConsumerThread       *ConsumerThread
	markerSecondaryConsumerThread     *ConsumerThread
	primaryForceProcessingEventChan   chan forceProcessingEvent
	secondaryForceProcessingEventChan chan forceProcessingEvent
	shutdownChan                      chan interface{}
	metricChan                        chan *consumerMetric
	shutDownWaitGroup                 *sync.WaitGroup
	ctx                               context.Context
}

const (
	secondarySuffix  = "_secondary"
	markerSuffix     = "_marker"
	faultMarkerTopic = "internal_fault_marker"
	seekBufferTime   = 125000
)

func newFaultTolerantConsumerInstance(name string, config *ConsumerConfig, handler ConsumerHandler, retryProducer *Producer, retryTopic string, deadLetterTopic string, faultDetector *faultDetector, shutdownChan chan interface{}, shutDownWaitGroup *sync.WaitGroup, metricChan chan *consumerMetric) (*faultTolerantConsumer, error) {
	// initialize context
	ctx := NewContext(context.Background(),
		AutoField("fault-tolerant-consumer-name", name))

	return &faultTolerantConsumer{
		name:                              name,
		config:                            config,
		handler:                           handler,
		retryProducer:                     retryProducer,
		retryTopic:                        retryTopic,
		deadLetterTopic:                   deadLetterTopic,
		faultDetector:                     faultDetector,
		shutdownChan:                      shutdownChan,
		metricChan:                        metricChan,
		shutDownWaitGroup:                 shutDownWaitGroup,
		primaryForceProcessingEventChan:   make(chan forceProcessingEvent),
		secondaryForceProcessingEventChan: make(chan forceProcessingEvent),
		ctx:                               ctx,
	}, nil
}

func (ftc *faultTolerantConsumer) init() error {
	var err error
	ftc.primaryConsumerThread, err = ftc.createConsumerThread(true)
	if err != nil {
		return err
	}

	if ftc.config.IsDualRWEnabled(){
		ftc.secondaryConsumerThread, err = ftc.createConsumerThread(false)
		ftc.markerPrimaryConsumerThread, err = ftc.createMarkerConsumerThread(true)    // Uptime
		ftc.markerSecondaryConsumerThread, err = ftc.createMarkerConsumerThread(false) //Downtime marker
		ftc.primaryConsumerThread.registerRebalanceListener(ftc.markerPrimaryConsumerThread)
		ftc.secondaryConsumerThread.registerRebalanceListener(ftc.markerSecondaryConsumerThread)
	}

	return err
}

func (ftc *faultTolerantConsumer) createConsumerThread(primary bool) (*ConsumerThread, error) {
	config := *ftc.config
	fpec := ftc.primaryForceProcessingEventChan
	if !primary {
		config.clientID = config.clientID + secondarySuffix
		fpec = ftc.secondaryForceProcessingEventChan
	}
	return NewConsumerThread(ftc.name,
		config, primary,
		ftc.handler,
		ftc.retryProducer,
		ftc.retryTopic,
		ftc.deadLetterTopic,
		ftc.shutdownChan,
		ftc.shutDownWaitGroup,
		fpec,
		true,
		ftc.metricChan)
}

func (ftc *faultTolerantConsumer) createMarkerConsumerThread(primary bool) (*ConsumerThread, error) {
	fpec := ftc.primaryForceProcessingEventChan
	if !primary {
		fpec = ftc.secondaryForceProcessingEventChan
	}

	return NewConsumerThread(ftc.name,
		*createMarkerConfig(ftc.config, primary),
		primary,
		markerHandler{
			faultTolerantConsumer: ftc,
			primary:               primary,
			ctx:                   ftc.ctx,
		},
		nil,
		"",
		"",
		ftc.shutdownChan,
		ftc.shutDownWaitGroup,
		fpec,
		false, ftc.metricChan)
}

func (ftc *faultTolerantConsumer) start() error {
	err := ftc.primaryConsumerThread.run()

	if err != nil {
		return err
	}
	if ftc.secondaryConsumerThread != nil {
		err = ftc.secondaryConsumerThread.run()
		if err != nil {
			return err
		}

		err = ftc.markerPrimaryConsumerThread.run()
		if err != nil {
			return err
		}

		err = ftc.markerSecondaryConsumerThread.run()
		if err != nil {
			return err
		}
	}

	return err
}

func (ftc *faultTolerantConsumer) shutdown() {
	ftc.primaryConsumerThread.shutdown()
	if ftc.secondaryConsumerThread != nil {
		ftc.secondaryConsumerThread.shutdown()
		ftc.markerPrimaryConsumerThread.shutdown()
		ftc.markerSecondaryConsumerThread.shutdown()
	}
}

func (ftc *faultTolerantConsumer) failOver() bool {
	logger := WithContext(ftc.ctx)

	logger.Info("failing over to secondary")
	if ftc.secondaryConsumerThread == nil {
		return false
	}

	ftc.sendMarkerEvents(ftc.primaryConsumerThread.lastProcessedTimes, false)
	return true
}

func (ftc *faultTolerantConsumer) sendMarkerEvents(lastProcessedTimeStamp map[string]int64, primary bool) {
	logger := WithContext(ftc.ctx)

	defaultSeekTime := toMillisUTC(time.Now()) - int64(ftc.config.seekTimeForSecondary)
	minTimestamp := defaultSeekTime
	for k, v := range lastProcessedTimeStamp {
		seekTime := v
		if seekTime <= 0 {
			seekTime = minTimestamp
		}
		seekTime = seekTime - seekBufferTime
		tpcPartition := strings.Split(k, "-")
		topicName := tpcPartition[0]
		partition, _ := strconv.Atoi(tpcPartition[1])
		ptn := &kafka.TopicPartition{
			Topic:     &topicName,
			Partition: int32(partition),
		}

		failovrMrkr := NewFailoverMarkerBuilder().
			TopicPartition(ptn).
			LastProcessed(seekTime).
			ConsumerGroup(ftc.config.consumerGroupID).Build()
		var producer kafkaProducer
		if primary {
			producer = (ftc.retryProducer.multiCluster.(*multiClusterProducer)).primaryProducer
		} else {
			producer = (ftc.retryProducer.multiCluster.(*multiClusterProducer)).secondaryProducer
		}
		logger.Info("Sending the marker event", AutoField("failover-marker", failovrMrkr), AutoField(isPrimaryLogKey, primary))
		ftc.sendMarkerEvent(producer, failovrMrkr)
	}
}

func (ftc *faultTolerantConsumer) failBack() bool {
	logger := WithContext(ftc.ctx)
	logger.Info("failing back to primary")
	if ftc.secondaryConsumerThread == nil {
		return false
	}

	if ftc.secondaryConsumerThread.processAll {
		ftc.sendMarkerEvents(ftc.secondaryConsumerThread.lastProcessedTimes, true)
		// Stop processing all messages in secondary
		ftc.secondaryConsumerThread.forceProcessingEventChan <- forceProcessingEvent{
			processAll:     false,
			topicPartition: kafka.TopicPartition{},
			fromTimeStamp:  -1,
		}
	}
	return true
}

func (ftc *faultTolerantConsumer) sendMarkerEvent(producer kafkaProducer, marker failoverMarker) {
	logger := WithContext(ftc.ctx)

	rcd := marker.ToRecord()
	message := createMarkerKafkaMessage(rcd)
	d := make(chan kafka.Event, 1)
	err := producer.produce(message, d)
	if err != nil {
		logger.Error("unable to send marker event", AutoField("marker", marker), ErrorField(err))
	}
}

func createMarkerConfig(config *ConsumerConfig, primary bool) *ConsumerConfig {
	// Copy existing values
	markerConfig := *config
	markerConfig.CommonConfig = config.CommonConfig
	markerConfig.retryConfig = nil

	// update to new values
	if !primary {
		markerConfig.clientID = markerConfig.clientID + secondarySuffix
	}
	markerConfig.clientID = markerConfig.clientID + markerSuffix
	markerConfig.consumerGroupID = markerConfig.consumerGroupID + markerSuffix
	markerConfig.topic = &Topic{name: faultMarkerTopic}
	return &markerConfig
}

//endregion

//region Marker Handler
type markerHandler struct {
	faultTolerantConsumer *faultTolerantConsumer
	primary               bool
	ctx                   context.Context
}

func (m markerHandler) Handle(record *Record) (Status, error) {
	logger := WithContext(m.ctx)

	marker := record.ToFailoverMarker()
	fpe := forceProcessingEvent{
		processAll:     !m.primary,
		topicPartition: marker.getTopicPartition(),
		fromTimeStamp:  marker.getLastProcessedTime(),
	}

	if m.faultTolerantConsumer.config.consumerGroupID == marker.getConsumerGroup() &&
		m.faultTolerantConsumer.config.topic.name == *marker.getTopicPartition().Topic {
		if m.primary {
			logger.Info("Received failback marker event.", AutoField("event", marker.toString()))
			m.faultTolerantConsumer.primaryForceProcessingEventChan <- fpe
		} else {
			logger.Info("Received failover marker event.", AutoField("event", marker.toString()))
			// We are waiting before the check to ensure that the secondary consumer had enough time to
			// detect failure of the primary cluster.
			waitBeforeProcessingMarker(m.faultTolerantConsumer.config.markerConsumerSleepMs)
			if !m.faultTolerantConsumer.faultDetector.isHealthy(true) {
				// Handle the marker event in the secondary consumer only when it also perceives that the
				// primary cluster is unhealthy.
				logger.Info("Processing failover marker event: {} ", AutoField("event", marker.toString()))
				m.faultTolerantConsumer.secondaryForceProcessingEventChan <- fpe
			}
		}
	}

	logger.Info("marker event handled", AutoField("marker", record), AutoField("topic", *marker.getTopicPartition().Topic),
		AutoField("partition", marker.getTopicPartition().Partition), AutoField("from-timestamp", marker.getLastProcessedTime()))
	return Success, nil
}

func waitBeforeProcessingMarker(sleepTime int) {
	for ; sleepTime > 0; sleepTime -= 1000 {
		currSleepTime := math.Min(float64(sleepTime), 1000)
		time.Sleep(time.Duration(int64(currSleepTime)) * time.Millisecond)
	}
}

//endregion

//region Force Processing Event
type forceProcessingEvent struct {
	processAll     bool
	topicPartition kafka.TopicPartition
	fromTimeStamp  int64
}

//endregion
