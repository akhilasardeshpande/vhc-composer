package swgykafka

type Status int

// Enum values for Status
const (
	Success Status = iota
	SoftFailure
	HardFailure
)
