package swgykafka

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
)

const threadGroupFormat = "kafka-cg-%s-thread-%s"
const threadClientIdFormat = "%s_%s"

type IConsumer interface {
	Start() error
	Shutdown()
	IsActive() bool
}

type Consumer struct {
	config                   ConsumerConfig
	consumerWorkers          []consumerWorker
	handler                  ConsumerHandler
	shutDownWaitGroup        *sync.WaitGroup
	metricChan               chan *consumerMetric
	active                   bool
	initialized              bool
	retryProducer            *Producer
	shutdownChan             chan interface{}
	shutdownOnce             sync.Once
	stopMetricsCapturingOnce sync.Once
	faultDetector            *faultDetector
	ctx                      context.Context
}

func NewConsumer(config ConsumerConfig, handler ConsumerHandler, retryProducer *Producer) (*Consumer, error) {
	// initialize context
	ctx := NewContext(context.Background(), AutoField("topic", config.topic.name))

	if handler == nil {
		return nil, errors.New("handler is required")
	}

	con := &Consumer{config: config, handler: handler, ctx: ctx}
	if retryProducer != nil {
		con.retryProducer = retryProducer
	}

	if config.IsDualRWEnabled(){
		con.faultDetector = newFaultDetector(config.primary, config.secondary, con, config.getHealthCheckIntervalMs(), config.getHealthDegradationThreshold())
	}

	return con, nil
}

func (con *Consumer) init() error {
	logger := WithContext(con.ctx)
	if con.initialized {
		return nil
	}

	// Initialize Health Checker if a fault detector is created
	if con.faultDetector != nil {
		initHealthCheckerInstance()
	}

	con.consumerWorkers = make([]consumerWorker, 0)
	con.shutDownWaitGroup = &sync.WaitGroup{}
	con.shutdownChan = make(chan interface{})
	con.metricChan= make(chan *consumerMetric)

	err := con.validateRetryProducer()
	if err != nil {
		logger.Error("error occurred in consumer validateRetryProducer()", ErrorField(err))
		return err
	}
	for i := 0; i < con.config.concurrency; i++ {
		cw, err := con.createConsumerWorker(con.config, con.handler, i)
		if err != nil {
			logger.Error("error occurred in consumer createConsumerWorker()", ErrorField(err))
			return err
		}
		con.consumerWorkers = append(con.consumerWorkers, cw)
	}

	for i := 0; i < con.config.retryConfig.retryConcurrency; i++ {
		cw, err := con.createConsumerWorker(*createRetryConfig(&con.config), newRetryHandler(con.handler, con.config.consumerGroupID), i)
		if err != nil {
			logger.Error("error occurred in consumer createConsumerWorker()", ErrorField(err))
			return err
		}
		con.consumerWorkers = append(con.consumerWorkers, cw)
	}

	if err := con.initMetrics(); err != nil {
		logger.Error("error occurred in consumer initMetrics()", ErrorField(err))
		return err
	}

	con.initialized = true
	return nil
}

func (con *Consumer) createConsumerWorker(config ConsumerConfig, handler ConsumerHandler, i int) (*faultTolerantConsumer, error) {
	// Set Client ID
	config.clientID = fmt.Sprintf(threadClientIdFormat, config.clientID, strconv.Itoa(i))
	name := fmt.Sprintf(threadGroupFormat, config.consumerGroupID, strconv.Itoa(i))

	// Create fault tolerant consumer
	cw, err := newFaultTolerantConsumerInstance(name,
		&config,
		handler,
		con.retryProducer,
		getRetryTopic(con.config.topic.name),
		getDeadLetterTopic(con.config.topic.name),
		con.faultDetector,
		con.shutdownChan,
		con.shutDownWaitGroup,con.metricChan)
	if err != nil {
		return nil, err
	}

	err = cw.init()
	if err != nil {
		return nil, err
	}
	return cw, nil
}

func (con *Consumer) IsActive() bool {
	return con.active
}

func (con *Consumer) validateRetryProducer() error {
	if con.isRetryConfigured() &&
		(con.retryProducer == nil || !con.isValidProducerConfig(con.retryProducer.config)) {
		return errors.New("retry producer is not configured correctly")
	}
	return nil
}

func (con *Consumer) isValidProducerConfig(producer ProducerConfig) bool {
	return con.config.primary.equals(producer.primary) && con.config.secondary.equals(producer.secondary)
}

func (con *Consumer) isRetryConfigured() bool {
	return con.config.retryConfig != nil && (con.config.retryConfig.retries > 0 || con.config.retryConfig.deadLettering)
}

func (con *Consumer) Start() error {
	logger := WithContext(con.ctx)

	if !con.initialized {
		err := con.init()
		if err != nil {
			logger.Error("error occurred in consumer Start()", ErrorField(err))
			return err
		}
	}

	logger.Info("Starting consumer")
	for i, th := range con.consumerWorkers {
		err := th.start()
		if err != nil {
			logger.Error("error occurred in consumer Start()", ErrorField(err))
			con.shutdownInitializedThreads(i)
			return err
		}
	}
	logger.Info("Consumer is started.")

	if con.faultDetector != nil {
		con.faultDetector.start()
	}
	con.active = true
	return nil
}

func (con *Consumer) shutdownInitializedThreads(n int) {
	con.closeShutdownChan()
	con.shutDownWaitGroup.Wait()

	for i := 0; i < n; i++ {
		con.consumerWorkers[i].shutdown()
	}

	if con.faultDetector != nil {
		con.faultDetector.stop()
	}
	con.active = false
}

func (con *Consumer) Shutdown() {
	if !con.active {
		return
	}
	con.closeShutdownChan()
	con.shutDownWaitGroup.Wait()
	for _, th := range con.consumerWorkers {
		th.shutdown()
	}

	if con.faultDetector != nil {
		con.faultDetector.stop()
		healthCheckerInstance.Shutdown()
	}
	con.closeMetricsChan()
	con.active = false
}

func (con *Consumer) initMetrics() error {
	return defaultMetricsRegistry.registerConsumerMetrics(con)
}

func (con *Consumer) closeMetricsChan() {
	con.stopMetricsCapturingOnce.Do(func() {
		close(con.metricChan)
	})
}

func (con *Consumer) closeShutdownChan() {
	con.shutdownOnce.Do(func() {
		close(con.shutdownChan)
	})
}

func (con *Consumer) failover() {
	for _, cw := range con.consumerWorkers {
		cw.failOver()
	}
}

func (con *Consumer) failback() {
	for _, cw := range con.consumerWorkers {
		cw.failBack()
	}
}

func createRetryConfig(config *ConsumerConfig) *ConsumerConfig {
	// Copy existing values
	retryConfig := *config
	retryConfig.CommonConfig = config.CommonConfig
	newRetryConfig := *config.retryConfig
	retryConfig.retryConfig = &newRetryConfig
	topic := config.topic.clone()
	retryConfig.topic = &topic

	// Set new values
	retryConfig.clientID = config.clientID + retrySuffix
	retryConfig.topic.name = getRetryTopic(config.topic.name)
	retryConfig.delayInMs = config.retryConfig.retryInterval
	return &retryConfig
}

func getRetryTopic(topic string) string {
	return topic + retrySuffix
}

func getDeadLetterTopic(topic string) string {
	return topic + deadLetterSuffix
}

type ConsumerHandler interface {
	Handle(record *Record) (Status, error)
}
