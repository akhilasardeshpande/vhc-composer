package swgykafka

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"strconv"
	"strings"
)

const (
	keyTopic         = "topic"
	keyPartition     = "partition"
	keyLastProcessed = "last_processed"
	keyConsumerGroup = "consumer_group"
)

type failoverMarker struct {
	Attributes map[string]string `json:"attributes"`
}

func (marker failoverMarker) ToRecord() *Record {
	partition, _ := strconv.Atoi(marker.Attributes[keyPartition])
	val := []byte(marker.toString())
	return &Record{
		Topic:     faultMarkerTopic,
		Partition: partition,
		Value:     val,
	}
}

func (marker failoverMarker) toString() string {
	var entries []string
	for k, v := range marker.Attributes {
		entries = append(entries, fmt.Sprintf("%s:%s", k, v))
	}
	return strings.Join(entries, ",")
}

func NewFailoverMarker(markerStr string) failoverMarker {
	marker := make(map[string]string)
	entries := strings.Split(markerStr, ",")
	for _, entry := range entries {
		kv := strings.Split(entry, ":")
		if len(kv) > 1 {
			marker[kv[0]] = kv[1]
		}
	}
	return failoverMarker{Attributes: marker}
}

func (marker failoverMarker) getTopicPartition() kafka.TopicPartition {
	topic := marker.Attributes[keyTopic]

	p, err := strconv.ParseInt(marker.Attributes[keyPartition], 10, 32)
	if err != nil {
		logger.Error("Invalid partition number found", AutoField(partitionLogKey, marker.Attributes[keyPartition]), ErrorField(err))
	}

	partition := int32(p)
	return kafka.TopicPartition{
		Topic:     &topic,
		Partition: partition,
	}
}

func (marker failoverMarker) getLastProcessedTime() int64 {
	lastProcessed, ok := marker.Attributes[keyLastProcessed]
	if !ok {
		return 0
	}

	lp, err := strconv.ParseInt(lastProcessed, 10, 64)

	if err != nil {
		logger.Error("invalid last processed time found", AutoField("last-processed", marker.Attributes[keyLastProcessed]), ErrorField(err))
	}
	return lp
}

func (marker failoverMarker) getConsumerGroup() string {
	consumerGroup, ok := marker.Attributes[keyConsumerGroup]
	if !ok {
		return ""
	}
	return consumerGroup
}

type failoverMarkerBuilder struct {
	failoverMarker *failoverMarker
}

func NewFailoverMarkerBuilder() *failoverMarkerBuilder {
	return &failoverMarkerBuilder{failoverMarker: &failoverMarker{
		Attributes: make(map[string]string),
	}}
}

func (builder *failoverMarkerBuilder) TopicPartition(topicPartition *kafka.TopicPartition) *failoverMarkerBuilder {
	builder.failoverMarker.Attributes[keyTopic] = *topicPartition.Topic
	builder.failoverMarker.Attributes[keyPartition] = strconv.Itoa(int(topicPartition.Partition))
	return builder
}

func (builder *failoverMarkerBuilder) LastProcessed(lastPrcssd int64) *failoverMarkerBuilder {
	builder.failoverMarker.Attributes[keyLastProcessed] = strconv.FormatInt(lastPrcssd, 10)
	return builder
}

func (builder *failoverMarkerBuilder) ConsumerGroup(consumerGroup string) *failoverMarkerBuilder {
	builder.failoverMarker.Attributes[keyConsumerGroup] = consumerGroup
	return builder
}

func (builder *failoverMarkerBuilder) Build() failoverMarker {
	return *builder.failoverMarker
}
