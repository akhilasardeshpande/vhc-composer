package swgykafka

import (
	"context"
	"errors"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"go.uber.org/zap"
	"sync"
	"time"
)

type IProducer interface {
	Send(topicName string, key string, value string, headers map[string]string) (chan *ProducerRecord, error)
	SendBytes(topicName string, key []byte, value []byte, headers map[string]string) (chan *ProducerRecord, error)
	Close()
}

type Producer struct {
	dispatcher    *dispatcher // Nil is a valid value, meaning each function will be processed in a separate goroutine
	config        ProducerConfig
	singleCluster dataProducer
	multiCluster  dataProducer
	closeOnce     sync.Once
}

func NewProducer(config ProducerConfig) (*Producer, error) {
	dispatcher := newDispatcher(config.maxWorkers, config.maxQueueLen)
	if dispatcher != nil {
		dispatcher.run()
	}

	primaryProducer, err := createProducer(config, true)
	if err != nil {
		return nil, err
	}

	flushTimeout := config.flushTimeout

	producer := &Producer{
		dispatcher: dispatcher,
		config:     config,
		closeOnce:  sync.Once{},
		singleCluster: &singleClusterProducer{
			dispatcher: dispatcher,
			producer: &concreteKafkaProducer{
				Producer:     primaryProducer,
				cluster:      config.primary,
				flushTimeout: flushTimeout,
			},
			flushTimeout: config.flushTimeout,
			ctx:          NewContext(context.Background(), AutoField(bootstrapServersLogKey, config.primary.bootstrapServers), AutoField("producer-type", "single-cluster")),
		},
	}

	var mcp *multiClusterProducer
	if config.IsDualRWEnabled() && config.secondary != nil {
		secondaryProducer, err := createProducer(config, false)
		if err != nil {
			return nil, err
		}

		mcp = &multiClusterProducer{
			dispatcher: dispatcher,
			primaryProducer: &concreteKafkaProducer{
				Producer:     primaryProducer,
				cluster:      config.primary,
				flushTimeout: flushTimeout,
			},
			secondaryProducer: &concreteKafkaProducer{
				Producer:     secondaryProducer,
				cluster:      config.secondary,
				flushTimeout: flushTimeout,
			},
			flushTimeout: config.flushTimeout,
			ctx:          NewContext(context.Background(), AutoField("producer-type", "multi-cluster")),
		}
		producer.multiCluster = mcp
	}

	if config.IsDualRWEnabled() && producer.multiCluster == nil {
		return nil, fmt.Errorf("topics with dual write exists. provide secondary cluster settings")
	}

	if config.IsDualRWEnabled() && mcp != nil {
		initHealthCheckerInstance()
		mcp.registerHealthChecker(config.getHealthCheckIntervalMs())
	}

	if err := producer.initMetrics(); err != nil {
		return nil, err
	}

	return producer, nil
}

type ProducerRecord struct {
	Message           *kafka.Message
	priRecordMetadata *RecordMetadata
	secRecordMetadata *RecordMetadata
	Done              bool
	Err               error
}

func (p ProducerRecord) String() string {

	return fmt.Sprintf("Message: %v, PriRecordMetaData: %p, SecRecordMetadata: %p, Done: %v, Error, %v",
		*(p.Message),
		p.priRecordMetadata,
		p.secRecordMetadata,
		p.Done,
		p.Err)
}

// Added an interface for Kafka Producer to allow mocking of the producer for unit test cases
type dataProducer interface {
	send(message *kafka.Message, recordChan chan *ProducerRecord) error
	close()
}

type multiClusterProducer struct {
	primaryProducer   kafkaProducer
	secondaryProducer kafkaProducer
	flushTimeout      int
	ctx               context.Context
	dispatcher        *dispatcher
	//produceMech       MultiClusterProduceMech
}

type singleClusterProducer struct {
	producer     kafkaProducer
	flushTimeout int
	ctx          context.Context
	dispatcher   *dispatcher
}

type kafkaProducer interface {
	produce(message *kafka.Message, deliveryChan chan kafka.Event) error
	flush(flushTimeout int) int
	isHealthy() bool
	updateHealth(cluster *Cluster, health *clusterHealth)
	registerHealthChecker(listener healthListener, healthCheckIntervalInMs int)
	deregisterHealthChecker(listener healthListener)
	close()
	events() chan kafka.Event
}

type concreteKafkaProducer struct {
	*kafka.Producer
	cluster *Cluster
	clusterHealth *clusterHealth
	flushTimeout int
}

func (producer *concreteKafkaProducer) produce(message *kafka.Message, deliveryChan chan kafka.Event) error {
	return producer.Produce(message, deliveryChan)
}

func (producer *concreteKafkaProducer) flush(flushTimeout int) int {
	return producer.Flush(flushTimeout)
}

func (producer *concreteKafkaProducer) updateHealth(cluster *Cluster, health *clusterHealth) {
	if producer.cluster.equals(cluster) {
		producer.clusterHealth = health
	}
}

func (producer *concreteKafkaProducer) registerHealthChecker(listener healthListener, healthCheckIntervalInMs int) {
	// register cluster for health check
	healthCheckUpdatePrimary := healthCheckerUpdate{
		cluster:                 producer.cluster,
		listeners:               map[healthListener]struct{}{listener: {}},
		healthCheckIntervalInMs: healthCheckIntervalInMs,
		register:                true,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdatePrimary
}

func (producer *concreteKafkaProducer) deregisterHealthChecker(listener healthListener) {
	// de-register cluster for health check
	healthCheckUpdatePrimary := healthCheckerUpdate{
		cluster:  producer.cluster,
		register: false,
	}
	healthCheckerInstance.healthCheckerUpdateChan <- &healthCheckUpdatePrimary
}

func (producer *concreteKafkaProducer) isHealthy() bool {
	if producer.clusterHealth != nil && !producer.clusterHealth.isHealthy{
		return false
	}

	return true
}

func (producer *concreteKafkaProducer) close() {
	producer.Close()
}

func (producer *concreteKafkaProducer) events() chan kafka.Event {
	return producer.Events()
}

func (scp *singleClusterProducer) send(message *kafka.Message, recordChan chan *ProducerRecord) error {
	logger := WithContext(scp.ctx)

	deliveryChan := make(chan kafka.Event, 1)
	err := scp.producer.produce(message, deliveryChan)
	if err != nil {
		close(deliveryChan)
		logger.Error("failed while sending the message", AutoField(messageKeyLogKey, message.Key), ErrorField(err))
		return err
	}

	process := func() {
		defer close(deliveryChan)
		receiveRecordInChan(deliveryChan, recordChan, logger)
	}

	if scp.dispatcher == nil {
		go process()
	} else {
		job := Job{
			process: process,
		}
		scp.dispatcher.jobQueue <- job
	}

	return nil
}

func (mcp *multiClusterProducer) send(message *kafka.Message, recordChan chan *ProducerRecord) error {
	return mcp.sendInSync(message, recordChan)
}

func (mcp *multiClusterProducer) onUpdate(cluster *Cluster, health *clusterHealth) {
	mcp.primaryProducer.updateHealth(cluster, health)
	mcp.secondaryProducer.updateHealth(cluster, health)
}

func (mcp *multiClusterProducer) registerHealthChecker(healthCheckIntervalInMs int) {
	mcp.primaryProducer.registerHealthChecker(mcp, healthCheckIntervalInMs)
	mcp.secondaryProducer.registerHealthChecker(mcp, healthCheckIntervalInMs)
}

func (mcp *multiClusterProducer) deregisterHealthChecker() {
	mcp.primaryProducer.deregisterHealthChecker(mcp)
	mcp.secondaryProducer.deregisterHealthChecker(mcp)
}

func receiveRecordInChan(deliveryChan chan kafka.Event, recordChan chan *ProducerRecord, logger *zap.Logger) {
	e := <-deliveryChan
	logger.Info("event received in delivery chan", AutoField("event", e.String()))
	m := e.(*kafka.Message)
	if m.TopicPartition.Error != nil {
		logger.Error("Delivery failed", ErrorField(m.TopicPartition.Error))
	}

	isDone := true
	if m.TopicPartition.Error != nil{
		isDone = false
	}

	recordChan <- &ProducerRecord{
		Message:           m,
		priRecordMetadata: toRecordMetadata(m),
		Err:               m.TopicPartition.Error,
		Done: isDone,
	}
}

func updateResult(result *ProducerRecord, deliveryChan chan kafka.Event, primaryProducer bool, shouldUpdateMessage bool, logger* zap.Logger) {
	e := <-deliveryChan
	logger.Info("event received", AutoField("event", e.String()), AutoField("isPrimaryProducer", primaryProducer))
	if e == nil {
		return
	}
	var m *kafka.Message
	var er *kafka.Error
	switch e.(type) {
	case *kafka.Message:
		m = e.(*kafka.Message)
	case kafka.Error:
		errrr := e.(kafka.Error)
		er = &errrr
		result.Err = errrr
	default:
		logger.Info("unknown event occurred.")
	}
	if result.Message == nil || shouldUpdateMessage {
		result.Message = m
	}

	if er == nil && m.TopicPartition.Error == nil {
		result.Done = true
		if primaryProducer {
			result.priRecordMetadata = toRecordMetadata(m)
		} else {
			result.secRecordMetadata = toRecordMetadata(m)
		}
	} else {
		if er == nil {
			logger.Error("Delivery failed", ErrorField(m.TopicPartition.Error))
			result.Err = m.TopicPartition.Error
		} else {
			logger.Error("Delivery failed", ErrorField(*er))
			result.Err = er
		}
		if primaryProducer {
			result.priRecordMetadata = nil
		} else {
			result.secRecordMetadata = nil
		}
	}
}

func toRecordMetadata(message *kafka.Message) *RecordMetadata {
	return &RecordMetadata{
		Topic:     *message.TopicPartition.Topic,
		Partition: int(message.TopicPartition.Partition),
		ValueSize: len(message.Value),
		KeySize:   len(message.Key),
		Timestamp: message.Timestamp.UTC(),
		Offset:    int64(message.TopicPartition.Offset),
		Headers:   headersToMap(message.Headers),
	}
}

func (scp *singleClusterProducer) close() {
	scp.producer.close()
}

func (mcp *multiClusterProducer) close() {
	logger := WithContext(mcp.ctx)

	pMsgCount := mcp.primaryProducer.flush(mcp.flushTimeout)
	logger.Info("messages were dropped due to primary cluster being closed",AutoField(isPrimaryLogKey, true) ,AutoField("message-count", pMsgCount))

	sMsgCount := mcp.secondaryProducer.flush(mcp.flushTimeout)
	logger.Info("messages were dropped due to secondary cluster being closed",AutoField(isPrimaryLogKey, false) ,AutoField("message-count", sMsgCount))

	mcp.deregisterHealthChecker()
}

/**
recordChan will return when both are finished processing.
*/
func (mcp *multiClusterProducer) sendInSync(message *kafka.Message, recordChan chan *ProducerRecord) error {
	logger := WithContext(mcp.ctx)

	pDeliveryChan := make(chan kafka.Event, 1)
	primaryMessage := toPrimaryMessage(message)
	result := &ProducerRecord{}

	var pErr error
	if mcp.primaryProducer.isHealthy() {
		pErr = mcp.primaryProducer.produce(primaryMessage, pDeliveryChan)
	} else {
		kErr := kafka.NewError(kafka.ErrMsgTimedOut, "Primary is Unhealthy", false)
		pErr = kErr
		pDeliveryChan <- kErr
	}

	if pErr != nil && !isTimeoutError(pErr) {
		defer func() { close(pDeliveryChan) }()
		logMessageSendFailure(message, "primary", pErr, logger)
		return pErr
	}

	process := func() {
		defer func() { close(pDeliveryChan) }()
		//if timeout occurred send message with primary path to secondary cluster
		if pErr != nil {
			mcp.retrySecondary(primaryMessage, result, recordChan)
			return
		}

		//check and update result from primaryDelivery channel
		updateResult(result, pDeliveryChan, true, true, logger)

		pM := result.Message
		//if there was some problem in delivering the msg to primary cluster send message with primary path to secondary cluster
		if pM != nil && pM.TopicPartition.Error != nil {
			if isTimeoutError(pM.TopicPartition.Error) {
				mcp.retrySecondary(primaryMessage, result, recordChan)
				return
			}

		}

		recordChan <- result

		//primary is processed successfully. send message without primary path to secondary cluster
		mcp.sendToSecondary(message, result)
	}

	if mcp.dispatcher == nil {
		go process()
	} else {
		job := Job{
			process: process,
		}
		mcp.dispatcher.jobQueue <- job
	}

	return nil
}

func (mcp *multiClusterProducer) sendToSecondary(message *kafka.Message, result *ProducerRecord) {
	logger := WithContext(mcp.ctx)

	sDeliveryChan := make(chan kafka.Event, 1)
	defer close(sDeliveryChan)

	var sErr error
	if mcp.secondaryProducer.isHealthy() {
		logger.Debug("sending the message to secondary producer", AutoField(messageKeyLogKey, string(message.Key)))
		sErr = mcp.secondaryProducer.produce(toSecondaryMessage(message, result), sDeliveryChan)
	} else {
		kErr := kafka.NewError(kafka.ErrMsgTimedOut, "Secondary is Unhealthy", false)
		sErr = kErr
		sDeliveryChan <- kErr
	}

	if sErr != nil {
		logMessageSendFailure(message, "secondary", sErr, logger)
		result.Err = sErr
	} else {
		updateResult(result, sDeliveryChan, false, false, logger)
	}
}

func (mcp *multiClusterProducer) retrySecondary(primaryMessage *kafka.Message, result *ProducerRecord, recordChan chan *ProducerRecord) {
	logger := WithContext(mcp.ctx)

	sDeliveryChan := make(chan kafka.Event, 1)
	defer func() { close(sDeliveryChan) }()

	var sErr error
	if mcp.secondaryProducer.isHealthy() {
		logger.Error("Sending the primary message to secondary producer", AutoField(messageKeyLogKey, string(primaryMessage.Key)))
		sErr = mcp.secondaryProducer.produce(primaryMessage, sDeliveryChan)
	} else {
		kErr := kafka.NewError(kafka.ErrMsgTimedOut, "Secondary is Unhealthy", false)
		sErr = kErr
		sDeliveryChan <- kErr
	}

	if sErr != nil {
		logMessageSendFailure(primaryMessage, "secondary", sErr, logger)
		result.Message = primaryMessage
		result.Err = sErr
		recordChan <- result
		return
	}
	updateResult(result, sDeliveryChan, false, true, logger)

	recordChan <- result
	return
}

func toPrimaryMessage(message *kafka.Message) *kafka.Message {
	hdrs := message.Headers
	hdrs = append(hdrs, kafka.Header{
		Key:   HeaderRecordPath,
		Value: []byte(RecordPathPrimary),
	})
	return &kafka.Message{
		TopicPartition: message.TopicPartition,
		Value:          message.Value,
		Key:            message.Key,
		Timestamp:      message.Timestamp,
		TimestampType:  message.TimestampType,
		Opaque:         message.Opaque,
		Headers:        hdrs,
	}
}

func toSecondaryMessage(message *kafka.Message, result *ProducerRecord) *kafka.Message {
	hdrs := message.Headers
	hdrs = append(hdrs, kafka.Header{
		Key:   HeaderRecordPath,
		Value: []byte(RecordPathSecondary),
	})
	partition := message.TopicPartition
	ts := message.Timestamp
	tsType := message.TimestampType
	if result != nil {
		partition = result.Message.TopicPartition
		ts = result.Message.Timestamp
		tsType = result.Message.TimestampType
	}
	return &kafka.Message{
		TopicPartition: partition,
		Value:          message.Value,
		Key:            message.Key,
		Timestamp:      ts,
		TimestampType:  tsType,
		Opaque:         message.Opaque,
		Headers:        hdrs,
	}
}

func createProducer(config ProducerConfig, primary bool) (*kafka.Producer, error) {
	configMap, err := config.toKafkaConfig(primary)
	if err != nil {
		return nil, err
	}
	return kafka.NewProducer(configMap)
}

/*
In case of a multi cluster approach following are followed
1. If primary cluster fails, sync call is made to secondary
2. If primary cluster succeeds msg is returned to channel without waiting for secondary
3. If function call fails in case of primary, sync call is made to secondary
*/
func (producer *Producer) Send(topicName string, key string, value string, headers map[string]string) (chan *ProducerRecord, error) {
	if topicName == "" {
		return nil, fmt.Errorf("topic cannot be empty")
	}
	if value == "" {
		return nil, errors.New("message value cannot be empty")
	}
	valueBytes := []byte(value)
	var keyBytes []byte
	if key != "" {
		keyBytes = []byte(key)
	}
	return producer.SendBytes(topicName, keyBytes, valueBytes, headers)

}

func (producer *Producer) SendBytes(topicName string, key []byte, value []byte, headers map[string]string) (chan *ProducerRecord, error) {
	if topicName == "" {
		return nil, fmt.Errorf("topic cannot be empty")
	}
	topic, found := producer.config.topics[topicName]
	if !found {
		topic, _ = NewTopicBuilder(topicName).Build()
	}

	message := createKafkaMessage(topic, key, value, headers)

	return producer.send(topic, &message)

}

func (producer *Producer) sendWithCustomTime(topicName string, key []byte, value []byte, headers map[string]string, msgTime time.Time) (chan *ProducerRecord, error) {
	if topicName == "" {
		return nil, fmt.Errorf("topic cannot be empty")
	}
	topic, found := producer.config.topics[topicName]
	if !found {
		topic, _ = NewTopicBuilder(topicName).Build()
	}

	message := createKafkaMessage(topic, key, value, headers)
	message.Timestamp = msgTime

	return producer.send(topic, &message)
}

func (producer *Producer) send(topic *Topic, message *kafka.Message) (chan *ProducerRecord, error) {
	recordChan := make(chan *ProducerRecord, 1)
	if producer.shouldSendToSecondary(topic) {
		err := producer.multiCluster.send(message, recordChan)
		return recordChan, err
	}

	err := producer.singleCluster.send(message, recordChan)
	return recordChan, err
}

func (producer *Producer) initMetrics() error {
	return defaultMetricsRegistry.registerProducerMetrics(producer)
}

func createKafkaMessage(topic *Topic, key []byte, value []byte, headers map[string]string) kafka.Message {
	var headersList []kafka.Header
	for k, v := range headers {
		headersList = append(headersList, kafka.Header{k, []byte(v)})
	}
	message := kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic: &topic.name,
			Partition: kafka.PartitionAny,
		},
		Value:         value,
		Key:           key,
		TimestampType: 0,
		Headers:       headersList,
	}
	return message
}

func createMarkerKafkaMessage(record *Record) *kafka.Message {
	message := kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     &record.Topic,
			Partition: int32(record.Partition),
		},
		Value: record.Value,
	}
	return &message
}

func (producer *Producer) shouldRetrySecondary(topic *Topic, err error) bool {
	return producer.shouldSendToSecondary(topic)
}

func (producer *Producer) shouldSendToSecondary(topic *Topic) bool {
	return topic.IsDualRWEnabled(producer.multiCluster != nil)
}

func (producer *Producer) Close() {
	producer.closeOnce.Do(producer.close)
}

func (producer *Producer) close() {
	if producer.dispatcher != nil {
		close(producer.dispatcher.jobQueue)
		//Wait till job queue is empty
		for ; len(producer.dispatcher.jobQueue) > 0; {
			time.Sleep(time.Duration(producer.config.jobQueueFlushTimeStepDuration) * time.Millisecond)
		}
		defer producer.dispatcher.shutdown()
	}

	if producer.multiCluster != nil {
		producer.multiCluster.close()
	} else {
		if producer.singleCluster != nil {
			producer.singleCluster.close()
		}
	}
}

func isTimeoutError(err error) bool {
	switch err.(type) {
	case kafka.Error:
		kfkErr := err.(kafka.Error)
		if kfkErr.Code() == kafka.ErrMsgTimedOut ||
			kfkErr.Code() == kafka.ErrTimedOut ||
			kfkErr.Code() == kafka.ErrTimedOutQueue ||
			kfkErr.Code() == kafka.ErrRequestTimedOut ||
			kfkErr.Code() == kafka.ErrInvalidSessionTimeout ||
			kfkErr.Code() == kafka.ErrInvalidTransactionTimeout {
			return true
		}
	}
	return false
}

func logMessageSendFailure(m *kafka.Message, cluster string, err error, logger *zap.Logger) {
	logger.Error("failed while sending message", AutoField(messageKeyLogKey, m.Key), AutoField("topic", *m.TopicPartition.Topic), ErrorField(err))
}
