package swgykafka

import (
	"encoding/json"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_model/go"
)

type registryState struct {
	producerRegistered bool
	consumerRegistered bool
}

var defaultRegistryState = &registryState{false, false}

type metric struct {
	name      string
	value     prometheus.Collector
	mType     io_prometheus_client.MetricType
	dimension []string
	converter metricConverter
}

type metrics []*metric

type metricInstance struct {
	*metric
	staticDim []string
}

type metricInstances []metricInstance

var (
	clientIDDim         = []string{"client_id", "cluster_id"}
	clientIDTopicDim    = []string{"client_id", "cluster_id", "topic"}

	consumerClientIDDim         = []string{"client_id", "cluster_id", "consumer_topic"}
	consumerClientIDTopicDim    = []string{"client_id", "cluster_id", "consumer_topic", "topic"}

	clientIDTopicLibDim = []string{"client_id", "cluster_id", "consumer_topic", "lib_version", "confluent_kafka_lib_version"}
	clientIDKafkaVerDim = []string{"client_id", "cluster_id", "consumer_topic", "kafka_version"}
)

func (metricInstances metricInstances) observe(stats stats) {
	for _, metricInstance := range metricInstances {
		metricInstance.observe(stats)
	}
}

func (metricInstances metricInstances) observeWithDim(stats stats, dims []string) {
	for _, metricInstance := range metricInstances {
		metricInstance.converter.observeWithDims(&metricInstance, stats, dims)
	}
}

func (metricInstance *metricInstance) observe(stats stats) {
	metricInstance.converter.observe(metricInstance, stats)
}

func observeValue(mType io_prometheus_client.MetricType, metric prometheus.Collector, dimensions []string, value interface{}) {
	switch mType {
	case io_prometheus_client.MetricType_SUMMARY:
		sum := metric.(*prometheus.SummaryVec)
		sum.WithLabelValues(dimensions...).Observe(value.(float64))
	case io_prometheus_client.MetricType_GAUGE:
		gauge := metric.(*prometheus.GaugeVec)
		gauge.WithLabelValues(dimensions...).Set(value.(float64))
	case io_prometheus_client.MetricType_COUNTER:
		counter := metric.(*prometheus.CounterVec)
		counter.WithLabelValues(dimensions...).Add(value.(float64))
	case io_prometheus_client.MetricType_HISTOGRAM:
		histogram := metric.(*prometheus.HistogramVec)
		histogram.WithLabelValues(dimensions...).Observe(value.(float64))
	}
}

func initializeMetrics(metrics []*metric, registry prometheus.Registerer) error {
	for _, metric := range metrics {
		switch metric.mType {
		case io_prometheus_client.MetricType_GAUGE:
			metric.value = prometheus.NewGaugeVec(prometheus.GaugeOpts{Name: metric.name}, metric.dimension)
		case io_prometheus_client.MetricType_COUNTER:
			metric.value = prometheus.NewCounterVec(prometheus.CounterOpts{Name: metric.name}, metric.dimension)
		case io_prometheus_client.MetricType_HISTOGRAM:
			metric.value = prometheus.NewHistogramVec(prometheus.HistogramOpts{Name: metric.name}, metric.dimension)
		case io_prometheus_client.MetricType_SUMMARY:
			metric.value = prometheus.NewSummaryVec(prometheus.SummaryOpts{Name: metric.name}, metric.dimension)
		}
		if err := registry.Register(metric.value); err != nil {
			switch err.(type) {
			case prometheus.AlreadyRegisteredError:
				continue
			}
			return err
		}
	}
	return nil
}

type kafkaProducerMetrics struct {
	producer *Producer
	registry prometheus.Registerer
}

type kafkaConsumerMetrics struct {
	consumer *Consumer
	registry prometheus.Registerer
}

type KafkaMetricsRegistry struct {
	producerMetrics []*kafkaProducerMetrics
	consumerMetrics []*kafkaConsumerMetrics
}

var defaultMetricsRegistry = &KafkaMetricsRegistry{}

func (registry *KafkaMetricsRegistry) registerProducerMetrics(producer *Producer) error {
	prodMetric := &kafkaProducerMetrics{producer, producer.config.registry}
	registry.producerMetrics = append(registry.producerMetrics, prodMetric)
	if err := prodMetric.initMetrics(); err != nil {
		return err
	}

	return nil
}

func (registry *KafkaMetricsRegistry) registerConsumerMetrics(con *Consumer) error {
	consMetric := &kafkaConsumerMetrics{consumer: con, registry: con.config.registry}
	registry.consumerMetrics = append(registry.consumerMetrics, consMetric)
	if err := consMetric.initMetrics(); err != nil {
		return err
	}
	return nil
}

func (prodMetric *kafkaProducerMetrics) initMetrics() error {
	if !defaultRegistryState.producerRegistered {
		defaultRegistryState.producerRegistered = true
		if err := initializeMetrics(producerMetricConverters, prodMetric.registry); err != nil {
			return err
		}
	}
	if prodMetric.producer.multiCluster != nil {
		multiCluster := prodMetric.producer.multiCluster.(*multiClusterProducer)
		go observeProducerEvents(multiCluster.primaryProducer, createMetricInstances(producerMetricConverters, []string{prodMetric.producer.config.clientID, prodMetric.producer.config.primary.GetClusterId()}))
		go observeProducerEvents(multiCluster.secondaryProducer, createMetricInstances(producerMetricConverters, []string{prodMetric.producer.config.clientID, prodMetric.producer.config.secondary.GetClusterId()}))
	} else {
		singleCluster := prodMetric.producer.singleCluster.(*singleClusterProducer)
		go observeProducerEvents(singleCluster.producer, createMetricInstances(producerMetricConverters, []string{prodMetric.producer.config.clientID, prodMetric.producer.config.primary.GetClusterId()}))
	}

	return nil
}

func (consMetric *kafkaConsumerMetrics) initMetrics() error {
	if !defaultRegistryState.consumerRegistered {
		defaultRegistryState.consumerRegistered = true
		if err := initializeMetrics(consumerMetricConverters, consMetric.registry); err != nil {
			return err
		}
	}
	go observerConsumerEvents(consMetric.consumer, createMetricInstances(consumerMetricConverters, []string{}))
	return nil
}

func createMetricInstances(metrics metrics, dims []string) metricInstances {
	metricInstances := []metricInstance{}
	for _, metric := range metrics {
		metricInstances = append(metricInstances, metricInstance{metric, dims})
	}
	return metricInstances
}

func observeProducerEvents(producer kafkaProducer, metricInstances metricInstances) {
	for {
		e, ok := <-producer.events()
		if !ok {
			return
		}
		switch e.(type) {
		case *kafka.Stats:
			metricMap := stats{}
			err := json.Unmarshal([]byte(e.(*kafka.Stats).String()), &metricMap)
			if err != nil {
				logger.Info("error in unmarshalling metric", ErrorField(err))
				continue
			}
			metricInstances.observe(metricMap)
		}

	}
}

func observerConsumerEvents(consumer *Consumer, metricInstances metricInstances) {
	for {
		cMetrics, ok := <-consumer.metricChan
		if !ok {
			return
		}
		statsJSON := cMetrics.stats
		metricMap := stats{}
		err := json.Unmarshal([]byte(statsJSON.String()), &metricMap)
		if err != nil {
			logger.Info("error in unmarshalling metric", ErrorField(err))
			continue
		}
		metricInstances.observeWithDim(metricMap, []string{cMetrics.clientID, cMetrics.clusterID, cMetrics.consumerTopic})
	}
}

type metricValue struct {
	dynamicLabels []string
	value         interface{}
}

type metricConverter interface {
	observe(metric *metricInstance, stats stats)
	observeWithDims(metric *metricInstance, stats stats, constantDim []string)
}

type functionMetricConverter struct {
	fn func(stats stats) ([]metricValue, error)
}

func (converter *functionMetricConverter) observe(metric *metricInstance, stats stats) {
	converter.observeWithDims(metric, stats, []string{})
}

func (converter *functionMetricConverter) observeWithDims(metric *metricInstance, stats stats, constantDim []string) {
	metricValues, err := converter.fn(stats)
	if err != nil {
		logger.Error("error in observing metric", AutoField("metric-name", metric.name), ErrorField(err))
		return
	}
	for _, metricValue := range metricValues {
		dims := append(metric.staticDim, constantDim...)
		dims = append(dims, metricValue.dynamicLabels...)
		observeValue(metric.mType, metric.value, dims, metricValue.value)
	}

}
