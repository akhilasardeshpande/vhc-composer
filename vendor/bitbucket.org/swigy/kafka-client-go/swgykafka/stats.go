package swgykafka

type stats struct {
	Name                  string                   `json:"name"`
	ClientID              string                   `json:"client_id"`
	Ts                    float64                  `json:"ts"`
	Time                  float64                  `json:"time"`
	ReplyCount            float64                  `json:"replyq"`
	MsgCount              float64                  `json:"msg_cnt"`
	MsgSize               float64                  `json:"msg_size"`
	MsgMax                float64                  `json:"msg_max"`
	MsgSizeMax            float64                  `json:"msg_size_max"`
	SimpleCnt             float64                  `json:"simple_cnt"`
	MetadataCacheCnt      float64                  `json:"metadata_cache_cnt"`
	TotalRqstSent         float64                  `json:"tx"`
	TotalBytesSent        float64                  `json:"tx_bytes"`
	TotalRspRec           float64                  `json:"rx"`
	TotalRspBytesRec      float64                  `json:"rx_bytes"`
	TotalMsgTransm        float64                  `json:"txmsgs"`
	TotalMsgBytesTransm   float64                  `json:"txmsg_bytes"`
	TotalMsgConsumed      float64                  `json:"rxmsgs"`
	TotalMsgBytesConsumed float64                  `json:"rxmsg_bytes"`
	Brokers               map[string]brokerMetrics `json:"brokers"`
	Topics                map[string]topicMetric   `json:"topics"`
	ConsumerGrp           consumerGroupMetric      `json:"cgrp"`
}

type windowStat struct {
	Max        float64 `json:"max"`
	Avg        float64 `json:"avg"`
	Sum        float64 `json:"sum"`
	Stddev     float64 `json:"stddev"`
	P50        float64 `json:"p50"`
	P75        float64 `json:"p75"`
	P90        float64 `json:"p90"`
	P95        float64 `json:"p95"`
	P99        float64 `json:"p99"`
	P99_99     float64 `json:"p99_99"`
	OutOfRange float64 `json:"outofrange"`
	HdrSize    float64 `json:"hdrsize"`
	Cnt        float64 `json:"cnt"`
}

type brokerMetrics struct {
	Name                      string     `json:"name"`
	NodeID                    float64    `json:"nodeid"`
	NodeName                  string     `json:"nodename"`
	Source                    string     `json:"source"`
	State                     string     `json:"state"`
	StateAge                  float64    `json:"stateage"`
	OutputBufCount            float64    `json:"outbuf_cnt"`
	OutputBufMsgCount         float64    `json:"outbuf_msg_cnt"`
	WaitRespCount             float64    `json:"waitresp_cnt"`
	WaitRespMsgCount          float64    `json:"waitresp_msg_cnt"`
	TotalRqstSent             float64    `json:"tx"`
	TotalBytesSent            float64    `json:"txbytes"`
	TotalTransmErrors         float64    `json:"txerrs"`
	TotalRetries              float64    `json:"txretries"`
	TotalRqstTimeouts         float64    `json:"req_timeouts"`
	TotalRspRec               float64    `json:"rx"`
	TotalRspBytesRec          float64    `json:"rxbytes"`
	TotalRqstErrors           float64    `json:"rxerrs"`
	TotalCorrErrors           float64    `json:"rxcorriderrs"`
	TotalRqstPartial          float64    `json:"rxpartial"`
	DecompressBufSizeIncrease float64    `json:"zbuf_grow"`
	BufferSizeIncrease        float64    `json:"buf_grow"`
	Wakeups                   float64    `json:"wakeups"`
	Connects                  float64    `json:"connects"`
	Disconnects               float64    `json:"disconnects"`
	IntProducerLatency        windowStat `json:"int_latency"`
	OutputBufLatency          windowStat `json:"outbuf_latency"`
	BrokerLatency             windowStat `json:"rtt"`
	Throttle                  windowStat `json:"throttle"`
	Req                       struct {
		Produce        float64 `json:"Produce"`
		Offset         float64 `json:"Offset"`
		Metadata       float64 `json:"Metadata"`
		SaslHandshake  float64 `json:"SaslHandshake"`
		ApiVersion     float64 `json:"ApiVersion"`
		InitProducerId float64 `json:"InitProducerId"`
	} `json:"req"`
	Toppars map[string]struct {
		Topic     string `json:"topic"`
		Partition int    `json:"partition"`
	} `json:"toppars"`
}

type topicMetric struct {
	Topic       string                  `json:"topic"`
	MetadataAge int64                   `json:"metadata_age"`
	BatchSize   windowStat              `json:"batchsize"`
	BatchCnt    windowStat              `json:"batchcnt"`
	Partitions  map[int]partitionMetric `json:"partitions"`
}

type consumerGroupMetric struct {
	State           string  `json:"state"`
	StateAge        float64 `json:"stateage"`
	JoinState       string  `json:"joinstate"`
	ReBalanceAge    float64 `json:"rebalance_age"`
	ReBalanceCnt    float64 `json:"rebalance_cnt"`
	ReBalanceReason string  `json:"rebalance_reason"`
	AssignmentSize  float64 `json:"assignment_size"`
}

type partitionMetric struct {
	Partition       int     `json:"partition"`
	Broker          int     `json:"broker"`
	Leader          int     `json:"leader"`
	Desired         bool    `json:"desired"`
	Unknown         bool    `json:"unknown"`
	MsgqCnt         float64 `json:"msgq_cnt"`
	MsgqBytes       float64 `json:"msgq_bytes"`
	XmitMsgqCnt     float64 `json:"xmit_msgq_cnt"`
	XmitMsgqBytes   float64 `json:"xmit_msgq_bytes"`
	FetchqCnt       float64 `json:"fetchq_cnt"`
	FetchqSize      float64 `json:"fetchq_size"`
	FetchState      string  `json:"fetch_state"`
	QueryOffset     float64 `json:"query_offset"`
	NextOffset      float64 `json:"next_offset"`
	AppOffset       float64 `json:"app_offset"`
	StoredOffset    float64 `json:"stored_offset"`
	CommitedOffset  float64 `json:"commited_offset"`
	CommittedOffset float64 `json:"committed_offset"`
	EofOffset       float64 `json:"eof_offset"`
	LoOffset        float64 `json:"lo_offset"`
	HiOffset        float64 `json:"hi_offset"`
	LsOffset        float64 `json:"ls_offset"`
	ConsumerLag     float64 `json:"consumer_lag"`
	Txmsgs          float64 `json:"txmsgs"`
	Txbytes         float64 `json:"txbytes"`
	Rxmsgs          float64 `json:"rxmsgs"`
	Rxbytes         float64 `json:"rxbytes"`
	Msgs            float64 `json:"msgs"`
	RxVerDrops      float64 `json:"rx_ver_drops"`
	MsgsInflight    float64 `json:"msgs_inflight"`
	NextAckSeq      float64 `json:"next_ack_seq"`
	NextErrSeq      float64 `json:"next_err_seq"`
	AckedMsgid      float64 `json:"acked_msgid"`
}
