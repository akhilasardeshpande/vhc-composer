package swgykafka

import (
	"fmt"
	"time"
)

/**
 * Created by Sai Ravi Teja K on 17, Dec 2019
 * © Bundl Technologies Private Ltd.
 */

const Version = "1.9"
const ConfluentKafkaVersion = "1.7.0"

func CurrentTimeMillis() int64 {
	// divide current nano seconds by 10^6
	return time.Now().UnixNano() / 1e6
}

func combineErrors(errs []error) error {
	var errStr = ""
	for _, err := range errs {
		if errStr == "" {
			errStr = fmt.Sprintf("%s", err.Error())
		} else {
			errStr = fmt.Sprintf("%s,%s", errStr, err.Error())
		}
	}
	if errStr != "" {
		return fmt.Errorf("%s", errStr)
	}
	return nil
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}