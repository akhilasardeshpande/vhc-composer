package swgykafka

import (
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"strconv"
	"time"
)

type RecordMetadata struct {
	Topic     string
	Partition int
	ValueSize int
	KeySize   int
	Timestamp time.Time
	Offset    int64
	Headers   map[string]string
}

type Record struct {
	Topic     string
	Partition int
	Value     []byte
	Key       []byte
	Timestamp time.Time
	Offset    int64
	Headers   map[string]string
}

func (r Record) String() string {
	return fmt.Sprintf("{Topic:%v, Partition:%v, Key:%v, Timestamp:%v, Offset:%v, Headers: %+v}",
		r.Topic,
		r.Partition,
		string(r.Key),
		r.Timestamp,
		r.Offset,
		r.Headers,
	)
}

func (r Record) ToMap() map[string]string {
	attributes := make(map[string]string)

	attributes["topic"] = r.Topic
	attributes["partition"] = strconv.Itoa(r.Partition)
	attributes["key"] = string(r.Key)
	attributes["timestamp"] = strconv.FormatInt(toMillisUTC(r.Timestamp), 10)
	attributes["offset"] = strconv.FormatInt(r.Offset, 10)

	for k, v := range r.Headers {
		attributes[k] = v
	}

	return attributes
}

func (r Record) ToFailoverMarker() failoverMarker {
	fm := NewFailoverMarker(string(r.Value))
	return fm
}

func (r Record) getRetryCount() int {
	if r.Headers == nil {
		return 0
	}
	retryKey := r.Headers[retryNumberKey]
	if retryKey == "" {
		retryKey = "0"
	}
	val, err := strconv.Atoi(retryKey)
	if err != nil {
		logger.Error("couldn't parse value for retry_number", AutoField("retry-number", r.Headers[retryNumberKey]))
		return -1
	}
	return val

}

func (r Record) msgTime() time.Time {
	hKey := r.Headers[recordTimestampKey]
	if hKey == "" {
		return r.Timestamp
	}
	tI, err := strconv.Atoi(hKey)
	if err != nil {
		logger.Error("error in reading time", ErrorField(err))
		return r.Timestamp
	}
	return time.Unix(0, int64(tI)*int64(time.Millisecond))

}

func (r Record) isRetryMsg() bool {
	if r.Headers == nil {
		return false
	}
	rK := r.Headers[retryNumberKey]
	if rK == "" || rK == "0" {
		return false
	}
	return true
}

func (r Record) retryConsumerGroup() string {
	if r.Headers == nil {
		return ""
	}
	return r.Headers[retryConsumerConsumerIdKey]
}

func toRecord(message *kafka.Message) *Record {
	return &Record{
		Topic:     *message.TopicPartition.Topic,
		Partition: int(message.TopicPartition.Partition),
		Value:     message.Value,
		Key:       message.Key,
		Timestamp: message.Timestamp.UTC(),
		Offset:    int64(message.TopicPartition.Offset),
		Headers:   headersToMap(message.Headers),
	}
}

func headersToMap(headers []kafka.Header) map[string]string {
	hdrMap := make(map[string]string)
	if headers == nil {
		return nil
	}
	for _, val := range headers {
		hdrMap[val.Key] = string(val.Value)
	}
	return hdrMap
}

func toMillisUTC(v time.Time) int64 {
	return v.UTC().UnixNano() / int64(time.Millisecond)
}
