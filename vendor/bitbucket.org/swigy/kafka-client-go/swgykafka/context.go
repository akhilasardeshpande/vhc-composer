package swgykafka

import (
	"context"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type fieldLogger struct {
	parent *fieldLogger
	fields []zapcore.Field
	logger *zap.Logger
}

type contextKey int

const fieldLoggerKey contextKey = iota

func NewContext(ctx context.Context, fields ...zapcore.Field) context.Context {
	return context.WithValue(ctx, fieldLoggerKey, getFieldLogger(ctx).withFields(fields))
}

func getFieldLogger(ctx context.Context) *fieldLogger {
	if fLogger, ok := ctx.Value(fieldLoggerKey).(*fieldLogger); ok {
		return fLogger
	} else {
		return nil
	}
}

//GetFields returns all of the fields which have been added to the
//current context via 'NewContext', plus all the standard fields
//emitted by all lines logged by this process.
func GetFields(ctx context.Context) []zapcore.Field {
	fLogger := getFieldLogger(ctx)
	return fLogger.GetFields()
}

func (fLogger *fieldLogger) GetFields() []zapcore.Field {
	fields := fLogger.fields
	if fLogger.parent != nil {
		fields = append(fLogger.parent.GetFields(), fields...)
	}
	return fields
}

func WithContext(ctx context.Context) *zap.Logger {
	fLogger := getFieldLogger(ctx)
	if fLogger == nil {
		// fall back to the standard logger
		return logger
	} else {
		return fLogger.logger
	}
}

func (fLogger *fieldLogger) withFields(fields []zapcore.Field) *fieldLogger {
	var zapLogger *zap.Logger
	if fLogger == nil {
		zapLogger = logger
	} else {
		zapLogger = fLogger.logger
	}
	return &fieldLogger{
		parent: fLogger,
		fields: fields,
		logger: zapLogger.With(fields...),
	}
}
