package swgykafka

import (
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/prometheus/client_model/go"
)

var consumerMetricConverters = metrics{
	{
		//Total consumer lag
		name:  "go_consumer_fetch_manager_metrics_records_lag",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				total := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					//This is needed because metrics come for all the partitions of the topic and the ones with fetch
					//state not active has consumer lag as -1. Each of our clientIds are client + thread name so we a
					//huge negative lag because of this. This can lead to suppression of actual lags due to negative values
					if partitionMetrics.ConsumerLag < 0 {
						continue
					}
					total += partitionMetrics.ConsumerLag
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, total})
			}
			return metricValues, nil
		}},
		dimension: consumerClientIDTopicDim},
	{
		name:  "go_consumer_fetch_manager_metrics_records_lead", // TODO Find Definition
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Batch message counts by topic
		name:  "go_consumer_fetch_manager_metrics_records_per_request_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				metricValues = append(metricValues, metricValue{[]string{topicName}, topicMetrics.BatchCnt.Avg})
			}
			return metricValues, nil
		}},
		dimension: consumerClientIDTopicDim},
	{
		//Batch sizes in bytes by topic
		name:  "go_consumer_fetch_manager_metrics_fetch_size_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				metricValues = append(metricValues, metricValue{[]string{topicName}, topicMetrics.BatchSize.Avg})
			}
			return metricValues, nil
		}},
		dimension: consumerClientIDTopicDim},
	{
		//Total number of messages consumed by topic
		name:  "go_consumer_fetch_manager_metrics_records_consumed_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				total := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					total += partitionMetrics.Rxmsgs
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, total})
			}
			return metricValues, nil
		}},
		dimension: consumerClientIDTopicDim},
	{
		//Total number of bytes received by topic
		name:  "go_consumer_fetch_manager_metrics_bytes_consumed_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			var metricValues []metricValue
			for topicName, topicMetrics := range stats.Topics {
				total := float64(0)
				for _, partitionMetrics := range topicMetrics.Partitions {
					total += partitionMetrics.Rxbytes
				}
				metricValues = append(metricValues, metricValue{[]string{topicName}, total})
			}
			return metricValues, nil
		}},
		dimension: consumerClientIDTopicDim},
	{
		name:  "go_consumer_coordinator_metrics_last_heartbeat_seconds_ago", //TODO Find Definition
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{}, nil
		}},
		dimension: consumerClientIDDim},
	{
		name:  "go_consumer_coordinator_metrics_commit_latency_avg", // TODO No direct way found to do this
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Current assignment's partition count
		name:  "go_consumer_coordinator_metrics_assigned_partitions",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.ConsumerGrp.AssignmentSize}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Total number of rebalances (assign or revoke).
		name:  "go_consumer_coordinator_metrics_rebalanced_count",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.ConsumerGrp.ReBalanceCnt}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Number of connection attempts, including successful and failed, and name resolution failures
		name:  "go_consumer_metrics_connection_count_sum",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.Connects
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Number of disconnects (triggered by broker, network, load-balancer, etc.).
		name:  "go_consumer_metrics_disconnect_count_sum",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.Disconnects
			}
			return []metricValue{{[]string{}, total}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Request Size in bytes
		name:  "go_consumer_metrics_request_size_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			if stats.TotalMsgTransm == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, stats.TotalMsgBytesTransm / stats.TotalMsgTransm}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Total number of requests sent to brokers
		name:  "go_consumer_metrics_request_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRqstSent}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Total number of responses received from Kafka brokers
		name:  "go_consumer_metrics_response_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRspRec}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Total number of bytes received from Kafka brokers
		name:  "go_consumer_metrics_incoming_byte_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalRspBytesRec}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Total number of bytes transmitted to Kafka brokers
		name:  "go_consumer_metrics_outgoing_byte_total",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{}, stats.TotalBytesSent}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		name:  "go_kafka_lib_version_info",
		mType: io_prometheus_client.MetricType_COUNTER,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			return []metricValue{{[]string{Version, ConfluentKafkaVersion}, 1.0}}, nil
		}},
		dimension: clientIDTopicLibDim},
	{
		name:  "go_confluent_kafka_version",
		mType: io_prometheus_client.MetricType_COUNTER,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			_, kafkaVersionStr := kafka.LibraryVersion()
			return []metricValue{{[]string{kafkaVersionStr}, 1.0}}, nil
		}},
		dimension: clientIDKafkaVerDim},
	{
		// Sum of Internal request queue latency in microseconds and Internal producer queue latency in milliseconds
		name:  "go_consumer_metrics_request_queue_time_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += (brokerMetrics.IntProducerLatency.Avg + brokerMetrics.OutputBufLatency.Avg) / 1000 // As these values are in microseconds
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: consumerClientIDDim},
	{
		//Broker latency / round-trip time in microseconds
		name:  "go_consumer_metrics_request_latency_avg",
		mType: io_prometheus_client.MetricType_GAUGE,
		converter: &functionMetricConverter{func(stats stats) ([]metricValue, error) {
			total := float64(0)
			for _, brokerMetrics := range stats.Brokers {
				total += brokerMetrics.BrokerLatency.Avg
			}
			if len(stats.Brokers) == 0 {
				return []metricValue{{[]string{}, 0.0}}, nil
			}
			return []metricValue{{[]string{}, total / float64(len(stats.Brokers))}}, nil
		}},
		dimension: consumerClientIDDim},
}
