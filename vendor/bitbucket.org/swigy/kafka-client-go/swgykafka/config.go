package swgykafka

import (
	"errors"
	"fmt"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"github.com/prometheus/client_golang/prometheus"
	"os"
	"strings"
)

// Kafka config keys
const (
	BootstrapServerConfigKey       = "bootstrap.servers"
	SaslUsernameKey                = "sasl.username"
	SaslPasswordKey                = "sasl.password"
	ClientIDConfigKey              = "client.id"
	SecurityProtocolConfigKey      = "security.protocol"
	SaslMechanismKey               = "sasl.mechanism"
	GroupIDConfigKey               = "group.id"
	MaxPollRecordConfigKey         = "max.poll.records"
	AutoOffsetResetConfigKey       = "auto.offset.reset"
	EnableAutoCommitConfigKey      = "enable.auto.commit"
	AutoCommitIntervalMsConfigKey  = "auto.commit.interval.ms"
	EnableAutoOffsetStoreConfigKey = "enable.auto.offset.store"
	AcksConfigKey                  = "acks"
	RetriesConfigKey               = "retries"
	LingerMsConfigKey              = "linger.ms"
	CompressionTypeConfigKey       = "compression.type"
	MessageTimeout                 = "message.timeout.ms"
	ReconnectBackoffMax            = "reconnect.backoff.max.ms"
	ReconnectBackoff               = "reconnect.backoff.ms"
	MetadataAgeMax                 = "metadata.max.age.ms"
	FetchWaitMax                   = "fetch.wait.max.ms"
	FetchMinBytes                  = "fetch.min.bytes"
	CheckCrcs                      = "check.crcs"
	SocketTimeout                  = "socket.timeout.ms"
	IsolationLevel                 = "isolation.level"
	ApiVersionRequestTimeout       = "api.version.request.timeout.ms"
	RequestTimeout                 = "request.timeout.ms"
	PartitionerStrategy            = "partitioner"
	HeaderRecordPath               = "record_path"
	RecordPathPrimary              = "primary"
	RecordPathSecondary            = "secondary"
	StatisticsPullInterval         = "statistics.interval.ms"
	HealthCheckIntervalMs          = "health.check.interval.ms"

	HealthDegradationThreshold     = "health.degradation.threshold"
	GlobalDualRwEnabled            = "global.dual.rw.enabled"

	// These are the current defaults in java library
	defaultGlobalDualRwEnabled  = false
	defaultRetryCount           = 10000
	defaultMessageTimeout       = 120000
	defaultFlushTimeout         = 300000
	defaultSeekTimeForSecondary = 900000
	defaultReconnectBackoff     = 50
	defaultReconnectBackoffMax  = 1000
	defaultMetadataAgeMax       = 300000
	// We want to set this to 5000, but chaos cases fail at this value. Most of the times at fetchMaxWait > 1000,
	// consumer will keep waiting for offset reset(commit offset after seeking) in
	// "Local: Waiting for coordinator" state.
	defaultFetchWaitMax                  = 500
	defaultCheckCrcs                     = true
	defaultSocketTimeout                 = 30000
	defaultIsolationLevel                = "read_uncommitted"
	defaultApiVersionRequestTimeout      = 60000
	defaultRequestTimeout                = 30000
	defaultPartitionerStrategy           = "murmur2_random"
	defaultFindOffsetForTimeTimeout      = 30000
	defaultOffsetSeekTimeout             = 30000
	defaultMarkerConsumerSleepMs         = 10000
	defaultMaxWorkers                    = 0 // Will be increased in the next major release so that worker pool is used by default.
	defaultMaxQueueLen                   = 0
	defaultJobQueueFlushTimeStepDuration = 100
	defaultLingerMs                      = 50
	defaultStatisticsPullInterval        = 60000

	defaultConcurrency = 1
	minConcurrency     = 1
	maxConcurrency     = 5

	defaultHealthDegradationThreshold = 10

	defaultFetchMinBytes = 5000
	minFetchMinBytes     = 1
	maxFetchMinBytes     = 100000

	defaultAdminClientTimeoutMs  = 5000
	defaultHealthCheckIntervalMs = 3000
	minHealthCheckIntervalMs     = 500
)

const defaultClusterId = "unknown"

// Environment Variables
var (
	LogLevel = GetEnv("LOG_LEVEL", "info")
)

// get environment variable
func GetEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return defaultValue
	}
	return value
}

func getExcludedExtraConfigsMap() map[string]bool {
	return map[string]bool{
		GlobalDualRwEnabled:        true,
		HealthDegradationThreshold: true,
	}
}

type Cluster struct {
	clusterId        string
	bootstrapServers string
	userName         string
	password         string
	authMechanism    AuthMechanism
}

func (cluster *Cluster) GetClusterId() string {
	if cluster.clusterId == "" {
		var cId string
		keys := strings.Split(cluster.bootstrapServers, ",")
		for _, key := range keys {
			trimmedKey := strings.TrimSpace(key)
			if trimmedKey == "" {
				continue
			}
			endpoint := strings.Split(trimmedKey, ":")[0]
			cId = strings.Split(endpoint, ".")[0]
			break
		}
		if cId == "" {
			cId = defaultClusterId
		}
		cluster.clusterId = cId
	}
	return cluster.clusterId
}

type ClusterBuilder struct {
	cluster *Cluster
}

func NewClusterBuilder(bootstrapServers string) *ClusterBuilder {
	return &ClusterBuilder{&Cluster{bootstrapServers: bootstrapServers}}
}

func (cb *ClusterBuilder) UserName(username string) *ClusterBuilder {
	cb.cluster.userName = username
	return cb
}

func (cb *ClusterBuilder) Password(password string) *ClusterBuilder {
	cb.cluster.password = password
	return cb
}

func (cb *ClusterBuilder) AuthMechanism(authMechanism AuthMechanism) *ClusterBuilder {
	cb.cluster.authMechanism = authMechanism
	return cb
}

func (cb *ClusterBuilder) Build() (*Cluster, error) {
	if cb.cluster.bootstrapServers == "" {
		return nil, errors.New("bootstrapServers is required")
	}
	return cb.cluster, nil
}

func (cluster *Cluster) getKafkaConfig() (*kafka.ConfigMap, error) {
	configMap := kafka.ConfigMap{BootstrapServerConfigKey: cluster.bootstrapServers}

	// Load Auth props
	properties, err := cluster.authMechanism.getAuthProperties(cluster)
	if err != nil {
		return nil, err
	}
	for k, v := range properties {
		configMap[k] = v
	}

	if err := configMap.SetKey(ReconnectBackoff, defaultReconnectBackoff); err != nil {
		return nil, err
	}
	if err := configMap.SetKey(ReconnectBackoffMax, defaultReconnectBackoffMax); err != nil {
		return nil, err
	}

	return &configMap, nil
}

func (cluster *Cluster) clone() Cluster {
	return Cluster{
		bootstrapServers: cluster.bootstrapServers,
		userName:         cluster.userName,
		password:         cluster.password,
		authMechanism:    cluster.authMechanism,
	}
}

func (cluster *Cluster) equals(cluster1 *Cluster) bool {
	if cluster == nil || cluster1 == nil {
		return cluster == cluster1
	}
	if cluster == cluster1 {
		return true
	}
	return cluster.bootstrapServers == cluster1.bootstrapServers &&
		cluster.authMechanism == cluster1.authMechanism &&
		cluster.userName == cluster1.userName &&
		cluster.password == cluster1.password
}

type AuthMechanism int

// Enum values for AuthMechanism
const (
	AuthNone AuthMechanism = iota
	AuthSaslPlain
	AuthSaslScram
)

func (mechanism AuthMechanism) getAuthProperties(cluster *Cluster) (map[string]string, error) {
	properties := map[string]string{}
	switch mechanism {
	case AuthSaslPlain:
		properties[SaslMechanismKey] = "PLAIN"
	case AuthSaslScram:
		properties[SaslMechanismKey] = "SCRAM-SHA-512"
	case AuthNone:
		return properties, nil
	default:
		return nil, fmt.Errorf("invalid auth mechanism %v", mechanism)
	}
	if cluster.userName == "" || cluster.password == "" {
		return nil, fmt.Errorf("username and password required for SASL %v auth", mechanism)
	}
	properties[SecurityProtocolConfigKey] = "SASL_SSL"
	properties[SaslUsernameKey] = cluster.userName
	properties[SaslPasswordKey] = cluster.password
	return properties, nil
}

type CommonConfig struct {
	primary           *Cluster
	secondary         *Cluster
	clientID          string
	collectMetrics    bool
	extraConfigs      map[string]interface{}
	additionalMetrics []string
	topics            map[string]*Topic
	registry          prometheus.Registerer
}

func (config *CommonConfig) getHealthCheckIntervalMs() int {
	return max(minHealthCheckIntervalMs, config.getIntExtraConfig(HealthCheckIntervalMs, defaultHealthCheckIntervalMs))
}

func (config *CommonConfig) getIntExtraConfig(configName string, defaultValue int) int {
	if val, ok := config.extraConfigs[configName]; ok {
		if intVal, typeOk := val.(int); typeOk {
			return intVal
		}
	}
	return defaultValue
}

type ConsumerAutoOffsetReset string

// Enum values for ConsumerAutoOffsetReset
const (
	Earliest ConsumerAutoOffsetReset = "earliest"
	Latest   ConsumerAutoOffsetReset = "latest"
)

type RetryMechanism string

// Enum values for RetryMechanism
const (
	RetryFixedInterval RetryMechanism = "fixed_interval"
)

type Topic struct {
	name             string
	faultStrategy    FaultStrategy
	enableEncryption bool
	keyID            string
}

type TopicBuilder struct {
	topic *Topic
}

func NewTopicBuilder(name string) *TopicBuilder {
	return &TopicBuilder{topic: &Topic{name: name}}
}

func (cb *TopicBuilder) FaultStrategy(strategy FaultStrategy) *TopicBuilder {
	cb.topic.faultStrategy = strategy
	return cb
}

func (cb *TopicBuilder) EnableEncryption(enabled bool) *TopicBuilder {
	cb.topic.enableEncryption = enabled
	return cb
}

func (cb *TopicBuilder) EncryptionKeyID(keyID string) *TopicBuilder {
	cb.topic.keyID = keyID
	return cb
}

func (cb *TopicBuilder) Build() (*Topic, error) {
	if cb.topic.name == "" {
		return nil, errors.New("name is required")
	}
	if cb.topic.faultStrategy == "" {
		cb.topic.faultStrategy = FaultNone
	}
	return cb.topic, nil
}

func (topic *Topic) clone() Topic {
	return Topic{
		name:             topic.name,
		keyID:            topic.keyID,
		faultStrategy:    topic.faultStrategy,
		enableEncryption: topic.enableEncryption,
	}
}

func (topic *Topic) IsDualRWEnabled(globalDualRwEnabled bool) bool {
	return globalDualRwEnabled && topic.faultStrategy == FaultDualRW
}

type RetryConfig struct {
	retryConcurrency int
	retries          int
	retryInterval    int
	deadLettering    bool
	retryMechanism   RetryMechanism
}

type RetryConfigBuilder struct {
	retryConfig *RetryConfig
}

func NewRetryConfigBuilder() *RetryConfigBuilder {
	return &RetryConfigBuilder{
		retryConfig: &RetryConfig{},
	}
}

func (cb *RetryConfigBuilder) RetryConcurrency(retryConcurrency int) *RetryConfigBuilder {
	cb.retryConfig.retryConcurrency = retryConcurrency
	return cb
}

func (cb *RetryConfigBuilder) Retries(retries int) *RetryConfigBuilder {
	cb.retryConfig.retries = retries
	return cb
}

func (cb *RetryConfigBuilder) RetryInterval(retryInterval int) *RetryConfigBuilder {
	cb.retryConfig.retryInterval = retryInterval
	return cb
}

func (cb *RetryConfigBuilder) DeadLettering(enabled bool) *RetryConfigBuilder {
	cb.retryConfig.deadLettering = enabled
	return cb
}

func (cb *RetryConfigBuilder) RetryMechanism(mechanism RetryMechanism) *RetryConfigBuilder {
	cb.retryConfig.retryMechanism = mechanism
	return cb
}

func (cb *RetryConfigBuilder) Build() (*RetryConfig, error) {
	if cb.retryConfig.retryMechanism == "" {
		cb.retryConfig.retryMechanism = RetryFixedInterval
	}
	if cb.retryConfig.retryInterval <= 0 {
		cb.retryConfig.retryInterval = 5000
	}

	if cb.retryConfig.retryConcurrency < 0 {
		cb.retryConfig.retryConcurrency = 0
	}

	return cb.retryConfig, nil
}

type consumerMetric struct {
	clientID      string
	clusterID     string
	consumerTopic string
	stats         *kafka.Stats
}

type ConsumerConfig struct {
	CommonConfig
	topic                    *Topic
	consumerGroupID          string
	autoOffsetReset          ConsumerAutoOffsetReset
	fetchMinBytes            int
	enableAutoCommit         bool
	autoCommitIntervalMs     int
	concurrency              int
	delayInMs                int
	seekTimeForSecondary     int
	pollTimeoutInMs          int
	retryConfig              *RetryConfig
	offsetSeekTimeout        int
	findOffsetForTimeTimeout int
	markerConsumerSleepMs    int
	//MaxPollRecords       int
}

type ConsumerConfigBuilder struct {
	config *ConsumerConfig
}

func NewConsumerConfigBuilder(primary *Cluster, secondary *Cluster, clientID string, topic *Topic, groupID string) *ConsumerConfigBuilder {
	return &ConsumerConfigBuilder{
		config: &ConsumerConfig{
			CommonConfig: CommonConfig{
				primary:        primary,
				secondary:      secondary,
				clientID:       clientID,
				collectMetrics: true,
			},
			topic:                 topic,
			consumerGroupID:       groupID,
			enableAutoCommit:      true,
			markerConsumerSleepMs: defaultMarkerConsumerSleepMs,
			fetchMinBytes:         defaultFetchMinBytes,
			concurrency:           defaultConcurrency,
		},
	}
}

func (cb *ConsumerConfigBuilder) AutoOffsetReset(reset ConsumerAutoOffsetReset) *ConsumerConfigBuilder {
	cb.config.autoOffsetReset = reset
	return cb
}

func (cb *ConsumerConfigBuilder) FetchMinBytes(fetchMinBytes int) *ConsumerConfigBuilder {
	cb.config.fetchMinBytes = fetchMinBytes
	return cb
}

func (cb *ConsumerConfigBuilder) EnableAutoCommit(autoCommit bool) *ConsumerConfigBuilder {
	cb.config.enableAutoCommit = autoCommit
	return cb
}

func (cb *ConsumerConfigBuilder) AutoCommitIntervalMs(autoCommitIntervalMs int) *ConsumerConfigBuilder {
	cb.config.autoCommitIntervalMs = autoCommitIntervalMs
	return cb
}

func (cb *ConsumerConfigBuilder) Concurrency(concurrency int) *ConsumerConfigBuilder {
	cb.config.concurrency = concurrency
	return cb
}

func (cb *ConsumerConfigBuilder) PollTimeoutInMs(timeInMs int) *ConsumerConfigBuilder {
	cb.config.pollTimeoutInMs = timeInMs
	return cb
}

func (cb *ConsumerConfigBuilder) SeekTimeForSecondary(timeInMs int) *ConsumerConfigBuilder {
	cb.config.seekTimeForSecondary = timeInMs
	return cb
}

func (cb *ConsumerConfigBuilder) DelayInMs(delayInMs int) *ConsumerConfigBuilder {
	cb.config.delayInMs = delayInMs
	return cb
}

func (cb *ConsumerConfigBuilder) RetryConfig(config *RetryConfig) *ConsumerConfigBuilder {
	cb.config.retryConfig = config
	return cb
}

func (cb *ConsumerConfigBuilder) CollectMetrics(collect bool) *ConsumerConfigBuilder {
	cb.config.collectMetrics = collect
	return cb
}

func (cb *ConsumerConfigBuilder) ExtraConfigs(configs map[string]interface{}) *ConsumerConfigBuilder {
	cb.config.extraConfigs = configs
	return cb
}

func (cb *ConsumerConfigBuilder) AdditionalMetrics(metrics []string) *ConsumerConfigBuilder {
	cb.config.additionalMetrics = metrics
	return cb
}

func (cb *ConsumerConfigBuilder) Topics(topics map[string]*Topic) *ConsumerConfigBuilder {
	cb.config.topics = topics
	return cb
}

func (cb *ConsumerConfigBuilder) Registry(registry prometheus.Registerer) *ConsumerConfigBuilder {
	cb.config.registry = registry
	return cb
}

func (cb *ConsumerConfigBuilder) MarkerConsumerSleepMs(markerConsumerSleepMs int) *ConsumerConfigBuilder {
	cb.config.markerConsumerSleepMs = markerConsumerSleepMs
	return cb
}

func (cb *ConsumerConfigBuilder) Build() (*ConsumerConfig, error) {
	if cb.config.primary == nil {
		return nil, errors.New("Primary cluster is required")
	}
	if cb.config.clientID == "" {
		return nil, errors.New("clientId is required")
	}
	if cb.config.topic == nil {
		return nil, errors.New("topic is required")
	}
	if cb.config.consumerGroupID == "" {
		return nil, errors.New("groupId is required")
	}
	if cb.config.topics == nil {
		cb.config.topics = map[string]*Topic{}
	}
	if cb.config.extraConfigs == nil {
		cb.config.extraConfigs = map[string]interface{}{}
	}

	if cb.config.fetchMinBytes < minFetchMinBytes || cb.config.fetchMinBytes > maxFetchMinBytes {
		logger.Warn("The supplied fetchMinBytes is not within the expected limit.",
			AutoField("fetchMinBytes", cb.config.fetchMinBytes), AutoField("minFetchMinBytes", minFetchMinBytes),
			AutoField("maxFetchMinBytes", maxFetchMinBytes))
		return nil, fmt.Errorf("supplied fetchMinBytes %d not within the expected limit [%d, %d]",
			cb.config.fetchMinBytes, minFetchMinBytes, maxFetchMinBytes)
	}

	if cb.config.autoOffsetReset == "" {
		cb.config.autoOffsetReset = Earliest
	}
	if cb.config.autoCommitIntervalMs <= 0 {
		cb.config.autoCommitIntervalMs = 1000
	}
	if cb.config.concurrency < minConcurrency || cb.config.concurrency > maxConcurrency {
		logger.Warn("The supplied concurrency is not within the expected limit.",
			AutoField("concurrency", cb.config.concurrency), AutoField("minConcurrency", minConcurrency),
			AutoField("maxConcurrency", maxConcurrency))
		return nil, fmt.Errorf("supplied concurrency %d not within the expected limit [%d, %d]",
			cb.config.concurrency, minConcurrency, maxConcurrency)
	}

	if cb.config.delayInMs < 0 {
		cb.config.delayInMs = 0
	}
	if cb.config.seekTimeForSecondary <= 0 {
		cb.config.seekTimeForSecondary = defaultSeekTimeForSecondary
	}

	if cb.config.pollTimeoutInMs <= 0 {
		cb.config.pollTimeoutInMs = 1000
	}

	if cb.config.retryConfig == nil {
		// Retry is disabled
		retryConfig, _ := NewRetryConfigBuilder().Build()
		cb.config.retryConfig = retryConfig
	}

	if cb.config.registry == nil {
		cb.config.registry = prometheus.DefaultRegisterer
	}

	if cb.config.markerConsumerSleepMs < 0 {
		cb.config.markerConsumerSleepMs = defaultMarkerConsumerSleepMs
	}

	cb.config.offsetSeekTimeout = defaultOffsetSeekTimeout
	cb.config.findOffsetForTimeTimeout = defaultFindOffsetForTimeTimeout
	return cb.config, nil
}

func (config *ConsumerConfig) IsDualRWEnabled() bool {
	return config.topic.IsDualRWEnabled(config.IsGlobalDualRWEnabled())
}

func (config *ConsumerConfig) getHealthDegradationThreshold() int {
	return config.getIntExtraConfig(HealthDegradationThreshold, defaultHealthDegradationThreshold)
}

type ProducerAcks string

// Enum values for ProducerAcks
const (
	AckAll  ProducerAcks = "all"
	AckNone ProducerAcks = "0"
	AckOne  ProducerAcks = "1"
)

type FaultStrategy string

// Enum values for FaultStrategy
const (
	FaultNone   FaultStrategy = "none"
	FaultDualRW FaultStrategy = "dual_rw"
)

type ProducerConfig struct {
	CommonConfig
	acks                          ProducerAcks
	retries                       int
	lingerMs                      int
	enableCompression             bool
	flushTimeout                  int
	messageTimeout                int
	maxWorkers                    int
	maxQueueLen                   int
	jobQueueFlushTimeStepDuration int
}

type ProducerConfigBuilder struct {
	config *ProducerConfig
}

func NewProducerConfigBuilder(primary *Cluster, secondary *Cluster, clientID string) *ProducerConfigBuilder {
	return &ProducerConfigBuilder{
		config: &ProducerConfig{
			CommonConfig: CommonConfig{
				primary:        primary,
				secondary:      secondary,
				clientID:       clientID,
				collectMetrics: true,
			},
			retries:           defaultRetryCount,
			enableCompression: true,
			lingerMs:          defaultLingerMs,
		},
	}
}

func (cb *ProducerConfigBuilder) Acks(acks ProducerAcks) *ProducerConfigBuilder {
	cb.config.acks = acks
	return cb
}

func (cb *ProducerConfigBuilder) Retries(retries int) *ProducerConfigBuilder {
	cb.config.retries = retries
	return cb
}

func (cb *ProducerConfigBuilder) LingerMs(lingerMs int) *ProducerConfigBuilder {
	cb.config.lingerMs = lingerMs
	return cb
}

func (cb *ProducerConfigBuilder) EnableCompression(enabled bool) *ProducerConfigBuilder {
	cb.config.enableCompression = enabled
	return cb
}

func (cb *ProducerConfigBuilder) FlushTimeout(timeoutMs int) *ProducerConfigBuilder {
	cb.config.flushTimeout = timeoutMs
	return cb
}

func (cb *ProducerConfigBuilder) CollectMetrics(collect bool) *ProducerConfigBuilder {
	cb.config.collectMetrics = collect
	return cb
}

func (cb *ProducerConfigBuilder) ExtraConfigs(configs map[string]interface{}) *ProducerConfigBuilder {
	cb.config.extraConfigs = configs
	return cb
}

func (cb *ProducerConfigBuilder) AdditionalMetrics(metrics []string) *ProducerConfigBuilder {
	cb.config.additionalMetrics = metrics
	return cb
}

func (cb *ProducerConfigBuilder) Topics(topics map[string]*Topic) *ProducerConfigBuilder {
	cb.config.topics = topics
	return cb
}

func (cb *ProducerConfigBuilder) MessageTimeout(timeout int) *ProducerConfigBuilder {
	cb.config.messageTimeout = timeout
	return cb
}

func (cb *ProducerConfigBuilder) MaxWorkers(maxWorkers int) *ProducerConfigBuilder {
	cb.config.maxWorkers = maxWorkers
	return cb
}

func (cb *ProducerConfigBuilder) MaxQueueLen(maxQueueLen int) *ProducerConfigBuilder {
	cb.config.maxQueueLen = maxQueueLen
	return cb
}

func (cb *ProducerConfigBuilder) JobQueueFlushTimeStepDuration(jobQueueFlushTimeStepDuration int) *ProducerConfigBuilder {
	cb.config.jobQueueFlushTimeStepDuration = jobQueueFlushTimeStepDuration
	return cb
}

func (cb *ProducerConfigBuilder) Registry(registry prometheus.Registerer) *ProducerConfigBuilder {
	cb.config.registry = registry
	return cb
}

func (cb *ProducerConfigBuilder) Build() (*ProducerConfig, error) {
	if cb.config.primary == nil {
		return nil, errors.New("Primary cluster is required")
	}
	if cb.config.topics == nil {
		cb.config.topics = map[string]*Topic{}
	}
	if cb.config.extraConfigs == nil {
		cb.config.extraConfigs = map[string]interface{}{}
	}
	if cb.config.acks == "" {
		cb.config.acks = AckAll
	}
	if cb.config.flushTimeout <= 0 {
		cb.config.flushTimeout = defaultFlushTimeout
	}
	if cb.config.retries < 0 {
		cb.config.retries = defaultRetryCount
	}
	if cb.config.lingerMs < 0 {
		cb.config.lingerMs = defaultLingerMs
	}
	if cb.config.messageTimeout <= 0 {
		cb.config.messageTimeout = defaultMessageTimeout
	}

	if cb.config.registry == nil {
		cb.config.registry = prometheus.DefaultRegisterer
	}

	if cb.config.maxWorkers <= 0 {
		cb.config.maxWorkers = defaultMaxWorkers
	}

	if cb.config.maxQueueLen <= 0 {
		cb.config.maxQueueLen = defaultMaxQueueLen
	}

	if cb.config.jobQueueFlushTimeStepDuration <= 0 {
		cb.config.jobQueueFlushTimeStepDuration = defaultJobQueueFlushTimeStepDuration
	}

	return cb.config, nil
}

func (config CommonConfig) loadKafkaConfig(configMap *kafka.ConfigMap, primary bool) error {
	var cluster *Cluster
	if primary {
		cluster = config.primary
	} else {
		cluster = config.secondary
	}
	configMap.SetKey(BootstrapServerConfigKey, cluster.bootstrapServers)
	configMap.SetKey(ClientIDConfigKey, config.clientID)
	configMap.SetKey(StatisticsPullInterval, defaultStatisticsPullInterval)
	// Load Auth props
	properties, err := cluster.authMechanism.getAuthProperties(cluster)
	if err != nil {
		return err
	}
	for k, v := range properties {
		configMap.SetKey(k, v)
	}
	// Load extra props
	customExtraConfigsMap := getExcludedExtraConfigsMap()
	for k, v := range config.extraConfigs {
		if _, ok := customExtraConfigsMap[k]; !ok {
			configMap.SetKey(k, v)
		}
	}
	return nil
}

func (config *CommonConfig) IsGlobalDualRWEnabled() bool {
	if val, ok := config.extraConfigs[GlobalDualRwEnabled]; ok {
		if boolVal, typeOk := val.(bool); typeOk {
			return boolVal
		}
	}
	return defaultGlobalDualRwEnabled
}

func (config ConsumerConfig) toKafkaConfig(primary bool) (*kafka.ConfigMap, error) {
	configMap := &kafka.ConfigMap{}
	configMap.SetKey(GroupIDConfigKey, config.consumerGroupID)
	configMap.SetKey(AutoOffsetResetConfigKey, string(config.autoOffsetReset))
	//configMap.SetKey(MaxPollRecordConfigKey, config.MaxPollRecords)
	configMap.SetKey(EnableAutoCommitConfigKey, config.enableAutoCommit)
	if config.enableAutoCommit {
		configMap.SetKey(EnableAutoOffsetStoreConfigKey, false)
		configMap.SetKey(AutoCommitIntervalMsConfigKey, config.autoCommitIntervalMs)
	}

	//Defaults From Java Library
	configMap.SetKey(ReconnectBackoff, defaultReconnectBackoff)
	configMap.SetKey(ReconnectBackoffMax, defaultReconnectBackoffMax)

	// Leaving to go default, java default is different
	//configMap.SetKey(MetadataAgeMax, defaultMetadataAgeMax)

	configMap.SetKey(FetchWaitMax, defaultFetchWaitMax)
	configMap.SetKey(FetchMinBytes, config.fetchMinBytes)

	//If you want to take a little hit on the performance, you can enable this
	//configMap.SetKey(CheckCrcs, defaultCheckCrcs)

	//configMap.SetKey(SocketTimeout, defaultSocketTimeout)
	configMap.SetKey(IsolationLevel, defaultIsolationLevel)

	// Leaving to go default, java default is different
	//configMap.SetKey(ApiVersionRequestTimeout, defaultApiVersionRequestTimeout)

	err := config.loadKafkaConfig(configMap, primary)
	if err != nil {
		return nil, err
	}
	return configMap, nil
}

func (config ProducerConfig) toKafkaConfig(primary bool) (*kafka.ConfigMap, error) {
	configMap := &kafka.ConfigMap{}
	if err := configMap.SetKey(AcksConfigKey, string(config.acks)); err != nil {
		return nil, err
	}
	if config.enableCompression {
		if err := configMap.SetKey(CompressionTypeConfigKey, "snappy"); err != nil {
			return nil, err
		}
	}
	if config.lingerMs >= 0 {
		//Default set is -1, if left untouched will use librdkafka library's default
		if err := configMap.SetKey(LingerMsConfigKey, config.lingerMs); err != nil {
			return nil, err
		}
	}
	if err := configMap.SetKey(RetriesConfigKey, config.retries); err != nil {
		return nil, err
	}
	if err := configMap.SetKey(MessageTimeout, config.messageTimeout); err != nil {
		return nil, err
	}
	//Keeping the default same as java library
	if err := configMap.SetKey(ReconnectBackoff, defaultReconnectBackoff); err != nil {
		return nil, err
	}
	if err := configMap.SetKey(ReconnectBackoffMax, defaultReconnectBackoffMax); err != nil {
		return nil, err
	}

	// Leaving to go default, java default is different
	/*if err := configMap.SetKey(RequestTimeout, defaultRequestTimeout); err != nil {
		return nil, err
	}*/

	if err := configMap.SetKey(PartitionerStrategy, defaultPartitionerStrategy); err != nil {
		return nil, err
	}

	// Leaving to go default, java default is different
	/*if err := configMap.SetKey(MetadataAgeMax, defaultMetadataAgeMax); err != nil {
		return nil, err
	}*/
	err := config.loadKafkaConfig(configMap, primary)
	if err != nil {
		return nil, err
	}
	return configMap, nil
}

func (config *ProducerConfig) IsDualRWEnabled() bool {
	globalDualRwEnabled := config.IsGlobalDualRWEnabled()
	for _, v := range config.topics {
		if v.IsDualRWEnabled(globalDualRwEnabled) {
			return true
		}
	}
	return false
}
