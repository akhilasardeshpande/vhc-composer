package swgykafka

import (
	"context"
	"strconv"
)

type failureHandler struct {
	consumerGroupId string
	retryConfig     *RetryConfig
	producer        *Producer
	retryTopic      string
	deadLetterTopic string
	ctx             context.Context
}

const (
	retryConsumerConsumerIdKey = "retry_consumer_group_id"
	retryNumberKey             = "retry_number"
	recordTimestampKey         = "record_timestamp"
	retrySuffix                = "_retry"
	deadLetterSuffix           = "_error"
)

func newFailureHandler(consumerGroupId string, retryConfig *RetryConfig, producer *Producer, retryTopic string, deadLetterTopic string, ctx context.Context) *failureHandler {
	return &failureHandler{consumerGroupId: consumerGroupId, retryConfig: retryConfig, producer: producer, retryTopic: retryTopic, deadLetterTopic: deadLetterTopic, ctx: ctx}
}

func (f *failureHandler) Handle(record *Record, softFailure bool) error {
	logger := WithContext(f.ctx)

	if f.shouldRetry(record, softFailure) {
		return f.retry(record)
	}

	if f.deadLetteringEnabled() {
		return f.sendToDeadLetter(record)
	}

	logger.Info("dead lettering not enabled", AutoField(messageKeyLogKey, record.Key))
	return nil
}

func (f *failureHandler) shouldRetry(record *Record, softFailure bool) bool {
	logger := WithContext(f.ctx)

	if !softFailure {
		return false
	}
	if !f.retryingEnabled() {
		logger.Info("retrying not enabled", AutoField(messageKeyLogKey, record.Key))
		return false
	}

	retryCount := record.getRetryCount()
	if retryCount < 0 || retryCount >= f.retryConfig.retries {
		logger.Info("max retries exhausted for the record", AutoField(messageKeyLogKey, record.Key))
		return false
	}
	return true
}

func (f *failureHandler) deadLetteringEnabled() bool {
	return f.retryConfig != nil && f.retryConfig.deadLettering
}

func (f *failureHandler) retryingEnabled() bool {
	return f.retryConfig != nil && f.retryConfig.retries > 0
}

func (f *failureHandler) sendToDeadLetter(record *Record) error {
	if record.Headers == nil {
		record.Headers = make(map[string]string)
	}
	record.Headers[retryConsumerConsumerIdKey] = f.consumerGroupId

	if _, err := f.producer.sendWithCustomTime(f.deadLetterTopic, record.Key, record.Value, record.Headers, record.msgTime()); err != nil {
		return err
	}
	return nil
}

func (f *failureHandler) retry(record *Record) error {
	f.setRecordRetryHeaders(record)
	if _, err := f.producer.SendBytes(f.retryTopic, record.Key, record.Value, record.Headers); err != nil {
		return err
	}
	return nil
}

func (f *failureHandler) setRecordRetryHeaders(record *Record) {
	retryCount := record.getRetryCount()
	if record.Headers == nil {
		record.Headers = make(map[string]string)
	}
	if retryCount == -1 {
		retryCount = 0 // This is for completion in logic, this will never happen as shouldRetry handles -1 case
	}
	record.Headers[retryNumberKey] = strconv.Itoa(retryCount + 1)
	record.Headers[retryConsumerConsumerIdKey] = f.consumerGroupId
	if retryCount == 0 {
		record.Headers[recordTimestampKey] = strconv.FormatInt(toMillisUTC(record.Timestamp), 10)
	}
}
