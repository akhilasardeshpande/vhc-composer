package swgykafka

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"reflect"
	"sync"
	"time"
)

var logger *zap.Logger
var loggerOnce sync.Once

const (
	partitionLogKey        = "partition"
	bootstrapServersLogKey = "bootstrap-servers"
	messageKeyLogKey       = "message-key"
	errorLogKey            = "error"
	isPrimaryLogKey = "is-primary"
)

// init initializes a thread-safe logger logger
func init() {
	// loggerOnce ensures the logger is initialized only loggerOnce
	loggerOnce.Do(func() {
		Setup(true)
	})
}

func Setup(isProd bool) {
	timeEncoder := zapcore.ISO8601TimeEncoder
	var err error
	if isProd {
		var opts []zap.Option
		opts = append(opts, zap.AddStacktrace(zap.ErrorLevel), zap.AddCaller())
		prodConfig := zap.NewProductionConfig()
		prodConfig.EncoderConfig.EncodeTime = timeEncoder
		prodConfig.Level = getLogLevel()
		prodConfig.Sampling = nil // Don't want sampling and then filtering of logs as of now
		logger, err = prodConfig.Build(opts...)
	} else {
		logger, err = zap.NewDevelopment()
	}

	if err != nil {
		panic(err)
	}
}

func WithFields(field ...zapcore.Field) *zap.Logger {
	if len(field) != 0 {
		return logger.With(field...)
	} else {
		return logger
	}
}

func ErrorField(e error) zapcore.Field {
	if e != nil {
		return AutoField(errorLogKey, e)
	}
	return AutoField(errorLogKey, "")
}

func getType(myvar interface{}) string {
	return reflect.TypeOf(myvar).String()
}

func AutoField(k string, v interface{}) zapcore.Field {
	switch v := v.(type) {
	case string:
		return zap.String(k, v)
	case *string:
		return zap.String(k, *v)
	case int64:
		return zap.Int64(k, v)
	case *int64:
		return zap.Int64(k, *v)
	case int:
		return zap.Int(k, v)
	case float64:
		return zap.Float64(k, v)
	case bool:
		return zap.Bool(k, v)
	case time.Time:
		return zap.Time(k, v)
	default:
		if e, ok := v.(error); ok {
			return zap.String(k, e.Error())
		} else if v == nil {
			return zap.String(k, "nil")
		} else {
			return zap.Any(k, v)
		}
	}
}

func getLogLevel() zap.AtomicLevel {
	switch LogLevel {
	case "debug":
		return zap.NewAtomicLevelAt(zap.DebugLevel)
	case "info":
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	case "warn":
		return zap.NewAtomicLevelAt(zap.WarnLevel)
	case "error":
		return zap.NewAtomicLevelAt(zap.ErrorLevel)
	default:
		return zap.NewAtomicLevelAt(zap.InfoLevel)
	}
}