package swgykafka

import (
	"context"
	"github.com/confluentinc/confluent-kafka-go/kafka"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

/**
 * Created by Sai Ravi Teja K on 17, Dec 2019
 * © Bundl Technologies Private Ltd.
 */

//region Cluster Health
type clusterHealth struct {
	cluster   *Cluster
	isHealthy bool
	upSince   int64
	downSince int64
}

func newClusterHealth(cluster *Cluster) *clusterHealth {
	return &clusterHealth{
		cluster:   cluster,
		isHealthy: false,
		upSince:   -1,
		downSince: -1,
	}
}

func cloneClusterHealth(health *clusterHealth) *clusterHealth {
	return &clusterHealth{
		cluster:   health.cluster,
		isHealthy: health.isHealthy,
		upSince:   health.upSince,
		downSince: health.downSince,
	}
}

func (ch *clusterHealth) update(up bool) {
	if up {
		ch.healthy()
	} else {
		ch.unHealthy()
	}
}

func (ch *clusterHealth) healthy() {
	if !ch.isHealthy {
		ch.upSince = CurrentTimeMillis()
		ch.downSince = -1
	}

	ch.isHealthy = true
}

func (ch *clusterHealth) unHealthy() {
	if ch.isHealthy || ch.downSince < 0 {
		ch.upSince = -1
		ch.downSince = CurrentTimeMillis()
	}
	ch.isHealthy = false
}

//endregion

//region Cluster Health Checker
type clusterHealthCheckerInterface interface {
	startCheck()
	addListeners(map[healthListener]struct{})
	updateListeners(health *clusterHealth)
	isClusterAvailable() bool
	shutdown()
}

type clusterHealthChecker struct {
	ticker                           *time.Ticker
	cluster                          *Cluster
	adminClient                      *kafka.AdminClient
	listeners                        map[healthListener]struct{}
	intervalInMs                     int
	shutdownOnce                     sync.Once
	clusterHealthCheckerShutdownChan chan interface{}
	clusterShutdownWaitGroup         *sync.WaitGroup
	shutdownChan                     chan interface{}
	shutdownWaitGroup                *sync.WaitGroup
	ctx                              context.Context
}

func newClusterHealthChecker(hcu *healthCheckerUpdate, shutdownChan chan interface{}, shutdownWaitGroup *sync.WaitGroup) clusterHealthCheckerInterface {
	// initialize context
	ctx := NewContext(context.Background(), AutoField(bootstrapServersLogKey,hcu.cluster.bootstrapServers))

	logger := WithContext(ctx)
	ac, err := createAdminClient(hcu.cluster)
	if err != nil {
		logger.Error("admin client created with error", ErrorField(err))
		return nil
	}

	return &clusterHealthChecker{
		// Should we add small amount of jitter in defaultHealthCheckIntervalMs to prevent thundering herd?
		ticker:                           time.NewTicker(time.Duration(hcu.healthCheckIntervalInMs) * time.Millisecond),
		cluster:                          hcu.cluster,
		adminClient:                      ac,
		intervalInMs:                     hcu.healthCheckIntervalInMs,
		listeners:                        make(map[healthListener]struct{}),
		clusterHealthCheckerShutdownChan: make(chan interface{}),
		clusterShutdownWaitGroup:         &sync.WaitGroup{},
		shutdownChan:                     shutdownChan,
		shutdownWaitGroup:                shutdownWaitGroup,
		ctx:                              ctx,
	}
}

func (chc *clusterHealthChecker) startCheck() {
	chc.clusterShutdownWaitGroup.Add(1)
	go chc.check()
}

func (chc *clusterHealthChecker) check() {
	logger := WithContext(chc.ctx)
	defer chc.shutdownWaitGroup.Done()
	defer chc.clusterShutdownWaitGroup.Done()

	health := newClusterHealth(chc.cluster)
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case sig := <-sigchan:
			logger.Info("Caught signal and terminating", AutoField("signal", sig))
			return
		case <-chc.shutdownChan:
			return
		case <-chc.clusterHealthCheckerShutdownChan:
			return
		case <-chc.ticker.C:
			up := chc.isClusterAvailable()
			health.update(up)
			chc.updateListeners(cloneClusterHealth(health))
		}
	}
}

func (chc *clusterHealthChecker) addListeners(listeners map[healthListener]struct{}) {
	// Add the new listeners
	for l := range listeners {
		chc.listeners[l] = struct{}{}
	}
}

func (chc *clusterHealthChecker) updateListeners(health *clusterHealth) {
	listeners := chc.listeners
	if listeners == nil {
		return
	}

	for listener := range listeners {
		listener.onUpdate(health.cluster, cloneClusterHealth(health))
	}
}

func (chc *clusterHealthChecker) isClusterAvailable() bool {
	logger := WithContext(chc.ctx)

	// Ask cluster for the cluster meta data
	var topic *string
	results, err := chc.adminClient.GetMetadata(topic, false, defaultAdminClientTimeoutMs)
	if err != nil || len(results.Brokers) <= 0 {
		logger.Info("cluster not available", ErrorField(err))
		return false
	}

	logger.Info("cluster is available")
	return true
}

func (chc *clusterHealthChecker) shutdown() {
	logger := WithContext(chc.ctx)

	logger.Info("Shutting down cluster health checker")
	chc.closeClusterHealthCheckerShutdownChan()
	chc.clusterShutdownWaitGroup.Wait()
	chc.adminClient.Close()
}

func (chc *clusterHealthChecker) closeClusterHealthCheckerShutdownChan() {
	chc.shutdownOnce.Do(func() {
		close(chc.clusterHealthCheckerShutdownChan)
	})
}

//endregion

//region Health Checker
var (
	initOnce              sync.Once
	healthCheckerInstance *healthChecker
)

type healthChecker struct {
	healthCheckerUpdateChan chan *healthCheckerUpdate
	HealthUpdateChan        chan *clusterHealth
	clusterHealthCheckers   map[Cluster]clusterHealthCheckerInterface
	shutdownOnce            sync.Once
	shutdownChan            chan interface{}
	shutdownWaitGroup       *sync.WaitGroup
	ctx                     context.Context
}

/* Lazy Initialization */
func newHealthChecker() *healthChecker {
	hc := &healthChecker{}
	hc.clusterHealthCheckers = make(map[Cluster]clusterHealthCheckerInterface)
	hc.shutdownChan = make(chan interface{})
	hc.healthCheckerUpdateChan = make(chan *healthCheckerUpdate)
	hc.HealthUpdateChan = make(chan *clusterHealth)
	hc.shutdownWaitGroup = &sync.WaitGroup{}
	hc.ctx = NewContext(context.Background())
	return hc
}

func initHealthCheckerInstance() {
	initOnce.Do(func() {
		healthCheckerInstance = newHealthChecker()
		healthCheckerInstance.shutdownWaitGroup.Add(1)
		go healthCheckerInstance.updateHealthChecks()
	})
}

func (hc *healthChecker) updateHealthChecks() {
	logger := WithContext(hc.ctx)
	defer hc.shutdownWaitGroup.Done()

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)
	for {
		select {
		case sig := <-sigchan:
			logger.Info("Caught signal and terminating", AutoField("signal", sig))
			hc.closeShutdownChan()
		case <-hc.shutdownChan:
			return
		case hcu := <-hc.healthCheckerUpdateChan:
			switch hcu.register {
			case true:
				hc.register(hcu)
			case false:
				hc.deregister(hcu.cluster)
			}
		}
	}
}

func (hc *healthChecker) deregister(cluster *Cluster) {
	clusterCopy := cluster.clone()
	_, ok := hc.clusterHealthCheckers[clusterCopy]
	if ok {
		hc.clusterHealthCheckers[clusterCopy].shutdown()
		delete(hc.clusterHealthCheckers, clusterCopy)
	}
}

func (hc *healthChecker) register(hcu *healthCheckerUpdate) {
	cluster := hcu.cluster.clone()
	_, isPresent := hc.clusterHealthCheckers[cluster]
	if !isPresent {
		hc.clusterHealthCheckers[cluster] = newClusterHealthChecker(hcu, hc.shutdownChan, hc.shutdownWaitGroup)
	}

	// Add new listeners
	hc.clusterHealthCheckers[cluster].addListeners(hcu.listeners)

	if !isPresent {
		hc.shutdownWaitGroup.Add(1)
		go hc.clusterHealthCheckers[cluster].startCheck()
	}
}

/* For unit testing */
func (hc *healthChecker) getCurrentHealthChecks() map[Cluster]clusterHealthCheckerInterface {
	return hc.clusterHealthCheckers
}

func (hc *healthChecker) Shutdown() {
	logger := WithContext(hc.ctx)
	logger.Info("Shutting down health checker")
	hc.closeShutdownChan()
	hc.shutdownWaitGroup.Wait()
	for _, chc := range hc.clusterHealthCheckers {
		chc.shutdown()
	}

	hc.clusterHealthCheckers = nil
}

func (hc *healthChecker) closeShutdownChan() {
	hc.shutdownOnce.Do(func() {
		close(hc.shutdownChan)
	})
}

type healthCheckerUpdate struct {
	cluster                 *Cluster
	listeners               map[healthListener]struct{}
	healthCheckIntervalInMs int
	register                bool
}

func createAdminClient(cluster *Cluster) (*kafka.AdminClient, error) {
	config, err := cluster.getKafkaConfig()
	if err != nil {
		return nil, err
	}
	return kafka.NewAdminClient(config)
}

//endregion

//region cluster Health Listener Interface
type healthListener interface {
	onUpdate(cluster *Cluster, health *clusterHealth)
}

//endregion
