package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/model"
)

func (r *mutationResolver) Login(ctx context.Context, input model.LoginRequest) (*model.LoginResponse, error) {
	return r.RMSService.LoginController(input.Username, input.Password)
}

func (r *mutationResolver) Logout(ctx context.Context, userName string) (bool, error) {
	return r.RMSService.LogoutController(userName, ctx.Value(constants.AccessToken).(string))
}

func (r *mutationResolver) Feedback(ctx context.Context, input model.FeedbackReq) (*model.FeedbackResp, error) {
	return r.HCMSResolutionService.FeedbackController(ctx.Value(constants.AccessToken).(string), input)
}

func (r *mutationResolver) UpdateSlots(ctx context.Context, restaurantID int, input []*model.UpdateSlotRequest) ([]*model.SlotsByDay, error) {
	return r.SRSService.UpdateTimeSlotsController(ctx, restaurantID, input)
}

func (r *mutationResolver) CreateTicket(ctx context.Context, ticket model.CreateTicketInput) (*model.Ticket, error) {
	return r.VHCTicketing.CreateTicketController(ctx.Value(constants.AccessToken).(string), ticket)
}

func (r *mutationResolver) UpdateTicketStatus(ctx context.Context, id string, status int64) (*model.Ticket, error) {
	return r.VHCTicketing.UpdateTicketStatusController(ctx.Value(constants.AccessToken).(string), id, status)
}

func (r *mutationResolver) CreateTicketReply(ctx context.Context, id string, reply model.TicketReplyInput) (*model.TicketReply, error) {
	return r.VHCTicketing.CreateTicketReplyController(ctx.Value(constants.AccessToken).(string), id, reply)
}

func (r *queryResolver) ResolveByUserIDAndSpaceID(ctx context.Context, input model.UserResolutionRequest) (*model.Space, error) {
	return r.HCMSResolutionService.ResolveByUserIDAndSpaceIDController(ctx.Value(constants.AccessToken).(string), input)
}

func (r *queryResolver) GetSwiggyStarRewards(ctx context.Context, input model.SwiggyStarRewardsRequest) (*model.SwiggyStarRewardsResponse, error) {
	return r.HungerGamesService.SwiggyStarRewardsController(ctx.Value(constants.AccessToken).(string), input.RestaurantIds)
}

func (r *queryResolver) GetUser(ctx context.Context) (*model.UserDetails, error) {
	return r.RMSService.GetUserController(ctx.Value(constants.AccessToken).(string))
}

func (r *queryResolver) GetSessionInfo(ctx context.Context) (*model.SessionDetails, error) {
	return r.RMSService.GetSessionInfoController(ctx.Value(constants.AccessToken).(string))
}

func (r *queryResolver) GetSlots(ctx context.Context, input model.GetSlotRequest) ([]*model.GetSlotResponse, error) {
	return r.SRSService.GetTimeSlotController(ctx, input)
}

func (r *queryResolver) Metrics(ctx context.Context, period string, restIds []int64) (*model.Metrics, error) {
	return r.VIService.MetricsController(ctx.Value(constants.AccessToken).(string), period, restIds)
}

func (r *queryResolver) GetNode(ctx context.Context, id string, dependencies []*model.DependencyTuple) (*model.GetNodeResponse, error) {
	return r.VHCService.GetNodeController(ctx, id, dependencies)
}

func (r *queryResolver) GetTicket(ctx context.Context, id string) (*model.Ticket, error) {
	return r.VHCTicketing.GetTicketController(ctx.Value(constants.AccessToken).(string), id)
}

func (r *queryResolver) GetTickets(ctx context.Context, restaurantIDs []int64, query *string, page int64) (*model.TicketsRes, error) {
	return r.VHCTicketing.GetTicketsController(ctx.Value(constants.AccessToken).(string), restaurantIDs, *query, page)
}

func (r *queryResolver) GetTicketConversations(ctx context.Context, id string) ([]*model.Conversation, error) {
	return r.VHCTicketing.GetTicketConversationsController(ctx.Value(constants.AccessToken).(string), id)
}

func (r *subscriptionResolver) OrderUpdates(ctx context.Context, restIds []int64) (<-chan *model.OrderNotification, error) {
	return r.OrderRelayer.OrderUpdateController(ctx, restIds)
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type subscriptionResolver struct{ *Resolver }
