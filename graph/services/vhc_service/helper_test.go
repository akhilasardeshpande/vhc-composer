package vhc_service

import (
	"context"
	"fmt"
	"testing"

	grpcTest "bitbucket.org/swigy/oh-my-test-helper/golang/pkg/grpc"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/mocks"
	pb "bitbucket.org/swigy/vhc-service/pkg/proto"
	"github.com/prometheus/common/log"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
)

func TestGetNode(t *testing.T) {
	container, err := grpcTest.NewContainer("7778", func(server *grpc.Server) {
		pb.RegisterVhcServiceServer(server, &mocks.UnimplementedVhcServer{})
	})
	if err != nil {
		t.Errorf("Error Initializing container. Error = %+v", err)
	}
	conf.VhcServiceURL = fmt.Sprintf("localhost:%s", "7778")
	vhc := Service{grpcClient: creategRPCClient()}

	ctx := context.Background()
	out, err := vhc.GetNode("0", nil, nil, ctx)
	if err == nil {
		t.Errorf("Expected Error: but got nil")
	}
	t.Logf("Expected Error: %v", err)

	var (
		request = &grpcTest.Request{
			Package: "vhcAdminContract",
			Service: "vhcService",
			Method:  "GetNode",
		}
		expected = mocks.FetchDummyNode()
		want     = &grpcTest.Response{
			Data: &expected,
			Err:  nil,
		}
	)

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	out, err = vhc.GetNode("0", map[string]string{"OUTLET_ID": "9990"}, nil, ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
	t.Logf("Out: %v \n expected:%v", out, expected)
	assert.Equal(t, expected.Id, out.ID)
	assert.Equal(t, expected.IsLeaf, out.IsLeaf)
	assert.Equal(t, expected.Title, out.Title)
	assert.Equal(t, expected.SubTitle, out.SubTitle)

	// Count Test
	count, err := container.GetRequestCount(request)
	if err != nil || count != 1 {
		t.Errorf("Want Request Count=1, got=%v, Error=%v", count, err)
	}
}

func TestUnmarshell(t *testing.T) {

	expected := pb.GetNodeResponse{Id: "0", IsLeaf: false, Title: "Title", SubTitle: "SubTitle", Issues: []*pb.Issues{
		{Id: "0", IsLeaf: false, Title: "Title", SubTitle: "SubTitle", Dependencies: []string{"OUTLET_ID"}}}}
	out, err := unmarshell(&expected)
	if err != nil {
		t.Errorf("Failed to Unmarshell(%v) :%v", expected, err)
	}
	assert.Equal(t, expected.Id, out.ID)
	assert.Equal(t, expected.IsLeaf, out.IsLeaf)
	assert.Equal(t, expected.Title, out.Title)
	assert.Equal(t, expected.SubTitle, out.SubTitle)

	log.Infof("Response content %v, Input Content: %v", out.Content, expected.Content)
}
