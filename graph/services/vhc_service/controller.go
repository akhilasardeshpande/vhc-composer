package vhc_service

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	pb "bitbucket.org/swigy/vhc-service/pkg/proto"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type ServiceRunner interface {
	GetNodeController(ctx context.Context, id string, dependencies []*model.DependencyTuple) (*model.GetNodeResponse, error)
}

// GetNodeController : Fetch and return node from vhc-service using node id & dependecy list
func (vhc Service) GetNodeController(ctx context.Context, id string, dependencies []*model.DependencyTuple) (*model.GetNodeResponse, error) {
	apiName := "GetNode"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	sessionKey := ctx.Value(constants.AccessToken).(string)
	log.Infof("[schema.resolvers.GetNode] Context : %v", ctx)

	dep := make(map[string]string)
	for _, val := range dependencies {
		dep[val.Key] = val.Value
	}
	dep[constants.AccessToken] = sessionKey
	dep[constants.UserAgent] = helper.DetectPlatform(ctx.Value(constants.UserAgent).(string))
	restIDs := []int64{}
	rid, ok := dep["OUTLET_ID"]
	logFields := log.Fields{
		"Node ID":      id,
		"Dependencies": dependencies,
	}

	if ok {
		restID, err := strconv.Atoi(rid)
		if err != nil {
			return nil, helper.LogAndInstrumentErrorV2(apiName, constants.InvalidOutletID, http.StatusUnprocessableEntity, logFields)
		}
		restIDs = append(restIDs, int64(restID))
	}
	//Todo - authV2Migration replace this with AuthenticateWithRestaurantsV2 when UserRole is available
	user, err := helper.AuthenticateWithRestaurantsV2(sessionKey, restIDs, txn, constants.PermissionNoneForRMSAuth)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	log.Infof("[schema.resolvers.GetNode] User:%v", user)

	var permissions []int64
	for _, v := range user.Permissions {
		permissions = append(permissions, int64(v))
	}

	accessContext := &pb.AccessFilter{
		Permissions: permissions,
	}
	ctx = newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.GetNode(id, dep, accessContext, ctx)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}
