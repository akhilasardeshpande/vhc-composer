package hunger_games_service

import (
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
)

type ServiceRunner interface {
	SwiggyStarRewardsController(sessionKey string, restIds []int64) (*model.SwiggyStarRewardsResponse, error)
}

func (svc Service) SwiggyStarRewardsController(sessionKey string, restIds []int64) (*model.SwiggyStarRewardsResponse, error) {
	apiName := "SwiggyStarRewards"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	_, err := helper.AuthenticateWithRestaurantsV2(sessionKey, restIds, txn, constants.BusinessMetricsPermission)
	if err != nil {
		return nil, helper.LogAndInstrumentError(apiName, constants.AuthorisationFailed)
	}

	resp, err := svc.SwiggyStarRewards(restIds, txn)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}
