package hunger_games_service

type Score struct {
	StatusCode    int                      `json:"statusCode"`
	StatusMessage string                   `json:"statusMessage"`
	Data          map[string][]*MetricData `json:"data"`
}

type MetricData struct {
	Name              string  `json:"name"`
	CurrentScore      int64   `json:"currentScore"`
	PreviousScore     int64   `json:"previousScore"`
	MaxScore          int64   `json:"maxScore"`
	RequiresAttention bool    `json:"requiresAttention"`
	VideoLink         string  `json:"videoLink"`
	ScorePercentage   float64 `json:"scorePercentage"`
}

type TierDetails struct {
	StatusCode    int      `json:"statusCode"`
	StatusMessage string   `json:"statusMessage"`
	Data          TierData `json:"data"`
}

type TierData struct {
	CurrentTierName         string `json:"currentTierName"`
	CurrentTierTillDate     int64  `json:"currentTierTillDate"`
	CurrentScore            int64  `json:"currentScore"`
	PreviousScore           int64  `json:"previousScore"`
	MaxScore                int64  `json:"maxScore"`
	CurrentLeaderBoardRank  int64  `json:"currentLeaderBoardRank"`
	PreviousLeaderBoardRank int64  `json:"previousLeaderBoardRank"`
	EnglishVideoLink        string `json:"englishVideoLink"`
	HindiVideoLink          string `json:"hindiVideoLink"`
	NotInProgramThisMonth   bool   `json:"notInProgramThisMonth"`
	NotInProgramLastMonth   bool   `json:"notInProgramLastMonth"`
}

type ScoreDetails struct {
	StatusCode    int       `json:"statusCode"`
	StatusMessage string    `json:"statusMessage"`
	Data          ScoreData `json:"data"`
}

type Metrics struct {
	Metrics []ScoreData `json:"metrics"`
}

type ScoreData struct {
	Name              string  `json:"name"`
	CurrentScore      int64   `json:"currentScore"`
	PreviousScore     int64   `json:"previousScore"`
	MaxScore          int64   `json:"maxScore"`
	RequiresAttention bool    `json:"requiresAttention"`
	VideoLink         string  `json:"videoLink"`
	Id                string  `json:"id"`
	ScorePercentage   float64 `json:"scorePercentage"`
	Null              bool    `json:"null"`
}
