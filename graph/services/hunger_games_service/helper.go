package hunger_games_service

import (
	"strconv"
	"sync"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type Service struct {
}

type ChannelEntity struct {
	Count int
	Err   error
}

func GetHungerGamesService() ServiceRunner {
	return &Service{}
}

func (svc Service) GetTieringDetails(restId int64, txn *newrelic.Transaction) (*TierData, error) {
	apiName := "getTier"
	url := conf.HungerGamesBaseURL + conf.HungerGamesGetTieringDetailsPath

	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	queryParams := make(map[string]string)
	queryParams["restaurantId"] = strconv.FormatInt(restId, 10)

	var content TierDetails

	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)

	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("Tier Response from Hunger Games: %v", content.Data)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (svc Service) GetScoreDetail(restId int64, txn *newrelic.Transaction) (*[]*MetricData, error) {
	apiName := "geScore"
	url := conf.HungerGamesBaseURL + conf.HungerGamesGetScorePath

	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	//Set Query params
	queryParams := make(map[string]string)
	queryParams["restaurantId"] = strconv.FormatInt(restId, 10)

	var content Score

	err := utils.MakeRequest(constants.HttpGetRequest, url, nil, requestHeader, queryParams, &content, apiName, txn)

	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("Score Response from Hunger Games: %v", &content.Data)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	metricsData := content.Data["metrics"]

	return &metricsData, nil
}

func (svc Service) SwiggyStarRewards(restIds []int64, txn *newrelic.Transaction) (*model.SwiggyStarRewardsResponse, error) {
	response := model.SwiggyStarRewardsResponse{
		Result: []*model.MetricsData{},
	}

	var waitGroup sync.WaitGroup

	for _, restId := range restIds {
		waitGroup.Add(1)
		go func(restId int64) {
			defer waitGroup.Done()
			data := svc.MetricsData(restId, txn)
			if data != nil {
				response.Result = append(response.Result, data)
			}
		}(restId)
	}
	waitGroup.Wait()

	return &response, nil
}

func (svc Service) MetricsData(restId int64, txn *newrelic.Transaction) *model.MetricsData {
	tierData, tieringErr := svc.GetTieringDetails(restId, txn)
	scoreDetails, scoreErr := svc.GetScoreDetail(restId, txn)

	if tieringErr != nil || scoreErr != nil {
		log.Errorf("Tiering error: %v.\n Score error: %v", tieringErr, scoreErr)
		return &model.MetricsData{}
	}

	if tierData.NotInProgramThisMonth {
		return nil
	}

	needsAtttentionMetrics := ""
	var currentScore int64 = 0
	var previousScore int64 = 0
	var maxScore int64 = 0

	for _, scoreData := range *scoreDetails {
		if scoreData.RequiresAttention {
			needsAtttentionMetrics += ", " + scoreData.Name
		}
		currentScore += scoreData.CurrentScore
		previousScore += scoreData.PreviousScore
		maxScore += scoreData.MaxScore
	}

	var arrowDir string

	if currentScore >= previousScore {
		arrowDir = "up"
	} else {
		arrowDir = "down"
	}

	if len(needsAtttentionMetrics) > 1 {
		needsAtttentionMetrics = needsAtttentionMetrics[2:len(needsAtttentionMetrics)]
	}

	metricsData := model.MetricsData{
		RestaurantID:           restId,
		NeedsAtttentionMetrics: needsAtttentionMetrics,
		CurrentScore:           currentScore,
		PreviousScore:          previousScore,
		MaxScore:               maxScore,
		ArrowDirection:         arrowDir,
		CurrentTierName:        tierData.CurrentTierName,
		CurrentTierScore:       tierData.CurrentScore,
		PreviousTierScore:      tierData.PreviousScore,
		MaxTierScore:           tierData.MaxScore,
	}
	log.Debugf("Response: %v", metricsData)

	return &metricsData
}
