package srs_service

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/types"
	"bitbucket.org/swigy/vhc-composer/utils"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gojek/heimdall/v7/httpclient"
	"github.com/mitchellh/mapstructure"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
	"strings"
)

type Service struct {
}

func NewSRSService() ServiceRunner {
	return &Service{}
}

func (srs Service) stringifyUpdateSlotRequest(request []*model.UpdateSlotRequest) string {
	s := "["
	for i, slot := range request {
		if i > 0 {
			s += ", "
		}
		s += fmt.Sprintf("%v", slot)
	}
	s += "]"
	return s
}

func (srs Service) UpdateSlots(ctx context.Context, user string, restaurantID int, request []*model.UpdateSlotRequest) ([]*model.SlotsByDay, error) {
	userAgent := ctx.Value(constants.UserAgent).(string)
	apiName := "SRSUpdateSlot"
	url := conf.SrsURL + conf.SrsTimeSlotPath + "bulk/" + strconv.Itoa(restaurantID)
	var logFields log.Fields
	if val, ok := ctx.Value("logFields").(log.Fields); ok {
		logFields = val
	} else {
		logFields = make(map[string]interface{})
	}
	log.WithFields(logFields).Infof("SRS Request {URL: %v}", url)
	var content SRSResponse
	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	reqBody := bytes.NewReader(body)
	h := make(map[string]string)
	h[constants.ContentType] = constants.ContentVal

	userMeta := getUserMeta(userAgent, user)
	userMetaStr, err := json.Marshal(userMeta)
	if err != nil {
		return nil, err
	}
	h[constants.UserMetaHeader] = string(userMetaStr)
	if requestID, ok := logFields["requestID"].(string); ok {
		h[constants.RequestID] = requestID
	}
	client := httpclient.NewClient(
		httpclient.WithRetryCount(constants.SRSMaxRetry),
		httpclient.WithHTTPTimeout(constants.SRSTimeout),
	)
	logFields["Request"] = srs.stringifyUpdateSlotRequest(request)
	logFields["UserMeta"] = string(userMetaStr)
	req := types.HttpRequest{Context: ctx, Url: url, Method: http.MethodPatch, Headers: h, Body: reqBody}
	meta := types.HttpRequestMeta{ApiName: apiName}
	err = utils.MakeAPICall(client, req, &content, meta)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusInternalServerError, logFields)
	}
	err = validateSRSResponse(content, logFields)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusBadRequest, logFields)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)
	var slots []Slot
	err = mapstructure.Decode(content.Data, &slots)
	return convertSlotDataToResponse(slots), err
}

func (srs Service) GetSlots(ctx context.Context, restaurantIds []int64) ([]*model.GetSlotResponse, error) {
	apiName := "SRSGetSlots"
	url := conf.SrsURL + conf.SrsTimeSlotPath + "restaurantTimings"
	var logFields log.Fields
	if val, ok := ctx.Value("logFields").(log.Fields); ok {
		logFields = val
	}
	var txn *newrelic.Transaction
	if val, ok := ctx.Value("txn").(*newrelic.Transaction); ok {
		txn = val
	}
	log.WithFields(logFields).Infof("SRS Request {URL: %v}", url)
	var content GetRestaurantTimingsResponse
	request := RestaurantTimingRequest{RestaurantIds: restaurantIds}
	body, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	reqBody := bytes.NewReader(body)
	h := make(map[string]string)
	h[constants.ContentType] = constants.ContentVal

	if requestID, ok := logFields["requestID"].(string); ok {
		h[constants.RequestID] = requestID
	}
	err = utils.MakeRequest(http.MethodPost, url, reqBody, h, nil, &content, apiName, txn)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusInternalServerError, logFields)
	}
	if content.Status == 0 {
		return nil, helper.LogAndInstrumentErrorV2(apiName, content.Message, http.StatusBadRequest, logFields)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)
	return convertRestaurantTimingToResponse(content.Data), nil
}

func convertSlotDataToResponse(slots []Slot) []*model.SlotsByDay {
	m := make(map[string][]*Slot)
	for i, slot := range slots {
		m[slot.Day] = append(m[slot.Day], &slots[i])
	}
	slotResponse := make([]*model.SlotsByDay, 0)
	for key, value := range m {
		tempSlot := &model.SlotsByDay{Day: key}
		for _, slot := range value {
			tempSlot.Slots = append(tempSlot.Slots, &model.Slot{OpenTime: slot.OpenTime, CloseTime: slot.CloseTime})
		}
		slotResponse = append(slotResponse, tempSlot)
	}
	return slotResponse
}

func getUserMeta(userAgent string, user string) UserMeta {
	var source string
	switch userAgent {
	case constants.Android.String():
		source = constants.PartnerApp.String()
	case constants.IOS.String():
		source = constants.OwnerApp.String()
	default:
		source = constants.VMS.String()
	}
	userMeta := UserMeta{Source: source}
	m := make(map[string]interface{})
	m["user"] = user
	userMeta.Meta = m
	return userMeta
}

func convertRestaurantTimingToResponse(restaurantTiming map[int64][]Slot) []*model.GetSlotResponse {
	slotResponse := make([]*model.GetSlotResponse, 0)
	for rid, slots := range restaurantTiming {
		daysMap := make(map[string][]*Slot)
		slotsByRID := &model.GetSlotResponse{RID: rid}
		for i, slot := range slots {
			daysMap[slot.Day] = append(daysMap[slot.Day], &slots[i])
		}
		for key, value := range daysMap {
			slotsByDay := &model.SlotsByDay{Day: key}
			for _, slot := range value {
				slotsByDay.Slots = append(slotsByDay.Slots, &model.Slot{OpenTime: slot.OpenTime, CloseTime: slot.CloseTime})
			}
			slotsByRID.Days = append(slotsByRID.Days, slotsByDay)
		}
		slotResponse = append(slotResponse, slotsByRID)
	}
	return slotResponse
}

func validateSRSResponse(content SRSResponse, logFields log.Fields) error {
	if content.Status == 0 {
		if strings.Contains(content.Message, constants.SRSValidationError) {
			fieldValidationError := FieldValidationError{}
			log.WithFields(logFields).Infof("error in validation: %+v", content.Data)
			err := mapstructure.Decode(content.Data, &fieldValidationError)
			if err != nil {
				return err
			}
			var errorsArr []string
			for _, validationError := range fieldValidationError.Errors {
				errorsArr = append(errorsArr, validationError.Value)
			}
			return errors.New(strings.Join(errorsArr, ","))
		}
		return errors.New(content.Message)
	}
	return nil
}
