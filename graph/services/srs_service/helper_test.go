package srs_service

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"context"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"testing"
)

func TestService_UpdateSlots(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.SrsURL + conf.SrsTimeSlotPath + "bulk/" + "1234"
	mockData, err := ioutil.ReadFile("../../../mocks/data/srs_update_slot_success_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("PATCH", URL,
		httpmock.NewBytesResponder(200, mockData))

	parentCxt := context.Background()
	ctx := context.WithValue(parentCxt, constants.UserAgent, "ios")
	request := make([]*model.UpdateSlotRequest, 0)
	slots := make(map[string]interface{})
	slots["open_time"] = 1100
	slots["close_time"] = 1200
	request = append(request, &model.UpdateSlotRequest{Day: "Mon", Slots: slots})
	res, err := Service{}.UpdateSlots(ctx, "", 1234, request)
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 2, len(res), "Test 1 failed")
	for _, data := range res {
		switch data.Day {
		case "Mon":
			assert.Equal(t, len(data.Slots), 2, "Test 2 failed")
			assert.Equal(t, data.Slots[0].OpenTime, 200, "Test 3 failed")
			assert.Equal(t, data.Slots[0].CloseTime, 400, "Test 4 failed")
			assert.Equal(t, data.Slots[1].OpenTime, 1100, "Test 5 failed")
			assert.Equal(t, data.Slots[1].CloseTime, 2000, "Test 6 failed")
		case "Tue":
			assert.Equal(t, len(data.Slots), 1, "Test 7 failed")
			assert.Equal(t, data.Slots[0].OpenTime, 1100, "Test 8 failed")
			assert.Equal(t, data.Slots[0].CloseTime, 2000, "Test 9 failed")
		default:
			t.Error(data.Day)
		}
	}
}

func TestService_UpdateSlots2(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.SrsURL + conf.SrsTimeSlotPath + "bulk/" + "1234"
	mockData, err := ioutil.ReadFile("../../../mocks/data/srs_update_slot_failed_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("PATCH", URL,
		httpmock.NewBytesResponder(200, mockData))

	parentCxt := context.Background()
	ctx := context.WithValue(parentCxt, constants.UserAgent, "ios")
	request := make([]*model.UpdateSlotRequest, 0)
	slots := make(map[string]interface{})
	slots["open_time"] = 1100
	slots["close_time"] = 1200
	request = append(request, &model.UpdateSlotRequest{Day: "Mon", Slots: slots})
	_, err = Service{}.UpdateSlots(ctx, "", 1234, request)
	if err == nil {
		t.Error(err)
	}
	assert.Equal(t, err != nil, true, "Test 1 failed")
	assert.Equal(t, err.Error(), "input: testError", "Test 2 failed")
}

func TestService_UpdateSlots3(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.SrsURL + conf.SrsTimeSlotPath + "bulk/" + "1234"
	mockData, err := ioutil.ReadFile("../../../mocks/data/srs_update_slot_failed_validation_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("PATCH", URL,
		httpmock.NewBytesResponder(406, mockData))

	parentCxt := context.Background()
	ctx := context.WithValue(parentCxt, constants.UserAgent, "ios")
	request := make([]*model.UpdateSlotRequest, 0)
	slots := make(map[string]interface{})
	slots["open_time"] = 1100
	slots["close_time"] = 1200
	request = append(request, &model.UpdateSlotRequest{Day: "Mon", Slots: slots})
	_, err = Service{}.UpdateSlots(ctx, "", 1234, request)
	if err == nil {
		t.Error(err)
	}
	assert.Equal(t, err != nil, true, "Test 1 failed")
	assert.Equal(t, err.Error(), "input: Slots overlapping for Mon", "Test 2 failed")
}

func TestService_GetSlots(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.SrsURL + conf.SrsTimeSlotPath + "restaurantTimings"
	mockData, err := ioutil.ReadFile("../../../mocks/data/srs_get_slot_success_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	ctx := context.Background()
	res, err := Service{}.GetSlots(ctx, []int64{9990, 3456})
	if err != nil {
		t.Error(err)
	}
	assert.Equal(t, 2, len(res))
	for _, data := range res {
		switch data.RID {
		case 9990:
			for _, d := range data.Days {
				switch d.Day {
				case "Mon":
					assert.Equal(t, len(d.Slots), 2, "Test 2 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 200, "Test 3 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 400, "Test 4 failed")
					assert.Equal(t, d.Slots[1].OpenTime, 600, "Test 5 failed")
					assert.Equal(t, d.Slots[1].CloseTime, 800, "Test 6 failed")
				case "Thu":
					assert.Equal(t, len(d.Slots), 2, "Test 7 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 8 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 9 failed")
					assert.Equal(t, d.Slots[1].OpenTime, 1200, "Test 10 failed")
					assert.Equal(t, d.Slots[1].CloseTime, 2200, "Test 11 failed")
				default:
					t.Error(d.Day)
				}
			}
		case 3456:
			for _, d := range data.Days {
				switch d.Day {
				case "Mon":
					assert.Equal(t, len(d.Slots), 1, "Test 12 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 13 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 14 failed")
				case "Tue":
					assert.Equal(t, len(d.Slots), 1, "Test 15 failed")
					assert.Equal(t, d.Slots[0].OpenTime, 0, "Test 16 failed")
					assert.Equal(t, d.Slots[0].CloseTime, 200, "Test 17 failed")
				default:
					t.Error(d.Day)
				}
			}
		default:
			t.Error(data.RID)
		}
	}
}

func TestService_GetSlots2(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	URL := conf.SrsURL + conf.SrsTimeSlotPath + "restaurantTimings"
	mockData, err := ioutil.ReadFile("../../../mocks/data/srs_get_slot_failed_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.RegisterResponder("POST", URL,
		httpmock.NewBytesResponder(200, mockData))

	ctx := context.Background()
	_, err = Service{}.GetSlots(ctx, []int64{9990, 3456})
	if err == nil {
		t.Error(err)
	}
	assert.Equal(t, err != nil, true, "Test 1 failed")
	assert.Equal(t, err.Error(), "input: testError", "Test 2 failed")
}
