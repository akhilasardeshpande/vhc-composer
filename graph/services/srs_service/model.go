package srs_service

// Slot struct
type Slot struct {
	Day       string `json:"day"`
	OpenTime  int    `json:"openTime"`
	CloseTime int    `json:"closeTime"`
}

// SRSResponse - response body of patch slot api
type SRSResponse struct {
	Status  int         `json:"status"`
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type UserMeta struct {
	Source string                 `json:"source"`
	Meta   map[string]interface{} `json:"meta"`
}

type RestaurantTimingRequest struct {
	RestaurantIds []int64 `json:"restaurant_ids"`
}

// GetRestaurantTimingsResponse - response body of restaurant timing api
type GetRestaurantTimingsResponse struct {
	Status  int              `json:"status"`
	Code    string           `json:"code"`
	Message string           `json:"message"`
	Data    map[int64][]Slot `json:"data"`
}

type FieldValidation struct {
	Field string `json:"field" mapstructure:"field"`
	Value string `json:"value" mapstructure:"value"`
}

type FieldValidationError struct {
	Errors []FieldValidation `json:"field_validation_errors" mapstructure:"field_validation_errors"`
}
