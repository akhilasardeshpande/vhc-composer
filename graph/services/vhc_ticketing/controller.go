package vhc_ticketing

import (
	"context"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

// Service for vhc-ticketing
type Service struct {
	grpcClient *grpc.ClientConn
}

// GetTicketController for getting one ticket by ticket ID
func (vhc Service) GetTicketController(accessToken, id string) (*model.Ticket, error) {
	apiName := "GetTicket"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	user, err := helper.AuthenticateV2(accessToken, txn)
	logFields := log.Fields{constants.TicketID: id}
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthorisationFailed, http.StatusUnauthorized, logFields)
	}
	ctx := newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.GetTicket(id, ctx)
	if err != nil {
		metric.Api.IncrementCounter(apiName, err)
		return resp, err
	}
	if resp.CustomFields == nil || resp.CustomFields.CfRestaurantID == nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthFailedMissingFields, http.StatusUnauthorized, logFields)
	}

	restID, permission := *resp.CustomFields.CfRestaurantID, resp.CustomFields.CfPermission
	if helper.AuthoriseRestAndPermissionsV2(nil, user, []int64{restID}, permission) != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthorisationFailed, http.StatusUnauthorized, logFields)
	}
	metric.Api.IncrementSuccessCounter(apiName)

	return resp, err
}

// GetTicketsController : Feteches all visible tickets based on query
func (vhc Service) GetTicketsController(accessToken string, restIDs []int64, query string, page int64) (*model.TicketsRes, error) {
	apiName := "GetTickets"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	logFields := log.Fields{constants.Query: query, constants.RestIDs: restIDs, constants.Page: page}

	user, err := helper.AuthenticateWithRestaurantsV2(accessToken, restIDs, txn, constants.PermissionNoneForRMSAuth)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthorisationFailed, http.StatusUnauthorized, logFields)
	}
	if strings.Contains(query, constants.CfRestaurantID) || strings.Index(query, "(") > strings.Index(query, ")") {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.InvalidGetTicketsQuery, http.StatusUnauthorized, logFields)
	}

	ctx := newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.GetTickets(restIDs, query, page, user.Permissions, ctx)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err

}

// GetTicketConversationsController : Fetches all the conversations related to the given ticket ID
func (vhc Service) GetTicketConversationsController(accessToken string, id string) ([]*model.Conversation, error) {
	apiName := "GetTicketConversations"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	logFields := log.Fields{constants.TicketID: id}

	user, err := helper.AuthenticateV2(accessToken, txn)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthorisationFailed, http.StatusUnauthorized, logFields)
	}
	ctx := newrelic.NewContext(context.Background(), txn)
	e := vhc.GetTicketAuthAsync(id, user, ctx)

	ctx = newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.GetTicketConversations(id, ctx)
	err1 := <-e
	if err1 != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err1.Error(), http.StatusUnauthorized, logFields)
	}
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// CreateTicketController : creates ticket in freshdesk
func (vhc Service) CreateTicketController(accessToken string, ticket model.CreateTicketInput) (*model.Ticket, error) {
	apiName := "CreateTicket"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	logFields := log.Fields{constants.RestID: ticket.CustomFields.CfRestaurantID}

	user, err := helper.AuthenticateWithRestaurantsV2(accessToken, []int64{ticket.CustomFields.CfRestaurantID}, txn, constants.PermissionNoneForRMSAuth)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	ctx := newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.CreateTicket(ticket, user.Username, ctx)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// UpdateTicketStatusController : Updates given ticket with the specified status
func (vhc Service) UpdateTicketStatusController(accessToken string, id string, status int64) (*model.Ticket, error) {
	apiName := "UpdateTicketStatus"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	logFields := log.Fields{constants.Status: status, constants.TicketID: id}

	user, err := helper.AuthenticateV2(accessToken, txn)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	log.WithFields(logFields).Infof("User Mobile: %s", user.Username)
	ctx := newrelic.NewContext(context.Background(), txn)
	err = <-vhc.GetTicketAuthAsync(id, user, ctx)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}
	ctx = newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.UpdateTicketStatus(id, status, ctx)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}

// CreateTicketReplyController : Reply to a ticket in freshdesk with following content
func (vhc Service) CreateTicketReplyController(accessToken string, id string, reply model.TicketReplyInput) (*model.TicketReply, error) {
	apiName := "CreateTicketReply"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	logFields := log.Fields{constants.TicketID: id}

	user, err := helper.AuthenticateV2(accessToken, txn)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, constants.AuthorisationFailed, http.StatusUnauthorized, logFields)
	}
	log.WithFields(logFields).Infof("User Mobile: %s", user.Username)
	ctx := newrelic.NewContext(context.Background(), txn)
	err = <-vhc.GetTicketAuthAsync(id, user, ctx)
	if err != nil {
		return nil, helper.LogAndInstrumentErrorV2(apiName, err.Error(), http.StatusUnauthorized, logFields)
	}

	ctx = newrelic.NewContext(context.Background(), txn)
	resp, err := vhc.CreateTicketReply(id, reply, ctx)
	metric.Api.IncrementCounter(apiName, err)

	return resp, err
}
