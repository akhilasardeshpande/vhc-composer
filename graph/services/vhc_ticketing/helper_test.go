package vhc_ticketing

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"testing"

	grpcTest "bitbucket.org/swigy/oh-my-test-helper/golang/pkg/grpc"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/mocks"
	pb "bitbucket.org/swigy/vhc-ticketing/proto"
	"github.com/99designs/gqlgen/graphql"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	request = &grpcTest.Request{
		Package: "ticket",
		Service: "TicketService",
		Method:  "GetTicket",
	}
)

func TestTicketingService(t *testing.T) {
	container, err := grpcTest.NewContainer("7780", func(server *grpc.Server) {
		pb.RegisterTicketServiceServer(server, &mocks.UnimplementedVhcTicketingServer{})
	})
	if err != nil {
		t.Errorf("Error Initializing container. Error = %+v", err)
	}
	conf.VhcTicketingURL = "localhost:7780"
	vhc := NewVHCTicketing()
	testGetTicket(t, container, *vhc)
	testGetTickets(t, container, *vhc)
	testGetTicketConversations(t, container, *vhc)
	testCreateTicket(t, container, *vhc)
	testUpdateTicket(t, container, *vhc)
	testCreateTicketReply(t, container, *vhc)

	testGetTicketAuthAsync(t, container, *vhc)

}

func testGetTicketAuthAsync(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	request.Method = "GetTicket"
	expected := mocks.FetchDummyTicket()
	expected.CustomFields = &pb.CustomField{
		CfRestaurantId: 9990,
		CfPermission:   2,
	}
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}
	container.DeleteAllStubs()
	err := container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	user := &model.SessionDetails{
		Permissions: []int{0, 1, 2, 3, 4},
		Restaurants: []int64{9990},
	}
	ctx := context.Background()
	err = <-vhc.GetTicketAuthAsync(fmt.Sprint(expected.Id), user, ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
		t.Errorf("Expected: %+v", expected.CustomFields)
	}

	// Failure cases
	expected.CustomFields.CfPermission = 5
	createStub(t, container, "GetTicket", want)
	err = <-vhc.GetTicketAuthAsync(fmt.Sprint(expected.Id), user, ctx)

	expected.CustomFields = nil
	createStub(t, container, "GetTicket", want)
	err1 := <-vhc.GetTicketAuthAsync(fmt.Sprint(expected.Id), user, ctx)

	want.Err = errors.New("Ticket Not found")
	createStub(t, container, "GetTicket", want)
	err2 := <-vhc.GetTicketAuthAsync(fmt.Sprint(expected.Id), user, ctx)

	if err == nil || err1 == nil || err2 == nil {
		t.Errorf("Expected Error: but got nil")
	}
}

func testGetTicket(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	out, err := vhc.GetTicket("1", ctx)
	_, err1 := vhc.GetTicket("a", ctx)
	if err == nil || err1 == nil {
		t.Errorf("Expected Error: but got nil")
	}
	//t.Logf("Expected Error: %v", err)

	request.Method = "GetTicket"
	expected := mocks.FetchDummyTicket()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	out, err = vhc.GetTicket("46", ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
	t.Logf("Out: %v \n expected:%v", out.ID, expected.Id)
	assert.Equal(t, fmt.Sprint(expected.Id), out.ID)
}

func testGetTickets(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	_, err := vhc.GetTickets([]int64{9990}, "", 1, []int{0}, ctx)
	if err == nil {
		t.Errorf("Expected Error: but got nil")
	}
	_, _ = vhc.GetTickets([]int64{}, "", 1, []int{0}, ctx)

	request.Method = "GetTickets"
	expected := mocks.FetchDummyTickets()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	out, err := vhc.GetTickets([]int64{9990}, "id=46", 1, []int{0}, ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
	t.Logf("Out: %v \n expected:%v", out.Results[0].ID, expected.Results[0].Id)
	assert.Equal(t, fmt.Sprint(expected.Results[0].Id), out.Results[0].ID)
}

func testGetTicketConversations(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	_, err := vhc.GetTicketConversations("1", ctx)
	_, err1 := vhc.GetTicketConversations("a", ctx)
	if err == nil || err1 == nil {
		t.Errorf("Expected Error: but got nil")
	}

	request.Method = "GetTicketConversations"
	expected := mocks.FetchTicketConversation()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	out, err := vhc.GetTicketConversations("48", ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
	t.Logf("Out: %+v \n expected:%+v", out, &expected)
	assert.Equal(t, fmt.Sprint(expected.Conversations[0].Id), out[0].ID)
}

func testCreateTicket(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	_, err := vhc.CreateTicket(model.CreateTicketInput{}, "9990", ctx)
	if err == nil {
		t.Errorf("Expected Error: but got nil")
	}

	request.Method = "CreateTicket"
	expected := mocks.FetchDummyTicket()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	_, err = vhc.CreateTicket(model.CreateTicketInput{}, "9990", ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
}

func testUpdateTicket(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	_, err := vhc.UpdateTicketStatus("48", 22, ctx)
	_, err1 := vhc.UpdateTicketStatus("a", 22, ctx)
	if err == nil || err1 == nil {
		t.Errorf("Expected Error: but got nil")
	}

	request.Method = "UpdateTicket"
	expected := mocks.FetchDummyTicket()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	_, err = vhc.UpdateTicketStatus("48", 2, ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
}

func createStub(t *testing.T, container *grpcTest.GRPC, method string, want *grpcTest.Response) {
	container.DeleteAllStubs()

	request.Method = method
	err := container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}
}
func testCreateTicketReply(t *testing.T, container *grpcTest.GRPC, vhc Service) {
	ctx := context.Background()
	_, err := vhc.CreateTicketReply("1", model.TicketReplyInput{}, ctx)
	_, err1 := vhc.CreateTicketReply("a", model.TicketReplyInput{}, ctx)

	if err == nil || err1 == nil {
		t.Errorf("Expected Error: but got nil")
	}

	request.Method = "CreateTicketReply"
	expected := mocks.FetchDummyTicketReplyRes()
	want := &grpcTest.Response{
		Data: &expected,
		Err:  nil,
	}

	err = container.CreateStub(&grpcTest.Stub{
		Request:  request,
		Response: want,
	})
	if err != nil {
		t.Errorf("Error Setting Stub. Error = %+v", err)
	}

	_, err = vhc.CreateTicketReply("48", model.TicketReplyInput{}, ctx)

	if err != nil {
		t.Errorf("Error: %v", err)
	}
}

func TestConvUploadToFileAttachment(t *testing.T) {
	File := graphql.Upload{
		Filename: "f1",
		File:     bytes.NewReader([]byte("Hi")),
	}
	inFiles := []*graphql.Upload{&File}
	ConvUploadToFileAttachment(inFiles)
}

func TestErrorHandler(t *testing.T) {
	err := fmt.Errorf("test error")
	logFields := log.Fields{"API Name": "TestErrorHandler"}

	errorHandler(err, nil)
	errorHandler(status.Errorf(codes.Internal, err.Error()), logFields)
	errorHandler(status.Errorf(codes.Unknown, err.Error()), logFields)
	errorHandler(status.Errorf(codes.OK, err.Error()), logFields)

}
