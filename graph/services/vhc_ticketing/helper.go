package vhc_ticketing

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	pb "bitbucket.org/swigy/vhc-ticketing/proto"
	"github.com/99designs/gqlgen/graphql"
	"github.com/newrelic/go-agent/v3/integrations/nrgrpc"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetTicket for getting one ticket by ticket ID
func (vhc Service) GetTicket(id string, ctx context.Context) (*model.Ticket, error) {
	apiName := "GetTicket"
	logFields := log.Fields{"Ticket ID": id, "API Name": apiName}
	log.WithFields(logFields).Infof("Invoked GetTicket")
	ticketID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Invalid Ticket ID")
		return nil, errors.New("ERROR: invalid ID")
	}

	c := pb.NewTicketServiceClient(vhc.grpcClient)

	startTime := time.Now()
	resp, err := c.GetTicket(ctx, &pb.TicketReq{Id: ticketID})
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	data := &model.Ticket{}
	if err := helper.HandleGqItemID(resp, data); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	data.ID = fmt.Sprintf("%v", resp.Id)

	return data, nil
}

// GetTickets : Feteches all visible tickets based on query
func (vhc Service) GetTickets(restIDs []int64, query string, page int64, permissions []int, ctx context.Context) (*model.TicketsRes, error) {
	apiName := "GetTickets"
	logFields := log.Fields{"Input query": query, "page": page, "API Name": apiName, "Restaurant IDs": restIDs}
	log.WithFields(logFields).Info("Invoked GetTicket")

	if len(restIDs) == 0 {
		return &model.TicketsRes{
			Results: nil,
			Total:   0,
		}, nil
	}

	filter := fmt.Sprintf("cf_restaurant_id:%d", restIDs[0])
	for _, restID := range restIDs[1:] {
		filter = fmt.Sprintf("%s OR cf_restaurant_id:%d", filter, restID)
	}

	filter2 := fmt.Sprint("cf_permission:null")
	for _, permission := range permissions {
		filter2 = fmt.Sprintf("%s OR cf_permission:%d", filter2, permission)
	}

	if query == "" {
		query = fmt.Sprintf("(%s) AND (%s)", filter, filter2)
	} else {
		query = fmt.Sprintf("(%s) AND (%s) AND (%s)", filter, filter2, query)
	}
	log.Infof("Query: %s", query)
	logFields["Final Query"] = query
	c := pb.NewTicketServiceClient(vhc.grpcClient)
	startTime := time.Now()
	resp, err := c.GetTickets(ctx, &pb.TicketsReq{Query: query, Page: page})
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	results := []*model.Ticket{}

	for _, pbTicket := range resp.Results {
		data := &model.Ticket{}
		if err := helper.HandleGqItemID(pbTicket, data); err != nil {
			log.WithFields(logFields).Error(err)
			return nil, err
		}
		data.ID = fmt.Sprintf("%v", pbTicket.Id)
		results = append(results, data)
	}

	return &model.TicketsRes{
		Results: results,
		Total:   resp.Total,
	}, nil
}

// GetTicketConversations : Fetches all the conversations related to the given ticket ID
func (vhc Service) GetTicketConversations(id string, ctx context.Context) ([]*model.Conversation, error) {
	apiName := "GetTicketConversations"
	logFields := log.Fields{"ConversationID": id, "API Name": apiName}
	log.WithFields(logFields).Info("Invoked GetTicketConversations")

	convID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Invalid Ticket Conversation ID")
		return nil, errors.New("ERROR: invalid ID")
	}

	c := pb.NewTicketServiceClient(vhc.grpcClient)
	startTime := time.Now()
	resp, err := c.GetTicketConversations(context.Background(), &pb.TicketConversationReq{Id: convID})
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	results := []*model.Conversation{}

	for _, conv := range resp.Conversations {
		data := &model.Conversation{}
		if err := helper.HandleGqItemID(conv, data); err != nil {
			log.WithFields(logFields).Error(err)
			return nil, err
		}
		data.ID = fmt.Sprintf("%v", conv.Id)
		results = append(results, data)
	}

	return results, nil
}

func StreamToByte(stream io.Reader) []byte {
	buf := new(bytes.Buffer)
	buf.ReadFrom(stream)
	return buf.Bytes()
}

func ConvUploadToFileAttachment(inFiles []*graphql.Upload) []*pb.FileAttachment {

	outFiles := make([]*pb.FileAttachment, len(inFiles))
	for i, _ := range inFiles {
		pblogFields := log.Fields{
			"Filename":     inFiles[i].Filename,
			"Content type": inFiles[i].ContentType,
			"Size":         inFiles[i].Size,
		}
		var temp pb.FileAttachment
		temp.ContentType = inFiles[i].ContentType
		temp.FileName = inFiles[i].Filename
		temp.Size = inFiles[i].Size
		temp.File = StreamToByte(inFiles[i].File)

		outFiles[i] = &temp
		log.WithFields(pblogFields).Infof("Proto attachments: %+v, Out: %v", inFiles[i], (outFiles[i].FileName))
	}
	return outFiles
}

// CreateTicket : creates ticket in freshdesk
func (vhc Service) CreateTicket(ticket model.CreateTicketInput, id string, ctx context.Context) (*model.Ticket, error) {
	apiName := "CreateTicket"
	logFields := log.Fields{"ticket": ticket, "API Name": apiName}
	log.WithFields(logFields).Info("Invoked CreateTicket")

	c := pb.NewTicketServiceClient(vhc.grpcClient)

	ticket.UniqueExternalID = &id

	createData := &pb.CreateTicketReq{}
	attachInput := make([]*graphql.Upload, 0)
	attachInput = append(attachInput, ticket.Attachments...)
	ticket.Attachments = nil
	if err := helper.HandleGqItemID(ticket, createData); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	createData.Attachments = ConvUploadToFileAttachment(attachInput)

	startTime := time.Now()
	resp, err := c.CreateTicket(ctx, createData)
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	data := &model.Ticket{}
	if err := helper.HandleGqItemID(resp, data); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	data.ID = fmt.Sprintf("%v", resp.Id)

	return data, nil
}

// UpdateTicketStatus : Updates the status of the ticket with the specified value
func (vhc Service) UpdateTicketStatus(id string, status int64, ctx context.Context) (*model.Ticket, error) {
	apiName := "UpdateTicketStatus"
	logFields := log.Fields{"id": id, "status": status, "API Name": apiName}
	log.WithFields(logFields).Info("Invoked UpdateTicketStatus")

	ticketID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Invalid Ticket ID")
		return nil, errors.New("ERROR: invalid ID")
	}

	c := pb.NewTicketServiceClient(vhc.grpcClient)

	updateTicketReq := &pb.UpdateTicketReq{
		Id:     ticketID,
		Ticket: &pb.UpdateTicketInput{Status: status},
	}

	startTime := time.Now()
	resp, err := c.UpdateTicket(ctx, updateTicketReq)
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	data := &model.Ticket{}
	if err := helper.HandleGqItemID(resp, data); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	data.ID = fmt.Sprintf("%v", resp.Id)

	return data, nil
}

// CreateTicketReply : Reply to a ticket in freshdesk with following content
func (vhc Service) CreateTicketReply(id string, reply model.TicketReplyInput, ctx context.Context) (*model.TicketReply, error) {
	apiName := "CreateTicketReply"
	logFields := log.Fields{"ticketID": id, "API Name": apiName}
	log.WithFields(logFields).Info("Invoked CreateTicketReply")

	ticketID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		log.WithFields(logFields).WithError(err).Error("Invalid Ticket ID")
		return nil, errors.New("ERROR: invalid ID")
	}

	c := pb.NewTicketServiceClient(vhc.grpcClient)

	replyInput := &pb.TicketReply{}
	attachInput := make([]*graphql.Upload, 0)
	attachInput = append(attachInput, reply.Attachments...)
	reply.Attachments = nil
	if err := helper.HandleGqItemID(reply, replyInput); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	replyInput.Attachments = ConvUploadToFileAttachment(attachInput)

	startTime := time.Now()
	resp, err := c.CreateTicketReply(ctx, &pb.TicketReplyReq{
		Reply: replyInput,
		Id:    ticketID,
	})
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	metric.ExternalApi.IncrementCounter(apiName, err)
	if err != nil {
		return nil, errorHandler(err, logFields)
	}

	data := &model.TicketReply{}
	if err := helper.HandleGqItemID(resp, data); err != nil {
		log.WithFields(logFields).Error(err)
		return nil, err
	}
	data.ID = fmt.Sprintf("%v", resp.Id)

	return data, nil
}

// NewVHCTicketing : TODO Doc
func NewVHCTicketing() *Service {
	return &Service{grpcClient: creategRPCClient()}
}

func creategRPCClient() *grpc.ClientConn {
	vhcTicketingURL := conf.VhcTicketingURL
	conn, err := grpc.Dial(
		vhcTicketingURL,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(nrgrpc.UnaryClientInterceptor),
		grpc.WithStreamInterceptor(nrgrpc.StreamClientInterceptor),
	)
	if err != nil {
		log.WithField("service", "unable to dial vhc-ticketing").Fatal(err)
	}
	return conn
}

func errorHandler(err error, logFields log.Fields) error {
	var message string
	statusCode := http.StatusInternalServerError

	if e, ok := status.FromError(err); ok {
		switch e.Code() {
		case codes.Internal:
			message = "Internal server"
		case codes.Unknown:
			message = "Ticketing Service:" + e.Message()
			statusCode = http.StatusUnprocessableEntity
		default:
			message = "Some error occured in ticketing service"
		}
	} else {
		message = "Ticketing server error"
	}
	if logFields == nil {
		logFields = log.Fields{}
	}
	logFields["Error Message"] = message
	logFields["StatusCode"] = statusCode
	log.WithFields(logFields).WithError(err).Errorf("Error making gRPC request to vhc_ticketing")

	return helper.Error(message, statusCode)
}

// GetTicketAuthAsync : verifies the user have access to the specified ticket ID
func (vhc Service) GetTicketAuthAsync(id string, user *model.SessionDetails, ctx context.Context) <-chan error {
	e := make(chan error)

	go func() {
		defer close(e)
		resp, err := vhc.GetTicket(id, ctx)
		if err != nil {
			e <- fmt.Errorf("Failed to fetch the ticket")
			return
		}

		if resp.CustomFields == nil || resp.CustomFields.CfRestaurantID == nil {
			e <- fmt.Errorf("Authentication Failed due to missing feilds")
			return
		}
		restID, permission := *resp.CustomFields.CfRestaurantID, resp.CustomFields.CfPermission

		if helper.AuthoriseRestAndPermissionsV2(nil, user, []int64{restID}, permission) != nil {
			e <- fmt.Errorf(constants.AuthorisationFailed)
			return
		}
		e <- nil
	}()
	return e
}
