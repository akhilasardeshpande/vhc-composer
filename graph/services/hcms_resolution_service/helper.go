package hcms_resolution_service

import (
	"bytes"
	"encoding/json"
	"errors"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type Service struct{}

func GetNewUserContentResolutionService() UserContentResolutionServiceRunner {
	return &Service{}
}

//content resolution API
func (svc Service) ResolveByUserIDAndSpaceID(sessionKey string, input model.UserResolutionRequest, txn *newrelic.Transaction) (*model.Space, error) {
	var content UserResolutionResponse
	var reqBody *bytes.Reader
	var apiName, userId, spaceId string
	apiName = "ResolveByUserIDAndSpaceID"
	var baseUrl, relativeUrl string

	_, err := helper.AuthenticateV2(sessionKey, txn)

	if err != nil {
		log.Errorf("Error from AuthV2 API. Error: %v, sessionKey: %v", err.Error(), sessionKey);
		return nil, helper.LogAndInstrumentError(apiName, constants.AuthorisationFailed)
	}

	baseUrl = conf.HeadlessCMSBaseURL
	relativeUrl = conf.HeadlessCMSContentResolutionPath

	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	queryParams := make(map[string]string)
	queryParams["userId"] = input.UserID
	queryParams["spaceId"] = input.SpaceID
	userId = input.UserID
	spaceId = input.SpaceID

	log.Info("UserId: ", userId, " SpaceId: ", spaceId)
	body, err := json.Marshal(input)
	if err != nil {
		log.Errorf("Error in Marshaling requestBody. Error: %v, requestBody: %v ", err.Error(), input);
		return nil, err
	}

	reqBody = bytes.NewReader(body)
	err = utils.MakeRequest(constants.HttpGetRequest, baseUrl+relativeUrl, reqBody, requestHeader, queryParams, &content, apiName, txn)
	if err != nil {
		log.Errorf("Error while fetching contents from HCMS. Error: %v", err.Error());
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		log.Errorf("HCMS Internal Server Error. statusCode: %v, statusMessage: %v", content.StatusCode, content.StatusMessage);
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, errors.New("failed to fetch user resolution info from Headless CMS: " + content.StatusMessage)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)
	log.Infof("data fetched from headlessCMS: %v", content.Data)

	return &content.Data, err
}

func (svc Service) Feedback(input model.FeedbackReq, txn *newrelic.Transaction) (*model.FeedbackResp, error) {
	apiName := "Feedback"
	url := conf.HeadlessCMSBaseURL + conf.HeadlessCMSFeedbackApi
	var content FeedbackResponse

	//set Headers
	requestHeader := make(map[string]string)
	requestHeader[constants.ContentType] = constants.ContentVal

	body, err := json.Marshal(input)
	if err != nil {
		log.Errorf("Error in Marshaling requestBody. Error: %v, requestBody: %v ", err.Error(), input);
		return nil, err
	}

	reqBody := bytes.NewReader(body)
	err = utils.MakeRequest(constants.HttpPostRequest, url, reqBody, requestHeader, nil, &content, apiName, txn)
	if err != nil {
		log.Errorf("Error while fetching contents from HCMS. Error: %v", err.Error());
		metric.ExternalApi.IncrementFailureCounter(apiName, constants.ReqFailed)
		return nil, err
	}

	if content.StatusCode != constants.SuccessStatusCode {
		log.Errorf("HCMS Internal Server Error. statusCode: %v, statusMessage: %v", content.StatusCode, content.StatusMessage);
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
	}

	log.Infof("Feedback response from HeadlessCMS: %v", content.Data)
	metric.ExternalApi.IncrementSuccessCounter(apiName)
	return &content.Data, nil
}
