package hcms_resolution_service

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/jarcoal/httpmock"
	"io/ioutil"
	"testing"
)

func MockGetUserSuccessResponse(t *testing.T) {
	URL := conf.RmsURL + conf.RmsUserInfoPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/rms_get_user_response.json")
	if err != nil {
		t.Errorf("Failed to read mock data: %v", err)
	}
	httpmock.Activate()
	httpmock.RegisterResponder("GET", URL, httpmock.NewBytesResponder(200, mockData))
}
