package hcms_resolution_service

import (
	"io/ioutil"
	"testing"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"
)

func TestContentResolutionSuccessResponse(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()

	URL := conf.HeadlessCMSBaseURL + conf.HeadlessCMSContentResolutionPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/hcms_success_response.json")
	require.Equal(t, nil, err, "error while reading mock data from file")
	httpmock.RegisterResponder("GET", URL, httpmock.NewBytesResponder(200, mockData))

	_, err = Service{}.ResolveByUserIDAndSpaceID("", model.UserResolutionRequest{UserID: "1", SpaceID: "2"}, nil)
	require.Equal(t, nil, err, "error when fetching content from Headless CMS")
}

func TestContentResolutionFailedResponse(t *testing.T) {
	MockGetUserSuccessResponse(t)
	defer httpmock.Deactivate()

	URL := conf.HeadlessCMSBaseURL + conf.HeadlessCMSContentResolutionPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/hcms_failed_response.json")
	require.Equal(t, nil, err, "error while reading mock data from file")
	httpmock.RegisterResponder("GET", URL, httpmock.NewBytesResponder(200, mockData))

	_, err = Service{}.ResolveByUserIDAndSpaceID("", model.UserResolutionRequest{UserID: "1", SpaceID: "2"}, nil)
	require.NotEqual(t, nil, err, "error when fetching content from Headless CMS")
}

func TestFeedbackResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.HeadlessCMSBaseURL + conf.HeadlessCMSFeedbackApi
	mockData, err := ioutil.ReadFile("../../../mocks/data/hcms_feedback_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("POST", url, httpmock.NewBytesResponder(200, mockData))

	feedback := make(map[string]string)
	feedback["action"] = "action"
	feedback["value"] = "value"
	req := model.FeedbackReq{
		UserID:    "123-456",
		ContentID: "1234-566",
		Feedback:  feedback,
	}
	_, err = Service{}.Feedback(req, nil)
	require.Equal(t, nil, err, "error while posting feedback")
}
