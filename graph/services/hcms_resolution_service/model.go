package hcms_resolution_service

import "bitbucket.org/swigy/vhc-composer/graph/model"

type UserResolutionResponse struct {
	StatusCode    int         `json:"statusCode"`
	StatusMessage string      `json:"statusMessage"`
	Data          model.Space `json:"data"`
}

type FeedbackResponse struct {
	StatusCode    int                `json:"statusCode"`
	StatusMessage string             `json:"statusMessage"`
	Data          model.FeedbackResp `json:"data"`
}
