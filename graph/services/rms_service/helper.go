package rms_service

import (
	"fmt"
	"strings"

	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

type Service struct {
}

func NewRMSService() ServiceRunner {
	return &Service{}
}

// Login : Authenticates the user in RMS & returns access_token along with some user info
func (rms Service) Login(username string, password string, txn *newrelic.Transaction) (*model.LoginResponse, error) {
	log.Infof("Login invoked")
	apiName := "RMSLogin"
	url := conf.RmsURL + conf.RmsLoginPath
	requestBody := strings.NewReader(fmt.Sprintf(`{
		"username": "%s",
		"password": "%s"
	}`, username, password))
	log.Infof("RMS Request {URL: %v, Body: %v }", url, requestBody)

	h := make(map[string]string)
	h["Content-Type"] = "application/json"
	var content LoginResponse

	err := utils.MakeRequest("POST", url, requestBody, h, nil, &content, apiName, txn)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		return nil, err
	}

	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, fmt.Errorf("Status Message: %v", content.StatusMessage)
	}
	log.Infof("Response: %v", content.Data)
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

// Logout : Invalidates the session key of the user in RMS & returns a bool with true indicating success
func (rms Service) Logout(username, sessionKey string, txn *newrelic.Transaction) (bool, error) {
	log.Infof("Logout invoked")
	apiName := "RMSLogout"
	url := conf.RmsURL + conf.RmsLogoutPath

	requestBody := strings.NewReader(fmt.Sprintf(`{
		"username": "%s"
	}`, username))
	log.Infof("RMS Request {URL: %v, Body: %v }", url, requestBody)
	requestHeader := makeHeaderMap(sessionKey)
	var content LoginResponse

	err := utils.MakeRequest("POST", url, requestBody, requestHeader, nil, &content, apiName, txn)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		return false, err
	}

	metric.ExternalApi.IncrementSuccessCounter(apiName)
	if content.StatusCode != 0 {
		return false, nil
	}
	log.Infof("Logout Success! : {user: %v, session-key: %v }", username, sessionKey)

	return true, nil
}

func makeHeaderMap(sessionKey string) map[string]string {
	return map[string]string{
		"Cookie":       "Swiggy_Session-alpha=" + sessionKey,
		"Content-Type": "application/json",
	}
}

// GetUser : Using session key it fetch & returns the user info from RMS
func (rms Service) GetUser(sessionKey string, txn *newrelic.Transaction) (*model.UserDetails, error) {
	log.Infof("GetUser invoked")
	apiName := "RMSGetUser"
	url := conf.RmsURL + conf.RmsUserInfoPath
	log.Infof("RMS Request {URL: %v, key: %v }", url, sessionKey)
	requestHeader := makeHeaderMap(sessionKey)
	var content GetUserResponse

	err := utils.MakeRequest("GET", url, nil, requestHeader, nil, &content, apiName, txn)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		return nil, err
	}

	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, fmt.Errorf("Failed to fetch User info from RMS: %v", content.StatusMessage)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}

func (rms Service) GetSessionInfo(sessionKey string, txn *newrelic.Transaction) (*model.SessionDetails, error) {
	log.Infof("GetSessionInfo invoked")
	apiName := "RMSGetSession"
	url := conf.RmsURL + conf.RmsSessionInfoPath
	log.Infof("RMS Request {URL: %v, key: %v }", url, sessionKey)
	requestHeader := makeHeaderMap(sessionKey)
	var content GetSessionResponse

	err := utils.MakeRequest("GET", url, nil, requestHeader, nil, &content, apiName, txn)
	if err != nil {
		metric.ExternalApi.IncrementFailureCounter(apiName, err.Error())
		return nil, err
	}

	if content.StatusCode != 0 {
		metric.ExternalApi.IncrementFailureCounter(apiName, content.StatusMessage)
		return nil, fmt.Errorf("Failed to fetch User info from RMS: %v", content.StatusMessage)
	}
	metric.ExternalApi.IncrementSuccessCounter(apiName)

	return &content.Data, nil
}
