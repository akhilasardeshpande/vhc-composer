package rms_service

import (
	"bitbucket.org/swigy/vhc-composer/graph/model"
)

// LoginResponse : Response body structure of RMS auth request
type LoginResponse struct {
	StatusCode    int                 `json:"statusCode"`
	StatusMessage string              `json:"statusMessage"`
	Data          model.LoginResponse `json:"data"`
}

// GetUserResponse : Response body structure of RMS fetch user info request
type GetUserResponse struct {
	StatusCode    int               `json:"statusCode"`
	StatusMessage string            `json:"statusMessage"`
	Data          model.UserDetails `json:"data"`
}

// GetSessionResponse : Response body structure of RMS fetch session info request
type GetSessionResponse struct {
	StatusCode    int                  `json:"statusCode"`
	StatusMessage string               `json:"statusMessage"`
	Data          model.SessionDetails `json:"data"`
}
