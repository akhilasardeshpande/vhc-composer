package order_relayer

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	"github.com/99designs/gqlgen/handler"
	"github.com/go-redis/redis/v8"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
	"runtime/debug"
	"time"
)

const (
	orderUpdate = "OrderUpdate"
)

type ServiceRunner interface {
	OrderUpdateController(ctx context.Context, restIds []int64) (<-chan *model.OrderNotification, error)
}
type Service struct {
	RedisClient redis.UniversalClient
}

func NewORS(client redis.UniversalClient) ServiceRunner {
	return &Service{
		RedisClient: client,
	}
}
func (ors Service) OrderUpdateController(ctx context.Context, restIds []int64) (<-chan *model.OrderNotification, error) {

	defer metric.Api.MeasureLatency(orderUpdate, time.Now())
	txn := metric.TraceTransaction(orderUpdate)
	defer metric.CloseTransaction(txn)
	reqId := utils.GetUUIDV4()
	logFields := log.Fields{
		"Restaurant Ids":    restIds,
		"InitPayload":       handler.GetInitPayload(ctx),
		constants.RequestID: reqId,
	}
	defer handlePanic(logFields)
	sessionKey := handler.GetInitPayload(ctx).GetString(constants.AccessToken)
	ctx = newrelic.NewContext(ctx, txn)
	ctx = utils.AddRequestIdToContext(ctx, reqId)
	user, err := helper.AuthenticateWithRestaurantsV3(ctx, sessionKey, restIds, constants.PermissionNoneForRMSAuth)
	if err != nil {
		defer cancelCtx(ctx, logFields)
		return nil, helper.LogAndInstrumentAuthFailure(orderUpdate, err, logFields)
	}
	log.WithFields(logFields).Infof("[schema.resolvers.OrderUpdate] User:%v", user)
	resp, err := ors.orderUpdates(ctx, restIds)
	metric.Api.IncrementCounter(orderUpdate, err)
	return resp, err

}

func cancelCtx(ctx context.Context, fields log.Fields) {
	subContext := utils.GetSubContext(ctx)
	if subContext == nil {
		log.WithFields(fields).Info("Failed to cancel the ws connection")
	} else {
		subContext.Cancel()
		log.WithFields(fields).Info("cancelled the ws connection ctx")
	}
}

func handlePanic(fields log.Fields) {
	if a := recover(); a != nil {
		log.WithFields(fields).WithField("StackTrace", string(debug.Stack())).Warnf("Panic Reovered: %v", a)
	}
}
