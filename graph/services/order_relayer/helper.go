package order_relayer

import (
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	"encoding/json"
	"errors"
	"github.com/99designs/gqlgen/handler"
	"strconv"
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

const (
	orderSubscription = "OrderSubscription"
)

func (ors Service) orderUpdates(ctx context.Context, restIds []int64) (<-chan *model.OrderNotification, error) {
	mc := make(chan *model.OrderNotification, 1)
	restaurantId := convertToStringSlice(restIds)
	var orderNotification model.OrderNotification
	logFields := log.Fields{
		"RestaurantIds":     restIds,
		constants.RequestID: utils.GetRequestIdFromContext(ctx),
	}
	userAgent := getPlatform(ctx)
	log.WithFields(logFields).Info("Subscribing to List of Restaurant Streams")
	go func() {
		metric.Consumer.IncrementSubscriptionCounter(orderSubscription, userAgent)
		defer close(mc)
		defer metric.Consumer.MeasureSubscriptionPeriod(orderSubscription, userAgent, time.Now())
		defer metric.Consumer.DecrementSubscriptionCounter(orderSubscription, userAgent)

		for {
			streams, err := ors.RedisClient.XRead(ctx, &redis.XReadArgs{
				Streams: restaurantId,
				Block:   0,
			}).Result()

			if errors.Is(err, context.Canceled) {
				log.WithFields(logFields).Info("closing the stream")
				return
			} else if err != nil {
				log.WithFields(logFields).Errorf("Error while Fetching Stream data %v", err)
				break
			}

			stream := streams[0]
			err = json.Unmarshal([]byte(stream.Messages[0].Values["data"].(string)), &orderNotification)
			if err != nil {
				log.WithFields(logFields).Errorf("Error while Unmarshalling data : %v", err)
				break
			}

			log.Infof("OrderNotification : %v", orderNotification)
			mc <- &orderNotification

		}
	}()

	return mc, nil
}

func convertToStringSlice(intSlice []int64) []string {
	var strSlice, idSlice []string
	//Convert all the int64 slice to string slice

	for _, x := range intSlice {
		strSlice = append(strSlice, strconv.FormatInt(x, 10))
		idSlice = append(idSlice, "$")

	}
	strSlice = append(strSlice, idSlice...)
	return strSlice
}

func getPlatform(ctx context.Context) string {
	useragent := handler.GetInitPayload(ctx).GetString(constants.UserAgent)

	switch useragent {
	case "android":
		return useragent
	case "ios":
		return useragent
	}

	return helper.DetectPlatform(ctx.Value(constants.UserAgent).(string))
}
