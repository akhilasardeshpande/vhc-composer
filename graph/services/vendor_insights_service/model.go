package vendor_insights_service

import "bitbucket.org/swigy/vhc-composer/graph/model"

type CartConversionsResponse struct {
	StatusCode    int                `json:"statusCode"`
	StatusMessage string             `json:"statusMessage"`
	Data          model.CartSessions `json:"data"`
}

type MenuSessionsResponse struct {
	StatusCode    int                `json:"statusCode"`
	StatusMessage string             `json:"statusMessage"`
	Data          model.MenuSessions `json:"data"`
}

type NewRepeatCustomersResponse struct {
	StatusCode    int                     `json:"statusCode"`
	StatusMessage string                  `json:"statusMessage"`
	Data          model.NewRepeatCustomer `json:"data"`
}

type ConversionMetricsResponse struct {
	StatusCode    int                             `json:"statusCode"`
	StatusMessage string                          `json:"statusMessage"`
	Data          model.ConversionMetricsResponse `json:"data"`
}

type BusinessMetricsResponse struct {
	StatusCode    int                   `json:"statusCode"`
	StatusMessage string                `json:"statusMessage"`
	Data          model.BusinessMetrics `json:"data"`
}

type CustomerSentimentsResponse struct {
	StatusCode    int                      `json:"statusCode"`
	StatusMessage string                   `json:"statusMessage"`
	Data          model.CustomerSentiments `json:"data"`
}
