package vendor_insights_service

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"testing"
)

func TestCartConversions(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VICartSessionsPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_cart_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.CartSessions(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestMenuSessions(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VIMenuSessionsPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_menu_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.MenuSessions(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestConversionMetrics(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VIConversionMetricsPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_conversion_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.ConversionMetrics(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestNewRepeatCustomer(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VINewRepeatCustomerPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_new_repeat_customer_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.NewRepeatCustomer(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestBusinessMetrics(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VIBusinessMetricsPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_business_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.BusinessMetrics(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestCustomerSentiments(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	url := conf.VIBaseURL + conf.VICustomerSentimentsPath
	mockData, err := ioutil.ReadFile("../../../mocks/data/vi_customer_sentiments_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", url, httpmock.NewBytesResponder(200, mockData))
	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.BusinessMetrics(period, restIds, nil)
	assert.Equal(t, nil, err)
}

func TestMetrics(t *testing.T) {
	httpmock.Activate()
	defer httpmock.Deactivate()
	cartSessionMockData, err := ioutil.ReadFile("../../../mocks/data/vi_cart_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch CartSession mock data")
	httpmock.RegisterResponder("GET", conf.HeadlessCMSBaseURL+conf.VICartSessionsPath,
		httpmock.NewBytesResponder(200, cartSessionMockData))

	menuSessionMockData, err := ioutil.ReadFile("../../../mocks/data/vi_menu_sessions_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIMenuSessionsPath,
		httpmock.NewBytesResponder(200, menuSessionMockData))

	conversionMockData, err := ioutil.ReadFile("../../../mocks/data/vi_conversion_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIConversionMetricsPath,
		httpmock.NewBytesResponder(200, conversionMockData))

	newRepeatMockData, err := ioutil.ReadFile("../../../mocks/data/vi_new_repeat_customer_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VINewRepeatCustomerPath,
		httpmock.NewBytesResponder(200, newRepeatMockData))

	businessMetrics, err := ioutil.ReadFile("../../../mocks/data/vi_business_metrics_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VIBusinessMetricsPath,
		httpmock.NewBytesResponder(200, businessMetrics))

	customerSentiments, err := ioutil.ReadFile("../../../mocks/data/vi_customer_sentiments_response.json")
	require.Equal(t, nil, err, "could not fetch mock data")
	httpmock.RegisterResponder("GET", conf.VIBaseURL+conf.VICustomerSentimentsPath,
		httpmock.NewBytesResponder(200, customerSentiments))

	restIds := make([]int64, 0)
	restIds = append(restIds, int64(9990))
	period := "today"
	_, err = Service{}.Metrics(period, restIds, nil)
	require.Equal(t, nil, err, "failed to fetch metrics")
}
