package vendor_insights_service

import (
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
)

type ServiceRunner interface {
	MetricsController(sessionKey, period string, restIds []int64) (*model.Metrics, error)
}

func (svc Service) MetricsController(sessionKey, period string, restIds []int64) (*model.Metrics, error) {
	apiName := "Metrics"
	defer metric.Api.MeasureLatency(apiName, time.Now())
	txn := metric.TraceTransaction(apiName)
	defer metric.CloseTransaction(txn)
	_, err := helper.AuthenticateWithRestaurantsV2(sessionKey, restIds, txn, constants.BusinessMetricsPermission)
	if err != nil {
		return nil, helper.LogAndInstrumentError(apiName, constants.AuthorisationFailed)
	}
	resp, err := svc.Metrics(period, restIds, txn)
	metric.Api.IncrementCounter(apiName, err)
	return resp, err
}
