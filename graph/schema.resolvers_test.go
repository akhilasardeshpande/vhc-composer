package graph

import (
	"fmt"
	"testing"

	grpcTest "bitbucket.org/swigy/oh-my-test-helper/golang/pkg/grpc"
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/model"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_ticketing"
	"bitbucket.org/swigy/vhc-composer/mocks"
	pb "bitbucket.org/swigy/vhc-ticketing/proto"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"google.golang.org/grpc"
)

// Unit tests for graphql handler

func fetchUser() *model.UserDetails {
	return &model.UserDetails{
		UserRole:    1,
		Permissions: []int{1, 2, 3, 4, 5},
		Restaurants: []*model.Restaurant{
			{
				RestID: 9990,
			},
		},
	}
}
func TestUnitLogin(t *testing.T) {
	testRmsService := new(mocks.MockedRmsService)

	resolvers := Resolver{
		RMSService: testRmsService,
	}
	c := client.New(GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))
	loginResponse := model.LoginResponse{
		AccessToken: "dummyToken",
		Rid:         12895,
		UserRole:    3,
		Permissions: []int{1, 2, 3, 4, 5},
	}
	testRmsService.On("LoginController", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(&loginResponse, nil)

	q := `
	mutation login {
		login(input: { username: "9738948943", password: "El3ment@ry" }) {
		  access_token,rid,userRole,permissions
	}}`

	var resp struct {
		Login *model.LoginResponse `json:"login"`
	}
	err := c.Post(q, &resp)
	if err != nil {
		t.Errorf("Error: %v", err)
	}
	t.Logf("Response: %v", resp)
	assert.Equal(t, &loginResponse, resp.Login)
}

func TestTicketingService(t *testing.T) {

	numberOfMockedQueries := 7

	testRmsService := new(mocks.MockedRmsService)

	_, err := grpcTest.NewContainer("7779", func(server *grpc.Server) {
		pb.RegisterTicketServiceServer(server, &mocks.UnimplementedVhcTicketingServer{})
	})
	if err != nil {
		t.Errorf("Error Initializing container. Error = %+v", err)
	}
	conf.RmsURL = "localhost:8888"
	conf.VhcTicketingURL = "localhost:7779"

	resolvers := Resolver{
		RMSService:   testRmsService,
		VHCTicketing: vhc_ticketing.NewVHCTicketing(),
	}
	c := client.New(GetHeadersMiddleware(handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &resolvers}))))

	testRmsService.On("GetUserController", mock.AnythingOfType("string")).Return(nil, fmt.Errorf("")).Times(numberOfMockedQueries)
	testTicketingServiceResolvers(t, c)

	user := fetchUser()
	testRmsService.On("GetUserController", mock.AnythingOfType("string")).Return(user, nil).Times(numberOfMockedQueries)
	testTicketingServiceResolvers(t, c)
}

func testTicketingServiceResolvers(t *testing.T, c *client.Client) {

	queries := getTicketingServiceQueries()

	for i := range queries {
		var resp interface{}
		err := c.Post(queries[i], &resp, client.AddHeader("access_token", "dummy"))

		if err == nil {
			t.Errorf("Expected Error: but got nil")
		}
		t.Logf("Expected Error: %v", err)
	}
}

func getTicketingServiceQueries() []string {
	return []string{`
		query GetTicket{
			GetTicket(id: "48") {
			description
			}
		}`, `
		query GetTickets{
			GetTickets(query:"", page:1, restaurantIDs:[9990]){
			total
			}
		}`, `
		query GetTicketConversations{
			GetTicketConversations(id:"1"){
			  body
			}
		}`, `
		mutation {
			CreateTicket(ticket:{type:"",custom_fields:{cf_restaurant_id:0}}){
			  name
			}
		}`, `
		mutation {
			UpdateTicketStatus(id:"1",status:2){
			  name
			}
		}`, `
		mutation {
			UpdateTicket(id:"1",ticket:{custom_fields:{cf_restaurant_id:0}}){
			  name
			}
		}`, `
		mutation {
			CreateTicketReply(id: "1", reply: {}) {
			  body
			}
		}`,
	}
}
