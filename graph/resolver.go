package graph

import (
	"bitbucket.org/swigy/vhc-composer/graph/services/hcms_resolution_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/hunger_games_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/order_relayer"
	"bitbucket.org/swigy/vhc-composer/graph/services/rms_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/srs_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vendor_insights_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_ticketing"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

// Resolver : TODO doc
type Resolver struct {
	VHCService            vhc_service.ServiceRunner
	RMSService            rms_service.ServiceRunner
	VHCTicketing          *vhc_ticketing.Service
	HCMSResolutionService hcms_resolution_service.UserContentResolutionServiceRunner
	VIService             vendor_insights_service.ServiceRunner
	HungerGamesService    hunger_games_service.ServiceRunner
	OrderRelayer          order_relayer.ServiceRunner
	SRSService            srs_service.ServiceRunner
}
