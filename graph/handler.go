package graph

import (
	"bitbucket.org/swigy/vhc-composer/conf"
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/graph/generated"
	"bitbucket.org/swigy/vhc-composer/graph/services/hcms_resolution_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/hunger_games_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/order_relayer"
	"bitbucket.org/swigy/vhc-composer/graph/services/rms_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/srs_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vendor_insights_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_service"
	"bitbucket.org/swigy/vhc-composer/graph/services/vhc_ticketing"
	"bitbucket.org/swigy/vhc-composer/helper"
	"bitbucket.org/swigy/vhc-composer/metric"
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	"errors"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/lru"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"net/http"
	"runtime/debug"
)

// GraphqlHandler : Returns a default handler for graphql
func GraphqlHandler(client redis.UniversalClient) http.Handler {
	cfg := generated.Config{
		Resolvers: &Resolver{
			VHCService:            vhc_service.NewVHCService(),
			RMSService:            rms_service.NewRMSService(),
			VHCTicketing:          vhc_ticketing.NewVHCTicketing(),
			HCMSResolutionService: hcms_resolution_service.GetNewUserContentResolutionService(),
			VIService:             vendor_insights_service.GetVendorInsightsService(),
			HungerGamesService:    hunger_games_service.GetHungerGamesService(),
			OrderRelayer:          order_relayer.NewORS(client),
			SRSService:            srs_service.NewSRSService(),
		},
	}
	srv := handler.New(generated.NewExecutableSchema(cfg))

	srv.AddTransport(&transport.Websocket{
		Upgrader: websocket.Upgrader{
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
		},
		InitFunc: func(ctx context.Context, initPayload transport.InitPayload) (context.Context, error) {
			if initPayload == nil || initPayload.GetString(constants.AccessToken) == "" {
				return ctx, helper.LogAndInstrumentAuthFailure("Subscription", &websocket.CloseError{Code: http.StatusUnauthorized, Text: "Empty initPayload"}, nil)
			}
			nctx, cancel := context.WithCancel(ctx)
			return context.WithValue(nctx, constants.SubContextKey, &utils.SubContext{Cancel: cancel}), nil
		},
		KeepAlivePingInterval: constants.KeepAlivePingInterval,
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{})

	srv.SetQueryCache(lru.New(1000))

	if conf.Env == "development" || conf.Env == "staging" {
		srv.Use(extension.Introspection{})
	}
	srv.Use(extension.AutomaticPersistedQuery{
		Cache: lru.New(100),
	})

	srv.SetRecoverFunc(func(ctx context.Context, err interface{}) error {
		log.WithFields(log.Fields{"ctx": ctx, "Error": err, "stack": string(debug.Stack())}).Errorf("Unhandled Panic!")

		return errors.New("internal server error")
	})

	return GetHeadersMiddleware(srv)
}

// GetHeadersMiddleware fetches headers & adds it to context
func GetHeadersMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		metric.Api.IncrementActiveRequests()
		defer metric.Api.DecrementActiveRequests()
		keys := []string{constants.AccessToken, constants.UserAgent}
		ctx := r.Context()
		for i := range keys {
			val := r.Header.Get(keys[i])
			ctx = context.WithValue(ctx, keys[i], val)
		}
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
