package datastore

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"context"
	"errors"
	"time"

	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/go-redis/redis/v8"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

func NewRedisClient(redisConfig *conf.RedisConfig, Environment string) (redis.UniversalClient, error) {
	log.WithFields(log.Fields{
		"RedisConfig": redisConfig,
	}).Infof("Redis Configuration")
	var client redis.UniversalClient
	options := &redis.UniversalOptions{
		Addrs:           []string{redisConfig.RedisUrl},
		Password:        redisConfig.RedisPassword, // no default password set
		DB:              redisConfig.RedisDBIndex,
		ReadTimeout:     redisConfig.RedisTimeout,
		WriteTimeout:    redisConfig.RedisTimeout,
		PoolSize:        redisConfig.RedisPoolSize,
		MaxRetries:      3,
		MinRetryBackoff: 10 * time.Millisecond,
		MaxRetryBackoff: 1 * time.Second,
	}
	if Environment == constants.Production {
		client = redis.NewClusterClient(options.Cluster())
	} else {
		client = redis.NewClient(options.Simple())
	}

	ctx := newrelic.NewContext(context.Background(), nil)
	_, err := client.Ping(ctx).Result()
	if !errors.Is(err, nil) {
		return nil, err
	}

	return client, nil
}
