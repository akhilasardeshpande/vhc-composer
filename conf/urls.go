package conf

var (
	// VhcTicketingURL for vhc-ticketing service
	VhcTicketingURL string

	// VhcServiceURL for vhc-service
	VhcServiceURL string

	// RmsURL is Host URL for RMS
	RmsURL string

	// RmsLoginPath is Path URL for RMS Login Endpoint
	RmsLoginPath string

	// RmsUserInfoPath is Path URL for RMS User Info Endpoint
	RmsUserInfoPath string

	// RmsSessionInfoPath is Path URL for RMS User Info Endpoint
	RmsSessionInfoPath string

	// SRSURL is Host URL for RMS
	SrsURL string
	// SrsTimeSlotPath is path for slot update
	SrsTimeSlotPath string

	// RmsLogoutPath is Path URL for RMS Logout Endpoint
	RmsLogoutPath                    string
	HeadlessCMSBaseURL               string
	HeadlessCMSContentResolutionPath string
	HeadlessCMSFeedbackApi           string

	//Customer Metrics Service path
	VIBaseURL                string
	VICartSessionsPath       string
	VIMenuSessionsPath       string
	VIConversionMetricsPath  string
	VINewRepeatCustomerPath  string
	VIBusinessMetricsPath    string
	VICustomerSentimentsPath string

	//Hunger Games
	HungerGamesBaseURL               string
	HungerGamesGetScorePath          string
	HungerGamesGetTieringDetailsPath string

	// VendorConfig
	VendorConfigURL           string
	VendorConfigWebSocketPath string

	//Vendor Auth Config
	VendorAuthURL            string
	VendorAuthGetSessionPath string
)

func init() {

	VhcTicketingURL = Get("VHC_TICKETING_URL")
	VhcServiceURL = Get("VHC_SERVICE_URL")

	RmsURL = Get("RMS_BASE_URL")
	RmsLoginPath = Get("RMS_LOGIN_PATH")
	RmsUserInfoPath = Get("RMS_USER_INFO_PATH")
	RmsSessionInfoPath = Get("RMS_SESSION_INFO_PATH")
	RmsLogoutPath = Get("RMS_LOGOUT_PATH")
	//HeadlessCMS variables
	HeadlessCMSFeedbackApi = Get("HEADLESS_CMS_FEEDBACK_PATH")
	HeadlessCMSContentResolutionPath = Get("HEADLESS_CMS_CONTENT_RESOLUTION_PATH")
	HeadlessCMSBaseURL = Get("HEADLESS_CMS_SERVICE_URL")
	//Vendor Insights Customer Metrics variables
	VIBaseURL = Get("VENDOR_INSIGHTS_URL")
	VICartSessionsPath = Get("VI_CART_SESSIONS_PATH")
	VIMenuSessionsPath = Get("VI_MENU_SESSIONS_PATH")
	VIConversionMetricsPath = Get("VI_CONVERSION_METRICS_PATH")
	VINewRepeatCustomerPath = Get("VI_NEW_REPEAT_CUSTOMER_PATH")
	VIBusinessMetricsPath = Get("VI_BUSINESS_METRICS_PATH")
	VICustomerSentimentsPath = Get("VI_CUSTOMER_SENTIMENTS_PATH")

	HungerGamesBaseURL = Get("HUNGER_GAMES_URL")
	HungerGamesGetScorePath = Get("HUNGER_GAMES_GET_SCORES_PATH")
	HungerGamesGetTieringDetailsPath = Get("HUNGER_GAMES_GET_TIERING_DETAILS_PATH")

	VendorConfigURL = Get("VENDOR_CONFIG_URL")
	VendorConfigWebSocketPath = Get("VENDOR_CONFIG_GET_WEBSOCKET")

	VendorAuthURL = Get("VENDOR_AUTH_URL")
	VendorAuthGetSessionPath = Get("VENDOR_AUTH_SESSION_INFO_PATH")
	SrsURL = Get("SRS_BASE_URL")
	SrsTimeSlotPath = Get("SRS_TIME_SLOT_PATH")
}
