package conf

import "bitbucket.org/swigy/kafka-client-go/swgykafka"

var (
	// TxnHAPrimaryCluster is the configs of TxnHAPrimary cluster
	TxnHAPrimaryCluster KafkaClusterConfig

	// TxnHASecondaryCluster is the configs of TxnHAPrimary cluster
	TxnHASecondaryCluster KafkaClusterConfig

	// NewOrderConsumer stores configs for Order polling consumer
	NewOrderConsumer KafkaConsumerConfig

	// EditedOrderConsumer stores configs for Order polling consumer
	EditedOrderConsumer KafkaConsumerConfig

	// PlacingFSMConsumer stores configs for Placing FSM event consumer
	PlacingFSMConsumer KafkaConsumerConfig

	// OrderCancelConsumer stores configs for Swiggy Order Cancel event consumer
	OrderCancelConsumer KafkaConsumerConfig

	// OrderCancelConsumer stores configs for Swiggy Order Cancel event consumer
	OrderStatusUpdateV2Consumer KafkaConsumerConfig

	// MFRAccuracyConsumer stores configs for Vendor RMS MFR Accuracy  event consumer
	MFRAccuracyConsumer KafkaConsumerConfig
)

func init() {

	TxnHAPrimaryCluster.Host = Get("CONFLUENT_KAFKA_TXN_HA_PRIMARY.HOST")
	TxnHAPrimaryCluster.Username = Get("KAFKA_TXN_HA_PRIMARY_API_KEY")
	TxnHAPrimaryCluster.Password = Get("KAFKA_TXN_HA_PRIMARY_API_SECRET")
	TxnHAPrimaryCluster.Auth = GetKafkaAuthMechanism(Get("CONFLUENT_KAFKA_TXN_HA_PRIMARY.AUTH"))
	TxnHAPrimaryCluster.ClientId = "vhc-composer"

	TxnHASecondaryCluster.Host = Get("CONFLUENT_KAFKA_TXN_HA_SECONDARY.HOST")
	TxnHASecondaryCluster.Username = Get("KAFKA_TXN_HA_SECONDARY_API_KEY")
	TxnHASecondaryCluster.Password = Get("KAFKA_TXN_HA_SECONDARY_API_SECRET")
	TxnHASecondaryCluster.Auth = GetKafkaAuthMechanism(Get("CONFLUENT_KAFKA_TXN_HA_SECONDARY.AUTH"))
	TxnHASecondaryCluster.ClientId = "vhc-composer"

	NewOrderConsumer.PrimaryCluster = &TxnHAPrimaryCluster
	NewOrderConsumer.Topic = Get("NEW_ORDER_CONSUMER.TOPIC")
	NewOrderConsumer.ConsumerGroup = Get("NEW_ORDER_CONSUMER.GROUP")
	NewOrderConsumer.AutoOffsetReset = GetKafkaConsumerAutoOffsetReset(Get("NEW_ORDER_CONSUMER.AUTO_OFFSET_RESET"))
	NewOrderConsumer.PollTimeout = GetInt("NEW_ORDER_CONSUMER.POLL_TIMEOUT")
	NewOrderConsumer.SecondaryCluster = &TxnHASecondaryCluster

	EditedOrderConsumer.PrimaryCluster = &TxnHAPrimaryCluster
	EditedOrderConsumer.Topic = Get("EDITED_ORDER_CONSUMER.TOPIC")
	EditedOrderConsumer.ConsumerGroup = Get("EDITED_ORDER_CONSUMER.GROUP")
	EditedOrderConsumer.AutoOffsetReset = GetKafkaConsumerAutoOffsetReset(Get("EDITED_ORDER_CONSUMER.AUTO_OFFSET_RESET"))
	EditedOrderConsumer.PollTimeout = GetInt("EDITED_ORDER_CONSUMER.POLL_TIMEOUT")
	EditedOrderConsumer.SecondaryCluster = &TxnHASecondaryCluster

	PlacingFSMConsumer.PrimaryCluster = &TxnHAPrimaryCluster
	PlacingFSMConsumer.Topic = Get("PLACING_FSM_CONSUMER.TOPIC")
	PlacingFSMConsumer.ConsumerGroup = Get("PLACING_FSM_CONSUMER.GROUP")
	PlacingFSMConsumer.AutoOffsetReset = GetKafkaConsumerAutoOffsetReset(Get("PLACING_FSM_CONSUMER.AUTO_OFFSET_RESET"))
	PlacingFSMConsumer.PollTimeout = GetInt("PLACING_FSM_CONSUMER.POLL_TIMEOUT")
	PlacingFSMConsumer.SecondaryCluster = &TxnHASecondaryCluster

	OrderCancelConsumer.PrimaryCluster = &TxnHAPrimaryCluster
	OrderCancelConsumer.Topic = Get("ORDER_CANCEL_CONSUMER.TOPIC")
	OrderCancelConsumer.ConsumerGroup = Get("ORDER_CANCEL_CONSUMER.GROUP")
	OrderCancelConsumer.AutoOffsetReset = GetKafkaConsumerAutoOffsetReset(Get("ORDER_CANCEL_CONSUMER.AUTO_OFFSET_RESET"))
	OrderCancelConsumer.PollTimeout = GetInt("ORDER_CANCEL_CONSUMER.POLL_TIMEOUT")
	OrderCancelConsumer.SecondaryCluster = &TxnHASecondaryCluster

	OrderStatusUpdateV2Consumer.PrimaryCluster = &TxnHAPrimaryCluster
	OrderStatusUpdateV2Consumer.Topic = Get("ORDER_STATUS_UPDATE_V2_CONSUMER.TOPIC")
	OrderStatusUpdateV2Consumer.ConsumerGroup = Get("ORDER_STATUS_UPDATE_V2_CONSUMER.GROUP")
	OrderStatusUpdateV2Consumer.AutoOffsetReset = GetKafkaConsumerAutoOffsetReset(Get("ORDER_STATUS_UPDATE_V2_CONSUMER.AUTO_OFFSET_RESET"))
	OrderStatusUpdateV2Consumer.PollTimeout = GetInt("ORDER_STATUS_UPDATE_V2_CONSUMER.POLL_TIMEOUT")
	OrderStatusUpdateV2Consumer.SecondaryCluster = &TxnHASecondaryCluster

}

type KafkaClusterConfig struct {
	Host     string
	Username string
	Password string
	ClientId string
	Auth     swgykafka.AuthMechanism
}

type KafkaConsumerConfig struct {
	PrimaryCluster   *KafkaClusterConfig
	SecondaryCluster *KafkaClusterConfig
	Topic            string
	ConsumerGroup    string
	PollTimeout      int
	AutoOffsetReset  swgykafka.ConsumerAutoOffsetReset
}

func GetKafkaAuthMechanism(auth string) swgykafka.AuthMechanism {
	switch auth {
	case "plain":
		return swgykafka.AuthSaslPlain
	case "scram":
		return swgykafka.AuthSaslScram
	default:
		return swgykafka.AuthNone
	}
}

func GetKafkaConsumerAutoOffsetReset(offset string) swgykafka.ConsumerAutoOffsetReset {
	switch offset {
	case "earliest":
		return swgykafka.Earliest
	default:
		return swgykafka.Latest
	}
}
