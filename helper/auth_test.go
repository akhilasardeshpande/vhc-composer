package helper

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/utils"
	"context"
	"net/http"
	"testing"

	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/wiremock"
	"bitbucket.org/swigy/vhc-composer/mocks"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func TestAuthV3(t *testing.T) {
	container, err := wiremock.NewContainer()
	assert.Nil(t, err, "error initialising wiremock")
	mocks.MockVendorAuthSuccessResponse(t, container)
	_, err = AuthenticateWithRestaurantsV3(context.Background(), "dummy", []int64{12895}, 0)
	assert.Equal(t, nil, err)
	reqId := "Req:123"
	ctx := utils.AddRequestIdToContext(context.TODO(), reqId)
	mocks.MockVendorAuthInternalErrorResponse(t, container)
	_, err = AuthenticateWithRestaurantsV3(ctx, "dummy", []int64{12895}, 0)
	assert.Equal(t, ErrorWithId("Invalid Response From Upstream Server", http.StatusBadGateway, reqId), err)

	mocks.MockVendorAuthInvalidInputResponse(t, container)
	_, err = AuthenticateWithRestaurantsV3(ctx, "dummy", []int64{12895}, 0)
	assert.Equal(t, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId), err)

	mocks.MockVendorAuthInvalidSessionResponse(t, container)
	_, err = AuthenticateWithRestaurantsV3(ctx, "dummy", []int64{12895}, 0)
	assert.Equal(t, Error(constants.AuthorisationFailed, http.StatusUnauthorized), err)

	mocks.MockVendorAuthCallFailure(t, container)
	_, err = AuthenticateWithRestaurantsV3(ctx, "dummy", []int64{12895}, 0)
	assert.Equal(t, ErrorWithId(constants.InvalidResponseFromUpstream, http.StatusBadGateway, reqId), err)

	err2 := LogAndInstrumentAuthFailure("AuthTest", err, nil)
	assert.Equal(t, err, err2)
}

func TestAuth(t *testing.T) {
	mocks.MockGetSessionSuccessResponse(t)
	defer httpmock.Deactivate()
	_, err := AuthenticateWithRestaurantsV2("", []int64{12895}, nil, 3)
	assert.Equal(t, nil, err)

	_, err = AuthenticateV2("", nil)
	assert.Equal(t, nil, err)

}
