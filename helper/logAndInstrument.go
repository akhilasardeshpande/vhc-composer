package helper

import (
	"fmt"
	"runtime"

	"bitbucket.org/swigy/vhc-composer/metric"
	log "github.com/sirupsen/logrus"
)

const (
	caller = "caller"
)

// LogAndInstrumentError Logs & Instruments the Error returned by schema resolver
func LogAndInstrumentError(apiName, message string) error {
	metric.Api.IncrementFailureCounter(apiName, message)
	fields := log.Fields{"API": apiName}
	if _, file, line, ok := runtime.Caller(1); ok {
		fields[caller] = fmt.Sprintf("%s:%d", file, line)
	}

	log.WithFields(fields).Errorf(message)

	return fmt.Errorf(message)
}

func LogAndInstrumentErrorV2(apiName, err string, status int, fields log.Fields) error {
	metric.Api.IncrementFailureCounter(apiName, fmt.Sprintf("Status:%d", status))
	localFields := log.Fields{"API": apiName, "status": status}
	if _, file, line, ok := runtime.Caller(1); ok {
		localFields[caller] = fmt.Sprintf("%s:%d", file, line)
	}

	log.WithFields(fields).WithFields(localFields).Error(err)

	return Error(err, status)
}

func LogAndInstrumentAuthFailure(apiName string, err error, fields log.Fields) error {
	metric.Api.IncrementFailureCounter(apiName, err.Error())

	localFields := log.Fields{"API": apiName}
	if _, file, line, ok := runtime.Caller(1); ok {
		localFields[caller] = fmt.Sprintf("%s:%d", file, line)
	}

	log.WithFields(fields).WithFields(localFields).Warn(err)

	return err
}
