package helper

import (
	"bitbucket.org/swigy/vhc-composer/constants"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

// Error forms & returns gqlerror object with message & status code
func Error(message string, status int) error {
	return &gqlerror.Error{
		Message: message,
		Extensions: map[string]interface{}{
			constants.StatusCode: status,
		},
	}
}

// ErrorWithId forms & returns gqlerror object with message, status code & requestId
func ErrorWithId(message string, status int, requestId string) error {
	return &gqlerror.Error{
		Message: message,
		Extensions: map[string]interface{}{
			constants.StatusCode: status,
			"RequestID":          requestId,
		},
	}
}
