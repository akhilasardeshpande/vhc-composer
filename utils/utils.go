package utils

import (
	"bitbucket.org/swigy/vhc-composer/types"
	"encoding/json"
	"fmt"
	"github.com/gojek/heimdall/v7/httpclient"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/swigy/vhc-composer/constants"
	"bitbucket.org/swigy/vhc-composer/metric"
	"github.com/araddon/dateparse"
	"github.com/google/uuid"
	"github.com/newrelic/go-agent/v3/newrelic"
	log "github.com/sirupsen/logrus"
)

func MakeRequest(method string, url string, body io.Reader, headers, queryParams map[string]string, response interface{}, apiName string, txn *newrelic.Transaction) error {
	client := &http.Client{}
	client.Transport = newrelic.NewRoundTripper(client.Transport)
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return fmt.Errorf("Error building request : %v", err)
	}

	//Set Headers
	for key, val := range headers {
		req.Header.Set(key, val)
		log.Infof("Set Header for req key: %q", key)
	}
	//Set Query Params
	q := req.URL.Query()
	for key, val := range queryParams {
		q.Add(key, val)
	}
	req.URL.RawQuery = q.Encode()
	req = newrelic.RequestWithTransactionContext(req, txn)
	startTime := time.Now()
	resp, err := client.Do(req)
	metric.ExternalApi.MeasureLatency(apiName, startTime)
	if err != nil {
		return fmt.Errorf("Error making request : %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusMultipleChoices {
		return fmt.Errorf("Error making request status code: %v", resp.StatusCode)
	}

	var respBody []byte
	respBody, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Error in reading response: %v", err)
	}

	if err = json.Unmarshal(respBody, response); err != nil {
		return fmt.Errorf("error in unmarshelling response : %v", err)
	}
	return nil
}

func ConvertToStringFromList(restIds []int64) string {
	var restIdString string
	//Convert all the restIds to string separated by ','
	for _, item := range restIds {
		itemString := strconv.FormatInt(item, 10)
		if len(restIdString) > 0 {
			restIdString = restIdString + "," + itemString
		} else {
			restIdString = itemString
		}
	}
	return restIdString
}
func ConvertToRMSTimeStampFormat(timestamp string) string {
	time, err := dateparse.ParseLocal(timestamp + constants.TimeZone)
	if err != nil {
		log.Errorf("error while parsing the time : %s", err.Error())
	}
	return time.Format(constants.RMSTimeFormat)

}

func GetUUIDV4() string {
	u, err := uuid.NewUUID()
	if err != nil {
		return ""
	}
	return u.String()
}

// MakeAPICall makes api call request
func MakeAPICall(client *httpclient.Client, request types.HttpRequest, response interface{}, meta types.HttpRequestMeta) error {
	req, err := http.NewRequest(request.Method, request.Url, request.Body)
	if err != nil {
		return fmt.Errorf("error building request : %v", err)
	}

	//Set Headers
	for key, val := range request.Headers {
		req.Header.Set(key, val)
	}
	//Set Query Params
	q := req.URL.Query()
	for key, val := range request.QueryParams {
		q.Add(key, val)
	}
	req.URL.RawQuery = q.Encode()
	txn := newrelic.FromContext(request.Context)
	req = newrelic.RequestWithTransactionContext(req, txn)
	s := newrelic.StartExternalSegment(txn, req)
	startTime := time.Now()
	resp, err := client.Do(req)
	metric.ExternalApi.MeasureLatency(meta.ApiName, startTime)
	s.Response = resp
	s.End()
	if err != nil {
		return fmt.Errorf("error making request : %v", err)
	}
	defer resp.Body.Close()

	respBody, readErr := ioutil.ReadAll(resp.Body)
	if resp.StatusCode < http.StatusOK || resp.StatusCode >= http.StatusMultipleChoices {
		// If we are able to parse the request, it means the response returned can be used to extract error message
		if err = json.Unmarshal(respBody, response); err == nil {
			return nil
		}
		return fmt.Errorf("error making request status code: %v", resp.StatusCode)
	}
	if readErr != nil {
		return fmt.Errorf("error in reading response: %v", err)
	}
	if err = json.Unmarshal(respBody, response); err != nil {
		return fmt.Errorf("error in unmarshelling response : %v", err)
	}
	return nil
}
