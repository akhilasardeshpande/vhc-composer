package main

import (
	"fmt"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/swigy/oh-my-test-helper/golang/pkg/redis"
	"bitbucket.org/swigy/vhc-composer/conf"
	"github.com/stretchr/testify/assert"
)

func TestMainFunc(t *testing.T) {
	redisContainer, err := redis.NewContainer()

	assert.Equal(t, nil, err, "Failed to initialise Redis test container")
	conf.Redis.RedisUrl = fmt.Sprintf("%s:%s", redisContainer.Host(), redisContainer.Port())
	conf.Redis.RedisPassword = ""
	err = RunMainSever()
	assert.Nil(t, err)
}

func RunMainSever() error {
	go func() {
		main()
	}()
	time.Sleep(3 * time.Second)
	return nil
}

func TestHealth(t *testing.T) {
	req := httptest.NewRequest("GET", "http://localhost/health", nil)
	w := httptest.NewRecorder()
	health(w, req)
	assert.Equal(t, "ok", w.Body.String())
}
