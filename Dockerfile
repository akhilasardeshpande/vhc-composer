FROM golang:1.15.15-alpine AS build-env
WORKDIR /work
COPY . .
RUN apk add --no-cache --update gcc libc-dev git openssh
RUN GOOS=linux go build -tags musl -mod vendor -v -installsuffix cgo -o vhc-composer .


FROM alpine:latest  
WORKDIR /work

ENV APP_PORT 7080
EXPOSE $APP_PORT

COPY --from=build-env /work/vhc-composer .
CMD ["./vhc-composer"]
